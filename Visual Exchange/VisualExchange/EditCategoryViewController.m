//
//  EditCategoryViewController.m
//  VisualExchange
//
//  Created by mac on 12/22/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "EditCategoryViewController.h"
#import "EditCategoryTableViewCell.h"
#import "EditProductVC.h"

@interface EditCategoryViewController ()

@end

@implementation EditCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrStates=[[NSMutableArray alloc]init];
    selectionary=[[NSMutableArray alloc]init];
    categoryidary=[[NSMutableArray alloc]init];
    _savebtn.layer.cornerRadius =_savebtn.frame.size.height/2;
    _savebtn.layer.masksToBounds=YES;
    [self apicall];

    // Do any additional setup after loading the view.
}
-(void)apicall
{
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"categoriesList.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
     {
         arrStates = [JSON[@"country"]mutableCopy];
         
         if(arrStates.count>0)
         {
             
             for (int i=0; i<arrStates.count; i++) {
                 
                 
                 NSMutableDictionary*  tempdict=[arrStates objectAtIndex:i];
                 NSMutableDictionary *   mutable=[tempdict mutableCopy];
                 [mutable setObject:@"1" forKey:@"flag"];
                 
                 // [selectionary replaceObjectAtIndex:i withObject:mutable];
                 
                 [selectionary addObject:mutable];
                 
                 
             }
         }
         if(_productpasary.count>0)
         {
             
             for (int i=0; i<_productpasary.count; i++)
             {
                 NSString *idstr1=[NSString stringWithFormat:@"%@",[_productpasary objectAtIndex:i]];
                 for(int j=0; j<selectionary.count; j++)
                {
                     NSString *idstr=[[selectionary objectAtIndex:j]objectForKey:@"categorie_id"] ;
                 if([idstr isEqualToString:idstr1])
                 {
                    NSMutableDictionary* tempdict=[selectionary objectAtIndex:j];
                  NSMutableDictionary*   mutable=[tempdict mutableCopy];
                     [mutable setObject:@"0" forKey:@"flag"];
                     
                     [selectionary replaceObjectAtIndex:j withObject:mutable];
                     
                     
                     
                 }
                 }
            }
         }

         
         
         

         [_tblview reloadData];
     }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectionary.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = (EditCategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if([[[selectionary objectAtIndex:indexPath.row]objectForKey:@"flag"]isEqualToString:@"0"])
    {
        [cell.chekimg setImage:[UIImage imageNamed:@"checked.png"]];
        cell.catbtn.tag=1;
        cell.chekimg.tag=1;
        
        
        
        
        
        
        
    }
    else{
        
        
        [cell.chekimg setImage:[UIImage imageNamed:@"unchecked.png"]];
        cell.catbtn.tag=2;
        cell.chekimg.tag=2;
        
        
    }
    
    [cell.catbtn addTarget:self action:@selector(checkbtnpress:) forControlEvents:UIControlEventTouchUpInside];
    cell.catlbl.text=[[arrStates objectAtIndex:indexPath.row]objectForKey:@"name"];
    
    return cell;
}
-(void)checkbtnpress:(UIButton *)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tblview];
    NSIndexPath *tappedIP = [self.tblview indexPathForRowAtPoint:buttonPosition];
    
    if(sender.tag==2)
    {
        EditCategoryTableViewCell *cell = [self.tblview cellForRowAtIndexPath:tappedIP];
        [cell.chekimg setImage:[UIImage imageNamed:@"checked.png"]];
        
        for (int i=0; i<selectionary.count; i++) {
            if([[[selectionary objectAtIndex:tappedIP.row]objectForKey:@"flag"]isEqualToString:@"1"])
            {
                
                NSMutableDictionary *tempdict=[selectionary objectAtIndex:tappedIP.row];
                [tempdict setObject:@"0" forKey:@"flag"];
                [selectionary replaceObjectAtIndex:tappedIP.row withObject:tempdict];
            }
        }
        //[selectionary addObject:[categoryidary objectAtIndex:tappedIP.row]];
        [sender setTag:1];
    }
    else
    {
        EditCategoryTableViewCell *cell = [self.tblview cellForRowAtIndexPath:tappedIP];
        for (int i=0; i<selectionary.count; i++) {
            if([[[selectionary objectAtIndex:tappedIP.row]objectForKey:@"flag"]isEqualToString:@"0"])
            {
                
                NSMutableDictionary *tempdict=[selectionary objectAtIndex:tappedIP.row];
                [tempdict setObject:@"1" forKey:@"flag"];
                [selectionary replaceObjectAtIndex:tappedIP.row withObject:tempdict];
            }
            
            
            
        }
        
        //[selectionary removeObjectAtIndex:[categoryidary objectAtIndex:tappedIP.row]];
        [cell.chekimg setImage:[UIImage imageNamed:@"unchecked.png"]];
        [sender setTag:2];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backpress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)savebtnpess:(id)sender
{
    NSMutableArray *getary=[[NSMutableArray alloc]init];
        NSMutableArray *myary=[[NSMutableArray alloc]init];
    for (int i=0; i<selectionary.count ; i++) {
        if([[[selectionary objectAtIndex:i]objectForKey:@"flag"]isEqualToString:@"0"])
        {
        [getary addObject:[[selectionary objectAtIndex:i]objectForKey:@"categorie_id"]];
         }
        else{
            [myary addObject:[selectionary objectAtIndex:i]];
        }
    }
    if(myary.count==selectionary.count)
    {
        UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select category." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [objalert show];

        
    }
    else{
    EditProductVC * obj = [ self.storyboard instantiateViewControllerWithIdentifier:@"EditProductVC"];
    obj.passcatidary=getary;
    [self.navigationController pushViewController:obj animated:true];
    }
    
}
@end
