//
//  AppDelegate.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//
#import "Firebase.h"

#import "AppDelegate.h"
#import "HomeSubCatItemsVC.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@import FirebaseMessaging;
@import FirebaseAnalytics;
@import FirebaseInstanceID;



@interface AppDelegate ()<UITabBarControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    JCSegmentBarController *segmentBarController;
//    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
//    for (int i = 0; i < 10; i++)
//    {
//        HomeSubCatItemsVC *vc1 = [[HomeSubCatItemsVC alloc] init];
//        //NSString *name = [dict[@"name"]uppercaseString];
//        NSString *name = [NSString stringWithFormat:@"%d",i];
//        vc1.title=name;
//        segmentBarController = [[JCSegmentBarController alloc] initWithViewControllers:vc1];
//        segmentBarController.delegate = self;
//        segmentBarController.segmentBar.selectedFont = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
//        segmentBarController.segmentBar.selectedFont=[UIColor redColor];
//         segmentBarController.segmentBar.unSelectedFont=[UIColor redColor];
//        segmentBarController.segmentBar.unSelectedFont = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
// 
//        
//    }
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:segmentBarController];
//    // Override point for customization after application launch.
//    
//
    
    [Fabric with:@[[Crashlytics class]]];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    self.window.rootViewController = nav;
    self.window.backgroundColor = [UIColor whiteColor];
    

    
    [self isLoginUser];
    [self.window makeKeyAndVisible];
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                           PayPalEnvironmentSandbox :@"AUEipAj-i-LKCxNCMzIoM5nmzVHHZev-VZdi3zRBjk0Ih0RpR4K9VUaQPK2hxLgocyuwjFZIygfcuOwp"}];
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    NSMutableArray *array = [kUserDefults_(@"cart+cart")mutableCopy];
    [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = array.count==0?nil:[NSString stringWithFormat:@"%lu",(unsigned long)array.count];
    
    if(kUserDefults_(@"cart+cart") == nil){
    NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
    [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%d",count]:nil;
    }
    self.tabbarController.delegate = self;
    
    
    
    
    NSString *platform = [UIDevice currentDevice].systemVersion;
    UIDevice *device = [UIDevice currentDevice];
    
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    [[NSUserDefaults standardUserDefaults]setObject:uniqueIdentifier forKey:@"DeviceId"];
    
    
    
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshCallBack:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
    {
        // ios 8 and 9 code ....... for registrer remot
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    else{
        
        // ios 10 code.......
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
         }
         ];
        
        // For iOS 10 display notification (sent via APNS)
        [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
        // For iOS 10 data message (sent via FCM)
        [[FIRMessaging messaging] setRemoteMessageDelegate:self];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    
    

    return YES;
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    NSLog(@"userInfo=>%@", userInfo);
    
    
    NSString *message = nil;
    NSDictionary *aps=[userInfo objectForKey:@"aps"];
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
        
       // [self handleBackgroundNotification:aps];
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
    else if (application.applicationState == UIApplicationStateBackground) {
        NSLog(@"Remote Notification Received: %@", userInfo);
        
        //[self handleBackgroundNotification:aps];
      //
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:[aps objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [objalert show];
        //Show an in-app banner
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    }

}
- (void) clearNotifications {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"device :%@",deviceToken);
  
    [[NSUserDefaults standardUserDefaults]setObject:deviceToken forKey:@"gcm"];
    
    
    const void *devTokenBytes = [deviceToken bytes];
    
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    
    
}

-(void)tokenRefreshCallBack:(NSNotification *)notification
{
    NSString *refreshedToken=[[FIRInstanceID instanceID]token];
    [[NSUserDefaults standardUserDefaults]setObject:refreshedToken forKey:@"refreshToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSLog(@"Instanced token: %@",refreshedToken);
    [self connectToFcm];
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // application.applicationIconBadgeNumber = 0;
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        // do stuff when app is active
        
    }else{
        // do stuff when app is in background
        [UIApplication sharedApplication].applicationIconBadgeNumber =
        [UIApplication sharedApplication].applicationIconBadgeNumber+1;
        /* to increment icon badge number */
    }
    
    
}


- (void)application:(UIApplication *)app
didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    
    NSLog(@"%@", [remoteMessage appData]);
}
//- (void)checkNetworkStatus:(NSNotification *)notice {
//    // called after network status changes
//    
//    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
//    switch (internetStatus)
//    {
//        case NotReachable:
//        {
//            NSLog(@"The internet is down.");
//            break;
//        }
//        case ReachableViaWiFi:
//        {
//            NSLog(@"...............");
//            break;
//        }
//        case ReachableViaWWAN:
//        {
//            NSLog(@"The internet is working via WWAN!");
//            break;
//        }
//    }
//}




- (void)applicationWillResignActive:(UIApplication *)application {
    NSLog(@"Disconnect from FCM");
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // [self connectToFcm];
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnect from FCM");
}


- (void)connectToFcm
{
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error)
     {
         if (error != nil)
         {
             NSLog(@"Unable to connect to FCM. %@", error);
         }
         else
         {
             NSLog(@"Connected to FCM.");
         }
     }];
}
// ios 10 code for receiving rremote notification....
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
//    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);

//    

    
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSDictionary *aps=[userInfo objectForKey:@"aps"];

    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
       NSLog(@"userInfo=>%@", userInfo);
    UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:[aps objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [objalert show];

    
}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - setRootViewController -
-(void)isLoginUser {
    
    [DLHelper shareinstance].selectedIndex = 1000;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (kUserDefults_(ISLOGIN)) {
        
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
        _tabbarController = [storyboard instantiateViewControllerWithIdentifier:@"TabbarController"];
        
        self.drawerController = [[MMDrawerController alloc]
                                 initWithCenterViewController:_tabbarController
                                 leftDrawerViewController:leftDrawer
                                 rightDrawerViewController:nil];
        [self.drawerController setShowsShadow:NO];
        [self.drawerController setRestorationIdentifier:@"MMDrawer"];
        [self.drawerController setMaximumRightDrawerWidth:200.0];
        [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeBezelPanningCenterView];
        [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
        self. window.rootViewController = self.drawerController;
        _tabbarController.delegate = self;
    }
    else
    {
        
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
        _tabbarController = [storyboard instantiateViewControllerWithIdentifier:@"TabbarController"];
       
        NSMutableArray *tbViewControllers = [NSMutableArray arrayWithArray:[self.tabbarController viewControllers]];
        [tbViewControllers removeObjectAtIndex:3];
        [self.tabbarController setViewControllers:tbViewControllers];
        self.drawerController = [[MMDrawerController alloc]
                                 initWithCenterViewController:_tabbarController
                                 leftDrawerViewController:leftDrawer
                                 rightDrawerViewController:nil];
        [self.drawerController setShowsShadow:NO];
        [self.drawerController setRestorationIdentifier:@"MMDrawer"];
        [self.drawerController setMaximumRightDrawerWidth:200.0];
        [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
       
        self. window.rootViewController = self.drawerController;
         _tabbarController.delegate = self;
        //
        //        UIViewController * center = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        //        UINavigationController *navCenter = [[UINavigationController alloc]initWithRootViewController:center];
        //        navCenter.navigationBarHidden = YES;
        //        [self.window setRootViewController:navCenter];
        
    }
    
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
 //   [self.videoPlayerViewController.moviePlayer stop];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(tabBarController.selectedIndex == 3){
        
        UINavigationController *navigation = self.tabbarController.selectedViewController;
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"ProfileEditVC"];
        navigation.viewControllers = @[leftDrawer].mutableCopy;
        
        return;
    }
    if(tabBarController.selectedIndex == 2){
        
        UINavigationController *navigation = self.tabbarController.selectedViewController;
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"SearchCatagoryVC"];
        navigation.viewControllers = @[leftDrawer].mutableCopy;
        
        return;
    }
    if(tabBarController.selectedIndex == 1){
        
        UINavigationController *navigation = self.tabbarController.selectedViewController;
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"MyCartVC"];
        navigation.viewControllers = @[leftDrawer].mutableCopy;
        
        return;
    }
    if(tabBarController.selectedIndex == 0){
        
        UINavigationController *navigation = self.tabbarController.selectedViewController;
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        navigation.viewControllers = @[leftDrawer].mutableCopy;
        
        return;
    }


//    UINavigationController *navigation = self.tabbarController.selectedViewController;
//    [navigation popToRootViewControllerAnimated:false];
//    [[AppLauncher sharedInstance].topItemVC.navigationController popViewControllerAnimated:false];
//    
//    NSLog(@"%@",[AppLauncher sharedInstance].topItemVC);
}

@end
