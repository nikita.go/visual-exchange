//
//  EditCategoryViewController.h
//  VisualExchange
//
//  Created by mac on 12/22/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditCategoryTableViewCell.h"

@interface EditCategoryViewController : UIViewController
{
    NSMutableArray *arrStates;
    NSMutableArray *categoryidary;
    EditCategoryTableViewCell *cell;
    NSMutableArray *selectionary;
}
- (IBAction)backpress:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblview;
@property (weak, nonatomic) IBOutlet UIButton *savebtn;
- (IBAction)savebtnpess:(id)sender;
@property(strong,nonatomic)NSMutableArray *productpasary;

@end
