//
//  ChangePasswordVC.m
//  VisualExchange
//
//  Created by Minhaz on 7/2/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "ChangePasswordVC.h"


@interface ChangePasswordVC () <UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView *theScrollView;
    __weak IBOutlet UITextField *tfOldPassword;
    __weak IBOutlet UITextField *tfNewPassword1;
    __weak IBOutlet UITextField *tfNewPassword2;
    __weak IBOutlet UIButton *btUpdate;
    UIView *selectedView;
}
@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Singup button shape
    btUpdate.layer.cornerRadius = btUpdate.frame.size.height/2.0;
    
    //set text field delegates
    tfOldPassword.delegate = self;
    tfNewPassword1.delegate = self;
    tfNewPassword2.delegate = self;
    
    [self doValidationShowMessage:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self doValidationShowMessage:NO];
    [self addKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeKeyboardNotifications];
}

#pragma mark - Keyboard notifications
- (void)addKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardDidShow:(NSNotification *)notif
{
    if (!selectedView) {
        return;
    }
    NSDictionary* info = [notif userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = [selectedView convertPoint:selectedView.frame.origin toView:nil];
    
    CGFloat buttonHeight = selectedView.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardDidHide:(NSNotification *)notif
{
    [theScrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Actions
- (IBAction)onBackBtClicked:(id)sender {
    [[AppLauncher sharedInstance]openLeftDrawer];
}

- (IBAction)onUpdateBtClicked:(id)sender {
 
    [self.view endEditing:YES];
    
    if (!tfOldPassword.text.validatePassword) {
        [self showErrorWithMessage:dlkOldPass];
        return;
        
    }
    else   if (!tfNewPassword1.text.validatePassword) {
        [self showErrorWithMessage:dlkNewPass];
        return;
        
    }
    else if (![tfNewPassword1.text isEqualToString:tfNewPassword2.text]) {
        [self showErrorWithMessage:dlkPassMis];
        return;
       
    }

        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"user_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        dict[@"old_password"] = tfOldPassword.text;
        dict[@"new_password"] = tfNewPassword1.text;
    
        
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"changePassword.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            if([[JSON objectForKey:@"message"] isEqualToString:@"Your Password Does not match."])
            {
                UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Your password does not match." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [objalert show];
            }
            else{
            tfOldPassword.text=@"";
            tfNewPassword1.text=@"";
             tfNewPassword2.text=@"";
            [self showErrorWithMessage:dlkSuccesspass];
            }
            
        }];

    
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    selectedView = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self doValidationShowMessage:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == tfOldPassword) {
        [tfNewPassword1 becomeFirstResponder];
    } else if (textField == tfNewPassword1) {
        [tfNewPassword2 becomeFirstResponder];
    } else if (textField == tfNewPassword2) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - Validations

- (BOOL)doValidationShowMessage:(BOOL)shouldShowErrorMessage
{
    BOOL valid = YES;
    
    NSString *errorString = nil;
    do {
        if (
            tfOldPassword.text.isEmptyString
            || tfNewPassword1.text.isEmptyString
            || tfNewPassword2.text.isEmptyString
            ) {
            valid = NO;
            errorString = @"Please fill all fields.";
            break;
        }
        if (
            ! tfOldPassword.text.validatePassword
            || ! tfNewPassword1.text.validatePassword
            || !tfNewPassword2.text.validatePassword
            ) {
            errorString = @"Password is not valid. At least 6 characters.";
            valid = NO;
            break;
        }
        if (![tfNewPassword1.text isEqualToString:tfNewPassword2.text]) {
            errorString = @"New passwords do not match.";
            valid = NO;
            break;
        }
        break;
        
    } while (1);
    
    if (shouldShowErrorMessage && errorString && errorString.length > 0) {
        [self showErrorWithMessage:errorString];
    }
    
    btUpdate.enabled = valid;
    [self updateButtonStateWithState:valid];
    
    return valid;
}

- (void)updateButtonStateWithState:(BOOL)state
{
    if (state) {
        btUpdate.backgroundColor = [UIColor specSheetGreen];
    } else {
        btUpdate.backgroundColor = [UIColor lightGrayColor];
    }
}
#pragma mark - Touch Events
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
