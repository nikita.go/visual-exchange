//
//  ProfileEditVC.h
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "BaseVC.h"
#import "DLHelper.h"
@interface ProfileEditVC : BaseVC<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;

@property (weak, nonatomic) IBOutlet UITextField *txtZipcode;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@end
