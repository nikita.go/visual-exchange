//
//  ProfileEditVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "ProfileEditVC.h"
#import "ProfilePhotoCell.h"
#import "ProfileTextInputCell.h"
#import "ProfileUpdateButtonCell.h"






@interface ProfileEditVC () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic) UIPopoverController *popover;

@end

@implementation ProfileEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollview.contentSize = CGSizeMake(_scrollview.frame.size.width, 800);

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUserImageClicked:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    self.imgProfile.userInteractionEnabled = true;
    [self.imgProfile addGestureRecognizer:tapGestureRecognizer];
    
   
    
    self.btnUpdate.layer.cornerRadius = self.btnUpdate.frame.size.height/2;
    self.btnUpdate.layer.masksToBounds = true;
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
      [DLHelper shareinstance]. selectedIndex = 2;
    
        for (UIView *v  in self.scrollview.subviews){
            v.userInteractionEnabled = false;
        }
        self.btnUpdate.hidden = true;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"user_id"] = [NSString stringWithFormat:@"%@",kUserDefults_(ResultUser)[@"customer_id"]];
    
    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"getaccount.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        for(NSString *str in JSON.allKeys){
            if([JSON[str] isKindOfClass:[NSNull class]]){
                JSON[str] = @"";
            }
        }
        
        
        JSON[@"customer_id"] = JSON[@"user_id"];
        [JSON removeObjectForKey:@"user_id"];
        
        kUserDefults(JSON.mutableCopy, ResultUser);
        
        NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
        self.txtName.text = dict[@"name"];
        self.txtAddress.text = dict[@"address"];
        self.txtCity.text = dict[@"city"];
        self.txtState.text = dict[@"state"];
        //self.txtCountry.text= dict[@"country"] ;
        self.txtZipcode.text = dict[@"zipcode"];
        self.txtMobile.text = dict[@"phone"];
        self.txtEmail.text = kUserDefults_(ResultUser)[@"email"];
        if(dict != nil){
            [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:self.imgProfile urlString:dict[@"image"] Complete:^(UIImage *image) {
                
            }];
        }
    }];

//        NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
//        self.txtName.text = dict[@"name"];
//        self.txtAddress.text = dict[@"address"];
//        self.txtCity.text = dict[@"city"];
//        self.txtState.text = dict[@"state"];
//        //self.txtCountry.text= dict[@"country"] ;
//        self.txtZipcode.text = dict[@"zipcode"];
//        self.txtMobile.text = dict[@"phone"];
//        self.txtEmail.text = kUserDefults_(ResultUser)[@"email"];
//        if(dict != nil){
//            [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:self.imgProfile urlString:dict[@"image"] Complete:^(UIImage *image) {
//                
//            }];
//        }

    
    
    self.imgProfile.userInteractionEnabled = true;
    
}
-(void)viewDidAppear:(BOOL)animated{
      }
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Get uer object



- (IBAction)EditPress:(id)sender {
    EditProfileVC * obj = [ self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    [self.navigationController pushViewController:obj animated:true];
    
}
#pragma mark - Actions
- (IBAction)onMenuButtonClicked:(id)sender {
    [[AppLauncher sharedInstance] openLeftDrawer];
}


#pragma mark - UITextFieldDelegate-

#pragma mark - On User image clicked
- (void)onUserImageClicked:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
        
        KIViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"KIViewController"];
        obj.arrayUrl = @[kUserDefults_(ResultUser)[@"image"]];
        [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
        [self presentViewController:obj animated:true completion:nil];
        return;
    
    
    
}



@end
