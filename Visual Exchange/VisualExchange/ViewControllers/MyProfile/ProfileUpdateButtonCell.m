//
//  ProfileUpdateButtonCell.m
//  VisualExchange
//
//  Created by Minhaz on 6/25/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "ProfileUpdateButtonCell.h"

@implementation ProfileUpdateButtonCell

- (void)layoutSubviews{
    [super layoutSubviews];
    _btUpdate.layer.cornerRadius = _btUpdate.frame.size.height/2.0;
    _btUpdate.layer.masksToBounds = YES;
}

@end
