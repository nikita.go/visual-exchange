//
//  OrderDetailVC.h
//  VisualExchange
//
//  Created by Nilay Shah on 25/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
@interface OrderDetailVC : BaseVC
@property(strong,nonatomic)NSMutableDictionary *result;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@property(assign,nonatomic) NSInteger from_sellonVC;
@property(strong,nonatomic)NSString *order;
@end
