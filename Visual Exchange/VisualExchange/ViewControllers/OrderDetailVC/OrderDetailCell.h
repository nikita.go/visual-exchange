//
//  OrderDetailCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 25/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet UILabel *lblmobile;
@property (weak, nonatomic) IBOutlet UILabel *lbladress;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbldate;
@property (weak, nonatomic) IBOutlet UILabel *lblTopay;
@property (weak, nonatomic) IBOutlet UILabel *lblShiping;
@property (weak, nonatomic) IBOutlet UILabel *lbltax;
@property (weak, nonatomic) IBOutlet UILabel *lblPromocode;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblstaticPromocode;
@property (weak, nonatomic) IBOutlet UILabel *shippinglabel;

@end
