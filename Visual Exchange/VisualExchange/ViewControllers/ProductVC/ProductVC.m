//
//  ProductVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 21/04/1938 SAKA.
//  Copyright © 1938 SAKA Company. All rights reserved.
//

#import "ProductVC.h"

@interface ProductVC ()
{
NSMutableArray *arraySearch;
}@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation ProductVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[DLHelper shareinstance]pedingAll:self.txtSearch.superview];
    self.txtSearch.layer.borderColor = [UIColor colorWithRed:153/255.f green:193/255.f blue:58/255.f alpha:1.0].CGColor;
    self.txtSearch.layer.cornerRadius = 4;
    self.txtSearch.layer.borderWidth = 1;
    self.txtSearch.layer.masksToBounds = true;
    self.tableview.tableFooterView = [UIView new];
    self.tableview.backgroundColor = [UIColor clearColor];
   
    
    UINib *nibProduct = [UINib nibWithNibName:NSStringFromClass([ProductCell class]) bundle:nil];
    [self.tableview registerNib:nibProduct forCellReuseIdentifier:@"ProductCell"];
   
    
    [self fetchItems];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)fetchItems
{
    if (self.moreProduct != nil){
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"customer_id"] = self.moreProduct[@"customer_id"];
        dict[@"product_id"] =  self.moreProduct[@"product_id"];
       
        
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"customersMoreProducts.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            arraySearch = [JSON[@"productList"]mutableCopy];
            [self.tableview reloadData];
            
        }];

        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"user_id"] = kUserDefults_(ResultUser)[@"customer_id"];
    dict[@"categorie_id"] = _category[@"categorie_id"];
    dict[@"page_number"] = @"1";

    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"productlist.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        for(NSMutableDictionary *dict in JSON[@"productList"]){
            dict[@"categorie_id"] = dict[@"categori_id"];
            dict[@"categori_id"] = nil;
        }
        arraySearch = [JSON[@"productList"]mutableCopy];
        [self.tableview reloadData];
        
    }];

   }

#pragma mark - UITextfield Delegate -
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}
#pragma mark -UITAbleview DataSource & Delegate -
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arraySearch.count;;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    
    NSString *imagepath = nil;
    if([arraySearch[indexPath.row][@"images"]count]>0){
        imagepath = arraySearch[indexPath.row][@"images"][0][@"product_image"];
    }else{
        imagepath = arraySearch[indexPath.row][@"image"];
    }
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:cell.imgmain urlString:imagepath Complete:^(UIImage *image) {}];
   
    cell.lblName.text = arraySearch[indexPath.row][@"name"];
    NSString *subtitle = [NSString stringWithFormat:@"%@ - by %@",arraySearch[indexPath.row][@"categoryname"],arraySearch[indexPath.row][@"customer_name"]];
    cell.lblSubtitle.text = subtitle;

    cell.lblprice.text = [NSString stringWithFormat:@"$%@",arraySearch[indexPath.row][@"price"]];
    cell.clipsToBounds = true;
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        ProductDetailsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailsVC"];
        vc.product = [arraySearch[indexPath.row]mutableCopy];
        [self.navigationController pushViewController:vc animated:YES];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 300 * SCREEN_WIDTH/320;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view  = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
#pragma mark -IBAction Methods -
- (IBAction)SearchPress:(id)sender {
    
    if(self.txtSearch.text.length>0){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"name"] = self.txtSearch.text;
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"productsearch.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            for(NSMutableDictionary *dict in JSON[@"productList"]){
                dict[@"categorie_id"] = dict[@"categori_id"];
                dict[@"categori_id"] = nil;
            }
            arraySearch = [JSON[@"productList"]mutableCopy];
            [self.tableview reloadData];
            
        }];
    }
}
- (IBAction)BackPress:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

@end
