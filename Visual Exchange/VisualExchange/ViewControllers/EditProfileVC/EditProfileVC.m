//
//  EditProfileVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 17/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "EditProfileVC.h"
#import "ProfileEditVC.h"
#import "UIImage+UIImage_fixOrientation.h"
@interface EditProfileVC ()
{
    UIPickerView *pickerStates;
    UIPopoverController *popOver;
    NSMutableArray *arrStates;
    NSInteger tg;
    
}
@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollview.contentSize = CGSizeMake(_scrollview.frame.size.width, 600);
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUserImageClicked:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    self.imgProfile.userInteractionEnabled = true;
    [self.imgProfile addGestureRecognizer:tapGestureRecognizer];
    
      
    self.btnUpdate.layer.cornerRadius = self.btnUpdate.frame.size.height/2;
    self.btnUpdate.layer.masksToBounds = true;
    
    NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
    self.txtName.text = dict[@"name"];
    self.txtAddress.text = dict[@"address"];
    self.txtCity.text = dict[@"city"];
    self.txtState.text = dict[@"state"];
    //self.txtCountry.text= dict[@"country"] ;
    self.txtZipcode.text = dict[@"zipcode"];
    self.txtMobile.text = dict[@"phone"];
    self.txtEmail.text = kUserDefults_(ResultUser)[@"email"];
    if(dict != nil){
        [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:self.imgProfile urlString:dict[@"image"] Complete:^(UIImage *image) {
            
        }];
    }


}
-(void)viewWillAppear:(BOOL)animated{
    
   
}
-(void)viewDidAppear:(BOOL)animated{
   // [self.scrollview AutomaticScrollview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == self.txtState){
        
        [self fetchStates];
        [textField resignFirstResponder];
        [[UIApplication sharedApplication] resignFirstResponder];
    }
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)onUpdateBtClicked:(id)sender {
    [self.view endEditing:YES];
    
    
    if (self.txtName.text.isEmptyString) {
        [self showErrorWithMessage:dlkName];
    }
    
    //    else if (!self.txtEmail.text.validateEmailWithString) {
    //        [self showErrorWithMessage:@"Please Enter valid Email Address"];
    //    }
    else if (self.txtAddress.text.isEmptyString) {
        [self showErrorWithMessage:dlkAddress];
    }
    else if (self.txtCity.text.isEmptyString) {
        [self showErrorWithMessage:dlkCity];
    }
    else if (self.txtState.text.isEmptyString) {
        [self showErrorWithMessage:dlkState];
    }
    else if (self.txtCountry.text.isEmptyString) {
        [self showErrorWithMessage:dlkCountry];
    }
    else if (self.txtZipcode.text.isEmptyString) {
        [self showErrorWithMessage:dlkZipcode];
    }
    
    else if (self.txtMobile.text.isEmptyString) {
        [self showErrorWithMessage:dlkMobile];
    }
    else if (!self.txtMobile.text.validatePhoneNumber) {
        [self showErrorWithMessage:dlkMobile];
    }
    else if ([self.imgProfile.image isEqual:[UIImage imageNamed:@"demo_image"]]) {
        [self showErrorWithMessage:dlkimageProfile];
    }
    else{
        NSString * stateid = nil;
        for(int i=0; i<arrStates.count; i++){
            if([self.txtState.text isEqualToString:arrStates[i][@"state"]]){
                stateid = arrStates[i][@"state_code"];
                break;break;
            }
        }
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"Id"] = [NSString stringWithFormat:@"%@",kUserDefults_(ResultUser)[@"customer_id"]];
        dict[@"name"] = self.txtName.text;;
        dict[@"address"] = self.txtAddress.text;
        dict[@"city"] = self.txtCity.text;
        dict[@"state"] = self.txtState.text;
        dict[@"country"] = self.txtCountry.text;
        dict[@"zipcode"] =self.txtZipcode.text;
        dict[@"phone"] = self.txtMobile.text;
        dict[@"image"] = img1;
       // dict[@"image"] = [self uplaodprofielpic];
        dict[@"email"] = self.txtEmail.text = kUserDefults_(ResultUser)[@"email"];
        
        [[DLHelper shareinstance]DLSERVICE_IMAGE:self.view Indicater:true url:@"editCustomer.php" UrlBody:dict Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            [JSON removeObjectForKey:@"status"];
            for (id object in [JSON allKeys]) {
                if([JSON[object] isKindOfClass:[NSNull class]]){
                    JSON[object] = @"";
                }
            }
            JSON[@"customer_id"] = JSON[@"user_id"];
            [JSON removeObjectForKey:@"user_id"];
            JSON[@"email"] =  kUserDefults_(ResultUser)[@"email"];
            kUserDefults(JSON, ResultUser);
            [self showAlertWithTitle:@"Message" message:dlkSuccessProfile];
        }];
        
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        ProfileEditVC * obj = [ self.storyboard instantiateViewControllerWithIdentifier:@"ProfileEditVC"];
        [self.navigationController pushViewController:obj animated:true];
    }
}
-(NSMutableData *)uplaodprofielpic
{
    NSString *string ;
    NSData *imageData;
    NSMutableData *body;
    body = [NSMutableData data];
    double my_time = [[NSDate date] timeIntervalSince1970];
    
    NSString *imageName = [NSString stringWithFormat:@"%d",(int)(my_time)];
    NSString *imagetag=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image[]\"; filename=\""];
    string = [NSString stringWithFormat:@"%@%@%@", imagetag, imageName, @".jpg\"\r\n\""];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:string] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    UIImage *image=[[UIImage alloc]init];
    image=self.imgProfile.image;
    imageData = UIImageJPEGRepresentation(image, .30);
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    return body;

}
- (void)fetchStates
{
    [[[UIApplication sharedApplication]keyWindow]endEditing: true];
    
    if(arrStates.count){
        
        PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
        pickerViewController.pickerType = CustomPickerType;
        pickerViewController.dataSourceForCustomPickerType = [arrStates valueForKey:@"state"];
        [pickerViewController setInitialItemAtIndex:0];
        pickerViewController.delegate = self;
        [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
        [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
        return;
    }
    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"states.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        arrStates = [JSON[@"states"]mutableCopy];
        PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
        pickerViewController.pickerType = CustomPickerType;
        pickerViewController.dataSourceForCustomPickerType = [arrStates valueForKey:@"state"];
        [pickerViewController setInitialItemAtIndex:0];
        pickerViewController.delegate = self;
        [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
        [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
        
        
    }];
    
}
- (void)didSelectItemAtIndex:(NSUInteger)index{
    self.txtState.text = arrStates[index][@"state"];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) { // Camera
        [self openImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
    } else  if (buttonIndex == 1) { // Photo Gallery
        [self openImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

- (void)openImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = sourceType;
        ipc.allowsEditing = NO;
        [self presentViewController:ipc animated:YES completion:nil];
       
    }];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *tmp = nil;
    if (info[UIImagePickerControllerEditedImage]) {
        tmp = info[UIImagePickerControllerEditedImage];
    } else {
        tmp = info[UIImagePickerControllerOriginalImage];
    }
    
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
            NSURL *urlPath = [info valueForKey:UIImagePickerControllerReferenceURL];
            NSString *urlString = [urlPath absoluteString];
            img1=  [img fixOrientation];
            self.imgProfile.image = img1;

            
        }];

        
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{
      
        
    }];
}


- (IBAction)BackPress:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (void)onUserImageClicked:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Gallery",nil];
    [actionSheet showInView:self.view];
}

@end
