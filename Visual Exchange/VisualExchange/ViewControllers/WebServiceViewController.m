//
//  WebServiceViewController.m
//  Diet Calorie Counter
//
//  Created by User Name on 5/17/13.
//  Copyright (c) 2013 xoomsolutions. All rights reserved.
//

#import "WebServiceViewController.h"
//#import "FacebookData.h"
#import "NSData+Base64.h"

static WebServiceViewController *wsVC =nil;
@interface WebServiceViewController ()
@property (strong, nonatomic) NSURLConnection *connectionManager;
@property (strong, nonatomic) NSMutableData *downloadedMutableData;
@property (strong, nonatomic) NSURLResponse *urlResponse;


@end

@implementation WebServiceViewController
@synthesize strURL,jsonString,strCallHttpMethod,strMethodName,activeDownload,imageConnection;
@synthesize delegate,responseData,strCurrentRequestMethod,connectionManager,downloadedMutableData,urlResponse;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
     // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(WebServiceViewController*) wsVC{
	
	if(!wsVC){
		wsVC = [[WebServiceViewController alloc] init];
	}
	
	return wsVC;
	
}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[NSString stringWithFormat:@"%@",[dictionary objectForKey:key]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [[NSString stringWithFormat:@"%@",key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

//- (NSMutableDictionary *)sendRequestForMethod:(NSString *)strMethod withParameter:(NSMutableDictionary *)dictDetail
//{
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[BSQ_URL stringByAppendingFormat:@"?method=%@",strMethod]]];
//    [request setHTTPMethod:@"POST"];
//    
//    NSData *body = [self encodeDictionary:dictDetail];
//    
//    [request setValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody:body];
//    
//    NSError *error;
//    NSURLResponse *resultResponse;
//    // Perform request and get JSON back as a NSData object
//    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resultResponse error:&error];
//    // Get JSON as a NSString from NSData response
//    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
//    NSLog(@"%@",json_string);
//    
//    NSLog(@"response=%@",json_string);
//   // NSError *jsonError = nil;
//    
//    NSMutableDictionary *dictResult =[NSMutableDictionary dictionaryWithDictionary:[json_string JSONValue]];
//    
//    return dictResult;
//}


#pragma marks --- Uploading images ---

//- (NSMutableDictionary *)sendRequestForMethod:(NSString *)strMethod withParameter:(NSMutableDictionary *)dictDetail withImageArray:(NSMutableArray *)arrImages
//
//{
//    //NSString *strDicDescription = [dictDetail description];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
//    [request setURL:[NSURL URLWithString:[BSQ_URL stringByAppendingFormat:@"?method=%@",strMethod]]];
//    
//    //    [request addValue:@"lat=42.345573&log=-71.098326&userId=12345&phone=408-8327642" forHTTPHeaderField:@"info"];
//    
////    [request addValue:strDicDescription forHTTPHeaderField:@"info"];
//    
//          [request setHTTPMethod:@"POST"];
//    for (int i = 0; i < arrImages.count; i++) {
//        UIImage *image = [arrImages objectAtIndex:i];
//        
//        // encode the image as JPEG
//       NSString *base64EncodedImage = [UIImageJPEGRepresentation(image, 0.8) base64EncodingWithLineLength:0];
//        
//        [dictDetail setObject:base64EncodedImage forKey:[NSString stringWithFormat:@"image%d",i+1]];
//    }
//    NSData *dicData = [self encodeDictionary:dictDetail];
//
//    
//
//        // add the POST data as the request body
//    [request setValue:[NSString stringWithFormat:@"%d", dicData.length] forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody:dicData];
//    
//        
//        // now lets make the connection to the web
//        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//        
//        NSLog(@"returnString %@", returnString);
//        
//      
//        // NSError *jsonError = nil;
//    
//    NSMutableDictionary *dictResult =[NSMutableDictionary dictionaryWithDictionary:[returnString JSONValue]];
//    
//    return dictResult;
//}

- (NSMutableDictionary *)sendReqGoogleApiWith:(NSMutableDictionary *)dictDetail
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:strCallHttpMethod];
    
    NSData *body = [self encodeDictionary:dictDetail];
    
    [request setValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:body];
    
    NSError *error;
    NSURLResponse *resultResponse;
    // Perform request and get JSON back as a NSData object
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resultResponse error:&error];
    // Get JSON as a NSString from NSData response
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"response=%@",json_string);
    // NSError *jsonError = nil;
    
//    NSMutableDictionary *dictResult =[NSMutableDictionary dictionaryWithDictionary:[json_string JSONValue]];
    
    NSMutableDictionary *dictResult =[NSMutableDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil]];
    
    return dictResult;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
     //   UIImage *overlayImage = [self faceOverlayImageFromImage:_image];
        dispatch_async(dispatch_get_main_queue(), ^{ // 2
           // [self fadeInNewImage:overlayImage]; // 3
        });
    });
}

- (NSMutableDictionary *)sendRequestWithParameter:(NSMutableDictionary *)dictDetail
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[strURL stringByAppendingFormat:@"?method=%@",strMethodName]]];
    [request setHTTPMethod:strCallHttpMethod];

    NSData *body = [self encodeDictionary:dictDetail];

    [request setValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:body];

    NSError *error;
    NSURLResponse *resultResponse;
    // Perform request and get JSON back as a NSData object
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resultResponse error:&error];
    // Get JSON as a NSString from NSData response
    //NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];

    //NSLog(@"response api =%@",json_string);
    // NSError *jsonError = nil;
    
    
    if (response == nil) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:[strURL stringByAppendingFormat:@"?method=%@",strMethodName]]];
        [request setHTTPMethod:strCallHttpMethod];
        
        
        //NSLog(@"url of webservice ::%@",request.URL);
        NSData *body = [self encodeDictionary:dictDetail];
        
        [request setValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:body];
        
        NSError *error;
        NSURLResponse *resultResponse;
        // Perform request and get JSON back as a NSData object
        NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resultResponse error:&error];
        // Get JSON as a NSString from NSData response
        //NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        
        //NSLog(@"response api =%@",json_string);
        return nil;
    }

    id  dictResult =[NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil];
   // id  dictResult2 =[NSMutableArray arrayWithArray:[NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil]];

//NSLog(@"response api_dict =%@",dictResult);
//    NSMutableDictionary *dictResult =[NSMutableDictionary dictionaryWithDictionary:[json_string JSONValue]];

   // NSMutableDictionary *dictResult =[NSMutableDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil]];
    return dictResult;
}

+ (NSMutableDictionary *)simpleURLFetch:(NSString * )methodName param:(NSDictionary *)jsonDict
{
    NSString *strURL;
    if ([methodName isEqualToString:@"Address"]) {
        
        strURL=[NSString stringWithFormat:@"%@%@",Place_URL
                ,@"?Key=CJ99-DH15-CC53-BX99&$Top=20&SearchFor=Everything&Country=GBR"];
    }
    else if ([methodName isEqualToString:@"GeoCode"]) {
        strURL=[NSString stringWithFormat:@"%@%@",Geocode_URL
                ,@"?Key=CJ99-DH15-CC53-BX99"];
    }
    else
    {
        strURL=[NSString stringWithFormat:@"%@%@",LatLong_URL
                ,@"?Key=CJ99-DH15-CC53-BX99"];
    }
     
    //NSLog(@"strURL is %@",strURL);
    
    for(int i=0;i<[[jsonDict allKeys]  count];i++)
    {
        
        NSString *strParam=[NSString stringWithFormat:@"&%@=%@",[[jsonDict allKeys] objectAtIndex:i],[jsonDict valueForKey:[[jsonDict allKeys] objectAtIndex:i]]];
        // NSLog(@"strparm is %@",strParam);
        strURL= [strURL stringByAppendingFormat:@"%@",strParam];
    }
    
    //    NSURLRequest *request=[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?method=login&email=%@&password=%@",BaseURL,@"kiran@gmail.com",@"abc"]]];
    //
    NSLog(@"strurl is %@",strURL);
    
    NSURLRequest *request=[NSURLRequest requestWithURL:[NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSError *err1=nil;
    NSData *response=[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&err1];
    
    if(err1)
    {
        NSLog(@"err is %@",err1);
        return nil;
    }
    NSError *jsonParsingError = nil;
    NSMutableDictionary *dictResonse = [NSJSONSerialization JSONObjectWithData:response
                                                                       options:0 error:&jsonParsingError];
    
    return dictResonse;
}

#pragma mark Check Network Connection

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

#pragma mark - NSURLConnection Delegate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"%lld", response.expectedContentLength);
    self.urlResponse = response;
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    if ([delegate respondsToSelector:@selector(webserviceConnection:didFailWithError:)]) {
        [delegate webserviceConnection:connection didFailWithError:error];
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [responseData appendData:data];
    NSLog(@"%.0f%%", ((100.0/self.urlResponse.expectedContentLength)*responseData.length));

}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if ([delegate respondsToSelector:@selector(webserviceConnectionDidFinishLoading:successFullyGotData:currentReqMethod:)]) {
        [delegate webserviceConnectionDidFinishLoading:connection successFullyGotData:responseData currentReqMethod:strCurrentRequestMethod];
    }
}

- (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    if ([delegate respondsToSelector:@selector(webserviceconnection:didSendBodyData:totalBytesWritten:totalBytesExpectedToWrite:)]) {
        [delegate webserviceconnection:connection didSendBodyData:bytesWritten totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
    }

}

-(void)PostData:(NSMutableDictionary *)dictDetail
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[strURL stringByAppendingFormat:@"?method=%@",strMethodName]]];
    [request setHTTPMethod:strCallHttpMethod];
    
    NSData *body = [self encodeDictionary:dictDetail];
    
    [request setValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:body];
      
    responseData = [NSMutableData data];
    
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    [connection start];
}
-(BOOL)isValidEmail:(NSString *)strEmail{
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:strEmail]) {
        aRange = [strEmail rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [strEmail length])];
        int indexOfDot = (int) aRange.location;
        
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [strEmail substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    
    return FALSE;
    
}

@end
