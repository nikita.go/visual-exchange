//
//  CartCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 23/04/1938 Saka.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblCardStock;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imglogo;
-(void)setDidtapBlock:(void(^)(UIButton *button))compition;
@property(strong,nonatomic)void(^didtapBlock)(UIButton *button);
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@end
