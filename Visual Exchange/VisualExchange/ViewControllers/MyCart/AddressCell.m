//
//  AddressCell.m
//  VisualExchange
//
//  Created by Nilay Shah on 25/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell

- (void)awakeFromNib {
    // Initialization code

    self.btnEdit.hidden = true;
    self.btnlarge.hidden = true;
    self.lblHeader.hidden = true;
    self.lblAddress.hidden = true;
    self.btnCheckbox.hidden = true;
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)prepareForReuse{
    
    self.btnEdit.hidden = true;
    self.btnlarge.hidden = true;
    self.lblHeader.hidden = true;
    self.lblAddress.hidden = true;
    self.btnCheckbox.hidden = true;

    

}

- (IBAction)EditPress:(id)sender {
    if(self.didtapBlock){
        self.didtapBlock(sender);
    }
}
@end
