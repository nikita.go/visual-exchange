//
//  MyCartVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "MyCartVC.h"
#import "CartSecondViewController.h"
#import "LoginVC.h"
#define kPayPalEnvironment PayPalEnvironmentNoNetwork

@interface MyCartVC (){
    NSMutableArray *arrayCart;
    NSString *promoDiscount;
    NSMutableArray *arrayadress;
    NSInteger tagadress;
    NSInteger tagpresent;
    NSInteger payment;
    NSInteger orderReferenceId;
    NSMutableDictionary * dictData;
    NSMutableDictionary *arrayIshide ;
}
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@end

@implementation MyCartVC
-(void)PaypalController{
      arrayIshide = [NSMutableDictionary dictionary];
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    _payPalConfig.acceptCreditCards = YES;
    
    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionBoth;
    
    
    
    self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    
    
    [PayPalMobile preconnectWithEnvironment:self.environment];
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
}


- (void)viewDidLoad {
    [super viewDidLoad];
     [self playerDidPause];
   // [self.videoPlayerViewController.moviePlayer stop];
    

      [DLHelper AppDelegate].tabbarController.tabBar.hidden =false;
    
    _tableview.backgroundColor = [UIColor clearColor];
    _tableview.opaque = NO;
    _tableview.backgroundView = nil;
    tagadress = 555555;
    
    UINib *nibProduct = [UINib nibWithNibName:NSStringFromClass([CartCell class]) bundle:nil];
    [self.tableview registerNib:nibProduct forCellReuseIdentifier:@"CartCell"];
//    
//    UINib *nibPayment = [UINib nibWithNibName:NSStringFromClass([PaymentCell class]) bundle:nil];
//    [self.tableview registerNib:nibPayment forCellReuseIdentifier:@"PaymentCell"];
//  
    
    
    UINib *nibPayment1 = [UINib nibWithNibName:NSStringFromClass([NewCartTableViewCell class]) bundle:nil];
    [self.tableview registerNib:nibPayment1 forCellReuseIdentifier:@"NewCartTableViewCell"];
    
    
//    UINib *nibaddress = [UINib nibWithNibName:NSStringFromClass([AddressCell class]) bundle:nil];
//    [self.tableview registerNib:nibaddress forCellReuseIdentifier:@"AddressCell"];
//    [self PaypalController];
    
    
//    if (!kUserDefults_(ResultUser)){
//        [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
//            if(DLindex==0)
//                return;
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
//            vc.hidesBottomBarWhenPushed = true;
//            [[DLHelper AppDelegate].tabbarController.selectedViewController pushViewController:vc animated:YES];
//            
//        } title:nil message:@"You need to Login/Sign up frist" canceltile:@"CANCEL" othertitle:@"OK", nil];
//        
//        [DLHelper shareinstance]. selectedIndex = 2;
//        return;
//    }
//    
    
}
- (void)playerDidPause
{
    [self.videoPlayerViewController.moviePlayer stop];
    // playerView = [[GUIPlayerView alloc] init];
    //  [playerView stop];
    //[self.videoContainerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // [playerView seekToTime:kCMTimeZero];
    
    // [playButton setSelected:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    
    
//    if (!kUserDefults_(ResultUser)){
//               return;
//    }
 
    [DLHelper shareinstance]. selectedIndex = 4;

    
    if(tagpresent!=10000){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
        if(custid.length==0)
        {
              [dict setObject:@"0" forKey:@"customer_id"];
        }
        else{
        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        }
      
        NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
        
        
        
        NSString * deviceTokenString = [[[[myid description]
                                          stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                         stringByReplacingOccurrencesOfString: @">" withString: @""]
                                        stringByReplacingOccurrencesOfString: @" " withString: @""];
        
        NSLog(@"The generated device token string is : %@",deviceTokenString);
        if(deviceTokenString.length==0)
        {
            [dict setObject:@"f37481a95690b8b5fc01dc5aa59a24f7153119eda8a5f1d91e35ed2c61d6cc28" forKey:@"device_id"];
        }
        else{
            [dict setObject:deviceTokenString forKey:@"device_id"];
        }

        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cartlist" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            arrayCart = [JSON[@"cartList"]mutableCopy];
            NSString *str =  [NSString stringWithFormat:@"%lu",(unsigned long)arrayCart.count];
             self.lblNoItemFound.hidden = true;
            if(arrayCart.count ==0){
                 self.lblNoItemFound.hidden = false;
            }
            
            NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
            dict[@"total_product"] = str;
            kUserDefults(dict.mutableCopy, ResultUser);
            
            
            kUserDefults(str, @"total_product");
          //  NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
             NSInteger count = [str integerValue];
            [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%ld",(long)count]:nil;

            
            arrayadress = [JSON[@"address"]mutableCopy];
            
            dictData  =[JSON mutableCopy];
            [self.tableview reloadData];
            if(arrayCart.count>0)
            {
           // self.tableview.frame = CGRectMake(self.tableview.frame.origin.x, self.tableview.frame.origin.y, self.tableview.frame.size.width, _tableview.contentSize.height+_tableview.frame.size.height+100
                                              
                                              
                                             // );
            }
            else{
                
            }
            
            
        }];
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mart - UITableview Delegate -
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
    if(custid.length==0)
    {
        if(!arrayCart.count)
        {
            return 0;
        }
        else{
         return 3;
        }
        
    }
    else{

    if(!arrayCart.count){
        
        return 0;
    }
    
    return 3;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
    if(custid.length==0)
    {
        if(section==0)
        {
            
            
            return arrayCart.count;
        }
        else if(section==2)
        {
            return 1;
       
        } else if(section==1)
        {
        return 0;
        
        
    
    }
    }
    else{
    if(arrayCart.count){
       
        if(section==0){
            
            
             return arrayCart.count;
        }
        else if(section==2)
        {
            return 1;
            
        } else if(section==1)
        {
            return 0;
            
            
            
        }
    }
        
//        else if(section==1)
//            return arrayadress.count+2;
//        return 1;
//    }else{
//               return 0;
//
//
//    }
//    }
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.section == 0){
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        __weak typeof(cell) weakcell = cell;
        [cell setDidtapBlock:^(UIButton *button) {
            
            if(button.tag ==3)
            {
                
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete product?" preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alertController animated:YES completion:nil];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                           
                {
                    
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
                    if(custid.length==0)
                    {
                        [dict setObject:@"0" forKey:@"customer_id"];
                       
                    }
                    else{
                        
                        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
                    }
                    NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
                    
                    
                    
                    NSString * deviceTokenString = [[[[myid description]
                                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
                    
                    NSLog(@"The generated device token string is : %@",deviceTokenString);
                    if (deviceTokenString.length>0)
                    {
                        [dict setObject:deviceTokenString forKey:@"device_id"];
                        
                    }
                    else{
                        [dict setObject:@"f37481a95690b8b5fc01dc5aa59a24f7153119eda8a5f1d91e35ed2c61d6cc28" forKey:@"device_id"];
                    }

                    
                    dict[@"cart_id"] = arrayCart[indexPath.row][@"cart_id"];
                    dict[@"address_id"] = @"0";
                    if (tagadress != 555555)
                    dict[@"address_id"] = arrayadress[tagadress-2][@"customer_address_id"];
                    dict[@"couponId"] = dictData [@"couponId"];
                    NSMutableArray  *cartary=[[NSMutableArray alloc]init];
                   cartary=[[NSUserDefaults standardUserDefaults]objectForKey:@"cartary"];
                    NSString *cartid=[NSString stringWithFormat:@"%@",arrayCart[indexPath.row][@"cart_id"]];
                    NSMutableArray *cart1ary=[cartary mutableCopy];
                    for (int  i=0; i<cart1ary.count; i++) {
                        if([[cart1ary objectAtIndex:i]isEqualToString:cartid])
                           {
                               [cart1ary removeObjectAtIndex:i];
                           }
                    }
                    if(cart1ary.count==0)
                    {
                        
                    }
                    else{
                    [[NSUserDefaults standardUserDefaults]setObject:cart1ary forKey:@"cartary"];
                    }
                    
                    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cart_delete" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                        
                        
                        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                        NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
                        if(custid.length==0)
                        {
                            [dict setObject:@"0" forKey:@"customer_id"];
                        }
                        else{
                            dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
                        }
                        
                        NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
                        
                        
                        
                        NSString * deviceTokenString = [[[[myid description]
                                                          stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                                         stringByReplacingOccurrencesOfString: @">" withString: @""]
                                                        stringByReplacingOccurrencesOfString: @" " withString: @""];
                        
                        NSLog(@"The generated device token string is : %@",deviceTokenString);
                        if(deviceTokenString.length==0)
                        {
                            [dict setObject:@"f37481a95690b8b5fc01dc5aa59a24f7153119eda8a5f1d91e35ed2c61d6cc28" forKey:@"device_id"];
                        }
                        else{
                        [dict setObject:deviceTokenString forKey:@"device_id"];
                        }

                        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cartList.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                            arrayCart = [JSON[@"cartList"]mutableCopy];
                            NSString *str =  [NSString stringWithFormat:@"%lu",(unsigned long)arrayCart.count];
                            
                            self.lblNoItemFound.hidden = true;
                            if(arrayCart.count ==0){
                                self.lblNoItemFound.hidden = false;
                            }
                            
                            NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
                            dict[@"total_product"] = str;
                            kUserDefults(dict.mutableCopy, ResultUser);
                            
                            
                            kUserDefults(str, @"total_product");
                            NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
                            [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%ld",(long)count]:nil;
                            
                            
                            arrayadress = [JSON[@"address"]mutableCopy];
                            
                            dictData  =[JSON mutableCopy];
                            [self.tableview reloadData];
                            
                            
                        }];

                
                        return ;
                        
                    }];

                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }]];
                

//                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
//                dict[@"cart_id"] = arrayCart[indexPath.row][@"cart_id"];
//                dict[@"address_id"] = @"0";
//                if (tagadress != 555555)
//                    dict[@"address_id"] = arrayadress[tagadress-2][@"customer_address_id"];
//                    dict[@"couponId"] = dictData [@"couponId"];
//                
//                [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cart_delete" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
//                    
//                    
//                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                    dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
//                    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cartList.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
//                        arrayCart = [JSON[@"cartList"]mutableCopy];
//                        NSString *str =  [NSString stringWithFormat:@"%lu",(unsigned long)arrayCart.count];
//                        
//                        self.lblNoItemFound.hidden = true;
//                        if(arrayCart.count ==0){
//                            self.lblNoItemFound.hidden = false;
//                        }
//
//                        NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
//                        dict[@"total_product"] = str;
//                        kUserDefults(dict.mutableCopy, ResultUser);
//                        
//                        
//                        kUserDefults(str, @"total_product");
//                        NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
//                        [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%ld",(long)count]:nil;
//                        
//                        
//                        arrayadress = [JSON[@"address"]mutableCopy];
//            
//                        dictData  =[JSON mutableCopy];
//                        [self.tableview reloadData];
//                        
//                        
//                    }];

//                    NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
//                    dict[@"total_product"] = JSON[@"total_product"];
//                    kUserDefults(dict.mutableCopy, ResultUser);
//                    
//                    NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
//                    [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%ld",(long)count]:nil;
//
//                    [arrayCart removeObjectAtIndex:indexPath.row];
//                    dictData = JSON.mutableCopy;
//                    [self.tableview reloadData];
                
               // return ;
            }
            else if(button.tag == 4){
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                dict[@"cart_id"] = arrayCart[indexPath.row][@"cart_id"];
                dict[@"qty"] = arrayCart[indexPath.row][@"qty"];
                 dict[@"address_id"] = @"0";
                if (tagadress != 555555)
                    dict[@"address_id"] = arrayadress[tagadress-2][@"customer_address_id"];
                NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
               
                if(custid.length==0)
                {
                    [dict setObject:@"0" forKey:@"customer_id"];
                }
                else{
                    dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
                }
                
                NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
                
                
                
                NSString * deviceTokenString = [[[[myid description]
                                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                                stringByReplacingOccurrencesOfString: @" " withString: @""];
                
                NSLog(@"The generated device token string is : %@",deviceTokenString);
                if(deviceTokenString.length==0)
                {
                    [dict setObject:@"f37481a95690b8b5fc01dc5aa59a24f7153119eda8a5f1d91e35ed2c61d6cc28" forKey:@"device_id"];
                }
                else{
                    [dict setObject:deviceTokenString forKey:@"device_id"];
                }

               
                dict[@"couponId"] = dictData [@"couponId"];
                
                [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cart_update" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                    
                    dictData = JSON.mutableCopy;
                    [self.tableview reloadData];
                    arrayIshide[arrayCart[indexPath.row][@"cart_id"]] = @"0";
                    
                }];
                
            }
            else if(button.tag==1)
            {
               // NSString *actualcount=[NSString stringWithFormat:@"%ld",[arrayCart[indexPath.row][@"qty"]integerValue]];
              //  NSString *actualcount=weakcell.lblQuantity.text;
                NSString *count = [NSString stringWithFormat:@"%ld",[arrayCart[indexPath.row][@"qty"]integerValue]+1 ];
                arrayCart[indexPath.row][@"qty"] = count;

//                if(![actualcount isEqualToString:count])
//                {
//                    NSString *limit=[NSString stringWithFormat:@"You can choose maximum %@ quantity.", arrayCart[indexPath.row][@"qty"]];
//
//                                       UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:limit delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//                                        [objalert show];
//                }
//                else{
                   //  NSString *count = [NSString stringWithFormat:@"%ld",[arrayCart[indexPath.row][@"qty"]integerValue]+1 ];
                     weakcell.btnSave.hidden = false;
                    
              //  }
//                if([actualcount integerValue] <[count integerValue])
//                {
//                    NSString *limit=[NSString stringWithFormat:@"You can choose maximum %@ quantity.", arrayCart[indexPath.row][@"qty"]];
//                   // weakcell.lblQuantity.text =[NSString stringWithFormat:@"%ld",[arrayCart[indexPath.row][@"qty"]integerValue]];
//                    weakcell.btnSave.hidden = true;
//                     arrayCart[indexPath.row][@"qty"] = actualcount;
//
//                    UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:limit delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//                    [objalert show];
//                                 }
//                else
//                {
             //   arrayCart[indexPath.row][@"qty"] = count;
              //  weakcell.lblQuantity.text =count;
                 //   weakcell.btnSave.hidden = false;


               // }
                //arrayIshide[arrayCart[indexPath.row][@"cart_id"]] = @"1";
                
                

                
                
            }
            else
            {
                NSString *count = [NSString stringWithFormat:@"%ld",[arrayCart[indexPath.row][@"qty"]integerValue]-1 ];
//                if(count==0)
//                {
//                    
//                    UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Select atleast one item" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//                    [objalert show];
//                }
//                else
//                {
                    arrayCart[indexPath.row][@"qty"] = count;
                //}
                weakcell.btnSave.hidden = false;
               arrayIshide[arrayCart[indexPath.row][@"cart_id"]] = @"1";
            }
            
            
        }];
        [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:cell.imglogo urlString:arrayCart[indexPath.row][@"product_image"] Complete:^(UIImage *image) {
            
        }];
        
        cell.lblProductName.text = arrayCart[indexPath.row][@"product_name"];
        NSString *str= arrayCart[indexPath.row][@"category_name"];
        if([str isEqual:[NSNull null]] || str==nil || str.length==0)
        {
            cell.lblCardStock.text =@"";

        }
        else{
            cell.lblCardStock.text =str;
        }
       // cell.lblPrice.text = [NSString stringWithFormat:@"USD %@",arrayCart[indexPath.row][@"price"]];
         cell.lblPrice.text = [NSString stringWithFormat:@"USD %@",arrayCart[indexPath.row][@"total"]];
        cell.lblQuantity.text = arrayCart[indexPath.row][@"qty"];
    
//        BOOL ishide = [arrayIshide[indexPath.row]boolValue];
        
        if([arrayIshide[arrayCart[indexPath.row][@"cart_id"]] isEqualToString:@"1"]) {
            
             cell.btnSave.hidden = false;
        }else{
            cell.btnSave.hidden = true;

        }
       
        

        return cell;
    }else if (indexPath.section==1)
    {
        
        
        AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell" forIndexPath:indexPath];
        
        cell.lblHeader.text = @"";
        if(indexPath.row==0){
            cell.lblHeader.text = @"  ADDRESS";
            cell.lblHeader.hidden = false;
            [[cell viewWithTag:525] TopRedius:4];
        }
        else if(indexPath.row==1){
            cell.lblAddress.text = @"Add New Address";
            cell.btnlarge.hidden = false;
            cell.lblAddress.hidden =false;
            [cell setDidtapBlock:^(UIButton *button) {
                
                AddAdressVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAdressVC"];
                vc.isNew = true;
                [self.navigationController pushViewController:vc animated:YES];
                
            }];
            
        }else{
            cell.btnEdit.hidden = false;
            cell.lblAddress.hidden = false;
            cell.btnCheckbox.hidden = false;
            NSString *address = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",arrayadress[indexPath.row-2][@"address"],arrayadress[indexPath.row-2][@"city"],arrayadress[indexPath.row-2][@"state"],arrayadress[indexPath.row-2][@"country"],arrayadress[indexPath.row-2][@"zipcode"]];
            cell.lblAddress.text = address;
            
            
            [cell setDidtapBlock:^(UIButton *button) {
                
                AddAdressVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAdressVC"];
                vc.isNew = false;
                vc.result = [arrayadress[indexPath.row-2]mutableCopy];
                [self.navigationController pushViewController:vc animated:YES];
                
            }];
            
        }
        
        if(tagadress==indexPath.row)
        {
            
            cell.lblHeader.backgroundColor = [UIColor specSheetGreen];
            cell.lblAddress.textColor = [UIColor whiteColor];
            [cell.btnEdit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.btnCheckbox.selected = true;
            
        }else{
            
            cell.lblHeader.backgroundColor = [UIColor clearColor];
            cell.lblAddress.textColor = [UIColor darkGrayColor];
            [cell.btnEdit setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            cell.btnCheckbox.selected = false;
        }
        
        cell.lblHeader.hidden = false;
        //cell.lblHeader.text = @"";
        cell.backgroundColor = [UIColor clearColor];
        return cell;
        
    }
    
    else if (indexPath.section==2)
    {
       // PaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
        NewCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewCartTableViewCell" forIndexPath:indexPath];
        cell.continuebtn.layer.cornerRadius =cell.continuebtn.frame.size.height/2;
        cell.continuebtn.layer.masksToBounds=YES;

        [cell.continuebtn addTarget:self action:@selector(OrderPlaced) forControlEvents:UIControlEventTouchUpInside];

        
//        NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
//        if(custid.length==0)
//        {
//            cell.labcontinuetn.frame=CGRectMake(cell.labcontinuetn.frame.origin.x, cell.labcontinuetn.frame.origin.y, cell.labcontinuetn.frame.size.width, 40);
//            cell.labcontinuetn.layer.cornerRadius =cell.labcontinuetn.frame.size.height/2;
//           // cell.labcontinuetn.layer.masksToBounds=YES;
//
//              [cell.labcontinuetn setTitle:@"CHECK OUT" forState:UIControlStateNormal];
//             cell.contentView.backgroundColor = [UIColor clearColor];
//            cell.bootomviewlbkl.hidden=YES;
//            cell.lblshipping.hidden=YES;
//            cell.lblShiping.hidden=YES;
//            cell.lbltax.hidden=YES;
//           cell.lbltotal.hidden=YES;
//             cell.lblamount.hidden=YES;
//             cell.lblmainpay.hidden=YES;
//             cell.lblmaintax.hidden=YES;
//            cell.lblpromotion.hidden=YES;
//            cell.lblpromocode.hidden=YES;
//            cell.labcontinuetn.hidden=NO;
//            cell.lblmainpromotion.hidden=YES;
//            cell.txtPromocode.hidden=YES;
//            cell.btnApply.hidden=YES;
//            cell.upperview.hidden=YES;
//            [cell setDidtapBlock:^(UIButton *button) {
//                
//                if(button.tag==1){
//                    [self OrderPlaced];
//                    return ;
//                }
//            }];
//
//            return cell;
//
//        }
//        else{
//            cell.labcontinuetn.frame=CGRectMake(cell.labcontinuetn.frame.origin.x, cell.labcontinuetn.frame.origin.y, cell.labcontinuetn.frame.size.width, 40);
//
//            cell.labcontinuetn.layer.cornerRadius =cell.labcontinuetn.frame.size.height/2;
//           // cell.labcontinuetn.layer.masksToBounds=YES;
//            [cell.labcontinuetn setTitle:@"CHECK OUT" forState:UIControlStateNormal];
//
//            cell.contentView.backgroundColor = [UIColor clearColor];
//            cell.bootomviewlbkl.hidden=YES;
//            cell.lblshipping.hidden=YES;
//            cell.lblShiping.hidden=YES;
//            cell.lbltax.hidden=YES;
//            cell.lbltotal.hidden=YES;
//            cell.lblamount.hidden=YES;
//            cell.lblmainpay.hidden=YES;
//            cell.lblmaintax.hidden=YES;
//            cell.lblpromotion.hidden=YES;
//            cell.lblpromocode.hidden=YES;
//            cell.labcontinuetn.hidden=NO;
//            cell.lblmainpromotion.hidden=YES;
//            cell.txtPromocode.hidden=YES;
//            cell.btnApply.hidden=YES;
//            cell.upperview.hidden=YES;
//            [cell setDidtapBlock:^(UIButton *button) {
//                
//                if(button.tag==1){
//                    [self OrderPlaced];
//                    return ;
//                }
//            }];
//            
            return cell;
       // }
        

//        __weak typeof(cell) weakcell = cell;
//        [cell setDidtapBlock:^(UIButton *button) {
//            
//            if(button.tag==1){
//                [self OrderPlaced];
//                return ;
//            }
//            if([weakcell.btnApply.titleLabel.text isEqualToString:@"Delete"]){
//                
//                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                dict[@"couponId"] = dictData[@"couponId"];
//                if (tagadress != 555555)
//                    dict[@"address_id"] = arrayadress[tagadress-2][@"customer_address_id"];
//                if (dict[@"address_id"] == nil){
//                    dict[@"address_id"] = @"0";
//                }
//                dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];;
//                dict[@"cart_id"] = [NSString stringWithFormat:@"%@",dictData [@"total"]];
//                [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"check_coupon" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
//                    [weakcell.btnApply setTitle:@"Delete" forState:UIControlStateNormal];
//                    [self showAlertWithTitle:nil message:@"Promo code Apply."];
//                    dictData = JSON.mutableCopy;
//                    [self.tableview reloadData];
//                    
//                }];
//
//                
//            }else{
//            
//            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//            dict[@"couponCode"] = weakcell.txtPromocode.text;
//            if (tagadress != 555555)
//            dict[@"address_id"] = arrayadress[tagadress-2][@"customer_address_id"];
//            if (dict[@"address_id"] == nil){
//                 dict[@"address_id"] = @"0";
//            }
//            dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];;
//          //  dict[@"total"] = [NSString stringWithFormat:@"%@",dictData [@"total"]];
//            [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"check_coupon" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
//                [weakcell.btnApply setTitle:@"Delete" forState:UIControlStateNormal];
//                [self showAlertWithTitle:nil message:@"Promo code Apply."];
//                dictData = JSON.mutableCopy;
//                [self.tableview reloadData];
//                
//            }];
//            }
//            
//        }];
//        
//        
//        [[cell.contentView viewWithTag:526] TopRedius:4];
//        for(NSString *key in dictData.allKeys){
//            if ([dictData[key] isKindOfClass:[NSString class]]){
//                NSString *str = dictData[key];
//                str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
//                dictData[key] = str;
//            }
//            
//        }
//
//        if(dictData[@"cartTotProductValue"]==nil)
//        {
//             cell.lblshipping.text=@"Shipping";
//            cell.lbltotalprice.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"total"]floatValue]];
//             // cell.lbltotalprice.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"finalAmount"]floatValue]];
//            cell.lblpromocode.text = [NSString stringWithFormat:@"- $%.2f",[dictData[@"promotion"]floatValue]];
//            cell.lblShiping.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"shipping"]floatValue]];
//            cell.lbltax.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"tax"]floatValue]];
//            cell.lblamount.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"to pay"]floatValue]];
//        }else{
//            cell.lblshipping.text=@"Flat_shiprate";
//            cell.lbltotalprice.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"cartTotProductValue"]floatValue]];
//            cell.lblpromocode.text = [NSString stringWithFormat:@"- $%.2f",[dictData[@"totalCartPrdDisAmt"]floatValue]];
//            cell.lblShiping.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"totalShipAmt"]floatValue]];
//            cell.lbltax.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"totalTaxAmount"]floatValue]];
//            cell.lblamount.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"finalAmount"]floatValue]];
//        }
//        }
//       
       
         }
    
    return nil;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==1 && indexPath.row>1)
    
    {
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        dict[@"address_id"] = arrayadress[indexPath.row-2][@"customer_address_id"];
        dict[@"couponId"] = dictData [@"couponId"];
        
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"check_address" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            dictData = JSON.mutableCopy;
            dictData[@"to pay"] = JSON[@"finalAmount"];
            tagadress = indexPath.row;
            [self ManageShipingPayment];
            [tableView reloadData];

        }];
        

       
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0){
        return 130 * SCREEN_WIDTH/320 ;
    }
    if(indexPath.section==2)
        return 45;
        //return 350*SCREEN_WIDTH/320;
    //if(indexPath.section==2)
    //   return 0;
    return 40*SCREEN_WIDTH/320;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==1)
        return 10;
    return 0.1;
}
-(NSMutableDictionary *)ManageShipingPayment{
    
    for(NSString *key in dictData.allKeys){
        if ([dictData[key] isKindOfClass:[NSString class]]){
            NSString *str = dictData[key];
            str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
            dictData[key] = str;
        }
        
    }
    
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:3];
    PaymentCell *cell = [self.tableview cellForRowAtIndexPath:indexpath];
         NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [cell.labcontinuetn setTitle:@"CHECK OUT" forState:UIControlStateNormal];
    if (dictData[@"cartTotProductValue"]==nil)
    {
    
    cell.lbltotalprice.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"total"]floatValue]];
    cell.lblpromocode.text = [NSString stringWithFormat:@"- $%.2f",[dictData[@"promotion"]floatValue]];
    cell.lblShiping.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"shipping"]floatValue]];
    cell.lbltax.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"tax"]floatValue]];
    cell.lblamount.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"to pay"]floatValue]];
    
   
    dict[@"amount"] = [NSString stringWithFormat:@"%.2f",[dictData[@"total"]floatValue]];
    dict[@"shipping_amount"] = [NSString stringWithFormat:@"%.2f",[dictData[@"shipping"]floatValue]];
    dict[@"tax"] = [NSString stringWithFormat:@"%.2f",[dictData[@"tax"]floatValue]];
    dict[@"total_amount"] = [NSString stringWithFormat:@"%.2f",[dictData[@"to pay"]floatValue]];
    dict[@"weight"] = @"0";
    dict[@"pay_status"] = @"0";
    dict[@"weight"] = @"0";
         cell.lblshipping.text=@"Shipping";
    }
    else
    {
        
        
       
        cell.lbltotalprice.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"cartTotProductValue"]floatValue]];
        cell.lblpromocode.text = [NSString stringWithFormat:@"- $%.2f",[dictData[@"totalCartPrdDisAmt"]floatValue]];
        cell.lblShiping.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"totalShipAmt"]floatValue]];
        cell.lbltax.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"orderTaxAmount"]floatValue]];
        cell.lblamount.text = [NSString stringWithFormat:@"$%.2f",[dictData[@"finalAmount"]floatValue]];
        cell.lblshipping.text=@"Flat_shiprate";
         dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
         dict[@"couponId"] = dictData[@"couponId"];
        
        dict[@"amount"] = [NSString stringWithFormat:@"%.2f",[dictData[@"cartTotProductValue"]floatValue]];
        dict[@"shipping_amount"] = [NSString stringWithFormat:@"%.2f",[dictData[@"totalShipAmt"]floatValue]];
        dict[@"tax"] = [NSString stringWithFormat:@"%.2f",[dictData[@"orderTaxAmount"]floatValue]];
        dict[@"total_amount"] = [NSString stringWithFormat:@"%.2f",[dictData[@"finalAmount"]floatValue]];
        dict[@"weight"] = dictData[@"orderTaxAmount"];
        dict[@"orderTaxAmount"] = @"0";
        dict[@"promocode"] = dictData[@"totalCartPrdDisAmt"];

        
    }
    return dict;
    
    
}

- (IBAction)MenuPress:(id)sender {
    [[AppLauncher sharedInstance]openLeftDrawer];
}


- (IBAction)pay {
    if (!kUserDefults_(ResultUser)){
        [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
            if(DLindex==0)
                return;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            vc.hidesBottomBarWhenPushed = true;
            [[DLHelper AppDelegate].tabbarController.selectedViewController pushViewController:vc animated:YES];
            return;
        } title:nil message:@"You need to Login/Sign up frist" canceltile:@"CANCEL" othertitle:@"OK", nil];
        
        return;
    }
    

    
    tagpresent=10000;
    self.resultText = nil;
    
    NSMutableArray *items = [NSMutableArray array];
    
    for(int i=0; i<arrayCart.count; i++){
        
        PayPalItem *item = [PayPalItem itemWithName:arrayCart[i][@"product_name"]
                                       withQuantity:[arrayCart[i][@"qty"]integerValue]
                                          withPrice:[NSDecimalNumber decimalNumberWithString:arrayCart[i][@"price"]]
                                       withCurrency:@"USD"
                                            withSku:@"Hip-00037"];
        [items addObject:item];
        
    }
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items.mutableCopy];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.0"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.0"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *pay = [[PayPalPayment alloc] init];
    pay.amount = total;
    pay.currencyCode = @"USD";
    pay.shortDescription = @"Visual Exchange";
    pay.items = items;  // if not including multiple items, then leave payment.items as nil
    pay.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!pay.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    
    self.payPalConfig.acceptCreditCards = true;
    [[IQKeyboardManager sharedManager]setEnable:false];
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:pay
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    self.resultText = [completedPayment description];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:^{
        
        [[IQKeyboardManager sharedManager]setEnable:true];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"order_id"] = [NSString stringWithFormat:@"%ld",(long)payment];
         dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
         dict[@"status"] = @"1";
         dict[@"orderReferenceId"] = [NSString stringWithFormat:@"%ld",(long)orderReferenceId];
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"update_order" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            
            NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
            dict[@"total_product"] = @"0";
            kUserDefults(dict.mutableCopy, ResultUser);
            NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
            [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%ld",(long)count]:nil;
            [[AppLauncher sharedInstance] setTabbarSelectedIndex:0];
            tagpresent = 0;
            
            
        }];
        
        
    }];
    
    
    
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
  
    NSLog(@"%@",paymentViewController.description);
    self.resultText = nil;
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        tagpresent = 0;
        [[IQKeyboardManager sharedManager]setEnable:true];
        
        [[IQKeyboardManager sharedManager]setEnable:true];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
         dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        dict[@"order_id"] = [NSString stringWithFormat:@"%ld",(long)payment];
        dict[@"status"] = @"3";
                dict[@"orderReferenceId"] = [NSString stringWithFormat:@"%ld",(long)orderReferenceId];
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"update_order" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            
            
            
            
        }];

        
    }];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}


#pragma mark - Flipside View Controller

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pushSettings"]) {
        [[segue destinationViewController] setDelegate:(id)self];
    }
}
-(void)OrderPlaced{
    
    if(tagadress==555555){
        
        NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
        if(custid.length==0)
        {
            LoginVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            //vc.cartpassary = arrayCart;
            [self.navigationController pushViewController:vc animated:YES];
            // [self showErrorWithMessage:dlkloginsignup];
        }
        else{
            CartSecondViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CartSecondViewController"];
            NSUserDefaults *default3=[NSUserDefaults standardUserDefaults];
            [default3 removeObjectForKey:@"cartry"];
            [default3 synchronize];

            [default3 setObject:arrayCart forKey:@"cartry"];
             [default3 synchronize];
            vc.cartpassary = arrayCart;
            [self.navigationController pushViewController:vc animated:YES];

        //[self showErrorWithMessage:dlkCardAddress];
        }
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict = [[self ManageShipingPayment]mutableCopy];
    dict[@"address_id"] = arrayadress[tagadress-2][@"customer_address_id"];
   
    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"add_order" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        payment = [JSON[@"order_id"]integerValue];
        orderReferenceId = [JSON[@"orderReferenceId"]integerValue];
        
       
        [self pay];
        
    }];

    

    
}
@end
