//
//  MyCartVC.h
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "BaseVC.h"
#import "PrefixHeader.pch"
#import "GUIPlayerView.h"
#import "NewCartTableViewCell.h"
@interface MyCartVC : BaseVC<UITableViewDataSource,UITableViewDelegate>
{
    NSString *checkstr;
    CartCell *cell;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) IBOutlet UIButton *payNowButton;
@property(nonatomic, strong, readwrite) IBOutlet UIButton *payFutureButton;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;
@property (weak, nonatomic) IBOutlet UILabel *lblNoItemFound;
@property (nonatomic, strong) XCDYouTubeVideoPlayerViewController *videoPlayerViewController;
@property (weak, nonatomic) id<GUIPlayerViewDelegate> delegate;

@end
