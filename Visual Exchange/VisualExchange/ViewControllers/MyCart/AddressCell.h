//
//  AddressCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 25/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
-(void)setDidtapBlock:(void(^)(UIButton *button))compition;
@property(strong,nonatomic)void(^didtapBlock)(UIButton *button);
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnlarge;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@end
