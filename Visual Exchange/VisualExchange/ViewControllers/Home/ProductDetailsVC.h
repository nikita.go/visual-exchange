//
//  ProductDetailsVC.h
//  VisualExchange
//
//  Created by Minhaz on 7/3/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "BaseVC.h"
#import "PrefixHeader.pch"
#import "GUIPlayerView.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ProductDetailsVC : BaseVC
{
    UIButton *btn;
    NSString *checkstr;
}

@property (nonatomic) NSDictionary *product;
@property(strong,nonatomic)MPMoviePlayerController *player;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollImages;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;
@property (nonatomic, weak) IBOutlet UIView *videoContainerView;
@property (nonatomic, strong) XCDYouTubeVideoPlayerViewController *videoPlayerViewController;
@property (nonatomic, strong) IBOutlet UIView *CouponView;
- (IBAction)onTapCouponCodeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *codediscount;
@property (weak, nonatomic) id<GUIPlayerViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *productdetailview;

@end
