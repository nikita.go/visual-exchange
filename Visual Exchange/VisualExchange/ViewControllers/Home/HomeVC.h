//
//  HomeVC.h
//  VisualExchange
//
//  Created by Minhaz on 6/20/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "HomeSubCategoriesDelegate.h"
#import "SearchCatagoryVC.h"

@interface HomeVC : BaseVC

@property (nonatomic) id <HomeSubCategoriesDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (nonatomic, strong) XCDYouTubeVideoPlayerViewController *videoPlayerViewController;
@end
