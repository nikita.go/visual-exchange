//
//  HomeSubCatListView.h
//  VisualExchange
//
//  Created by Minhaz on 6/26/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
@interface HomeSubCatListView : BaseVC

@property (nonatomic, strong) IBOutlet UITableView *theTableView;

@end
