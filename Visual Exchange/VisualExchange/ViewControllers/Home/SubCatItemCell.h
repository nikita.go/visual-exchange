//
//  SubCatItemCell.h
//  VisualExchange
//
//  Created by Minhaz on 6/26/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCatItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewItem;
@property (weak, nonatomic) IBOutlet UILabel *lblItemTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPrice;

@end
