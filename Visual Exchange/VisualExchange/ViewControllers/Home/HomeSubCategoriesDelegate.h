//
//  HomeSubCategoriesDelegate.h
//  VisualExchange
//
//  Created by Minhaz on 6/29/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HomeSubCategoriesDelegate <NSObject>
- (void)homeSubCategoriesFound:(NSArray *)items;
- (void)homeSubCategorySelectedWithItem:(NSDictionary *)item;
@end
