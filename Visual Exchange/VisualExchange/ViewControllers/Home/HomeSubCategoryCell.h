//
//  HomeSubCategoryCell.h
//  VisualExchange
//
//  Created by Minhaz on 6/25/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeSubCategoryCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewSubCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblSubCategoryName;

@end
