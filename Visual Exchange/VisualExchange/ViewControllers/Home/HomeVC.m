//
//  HomeVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/20/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "HomeVC.h"
//#import "CarbonKit.h"
#import "HomeSubCatItemsVC.h"
#import "HomeSubCategoriesDelegate.h"
#import "HomeProductDetailDelegate.h"
#import "ProductDetailsVC.h"


@interface HomeVC () <HomeProductDetailDelegate>
{
    __weak IBOutlet UIButton *backBt;
    __weak IBOutlet UIButton *menuBt;
    __weak IBOutlet UIButton *searchBt;
    __weak IBOutlet UIScrollView *theScrollView;
    NSArray *arrCatItems;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
      [DLHelper AppDelegate].tabbarController.selectedIndex=0;

    [self.videoPlayerViewController.moviePlayer stop];
    

    [DLHelper AppDelegate].tabbarController.tabBar.hidden =false;
    UITabBarItem *tabItem = [[[[DLHelper AppDelegate].tabbarController tabBar] items] objectAtIndex:1];
     NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
    if(custid.length>0)
    {
    [tabItem setTitle:@"MY CART"];
    }
    else{
        [tabItem setTitle:@"CART"];

    }

    

    id responder = [[[self.childViewControllers firstObject] childViewControllers] firstObject];
    if (responder) {
        self.delegate = responder;
    }
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(CategorySelect:) name:@"CateGorySelected" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(CategoryList) name:@"CategoryList" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ProductList) name:@"ProductList" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CategoryList" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ProductList" object:nil];
    
    // fetch categories
   
}
-(void)viewWillAppear:(BOOL)animated{
     [self fetchCategories];
     [DLHelper shareinstance]. selectedIndex = 1000;
       }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
   
}

#pragma mark - Fetch Categories
- (void)fetchCategories
{
    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"categoriesList.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        
        arrCatItems = [JSON[@"country"]mutableCopy];
        [self setupCategoriesOnTopBar];
        
    }];


}

#pragma mark - Setup top categries scrollview
- (void)setupCategoriesOnTopBar
{
    
    
    
    NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if(custid.length==0)
    {
        [dict setObject:@"0" forKey:@"customer_id"];
    }
    else{
        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
    }
    
    NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
    if(myid.length>0)
    {
    
    
    NSString * deviceTokenString = [[[[myid description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"The generated device token string is : %@",deviceTokenString);
    [dict setObject:deviceTokenString forKey:@"device_id"];
    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"cartList.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        NSMutableArray* arrayCart = [JSON[@"cartList"]mutableCopy];
        NSString *str =  [NSString stringWithFormat:@"%lu",(unsigned long)arrayCart.count];
        NSInteger count = [str integerValue];
        [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%ld",(long)count]:nil;
        
    }];
    }
    else{
        
    }
    
    

    if (arrCatItems == nil || arrCatItems.count == 0) {
        NSLog(@"HomeVC > categories not found");
        return;
    }

   
        CGFloat margin = 10;
    CGFloat height = 30;
    CGFloat xPos = margin;
    CGFloat yPos = theScrollView.frame.size.height/2.0 - height/2;
    for(UIView *btn in theScrollView.subviews){
        
        [btn removeFromSuperview];
    }
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];

    
    for (int i = 0; i < arrCatItems.count; i++) {
        NSDictionary *dict = arrCatItems[i];
        NSString *name = [dict[@"name"]uppercaseString];
        CGSize size = [name sizeWithAttributes:@{NSFontAttributeName: font}];
        CGFloat width = size.width + margin * 2;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        CGRect frame = CGRectMake(xPos, yPos, width, height);
        button.frame = frame;
        [button setTitle:name forState:UIControlStateNormal];
        [button setTitleColor:[UIColor specSheetGreen] forState:UIControlStateNormal];
        button.titleLabel.font = font;
        [button addTarget:self action:@selector(onCategorySelected:) forControlEvents:UIControlEventTouchUpInside];
        [theScrollView addSubview:button];
        xPos = xPos + margin + width;
    }
//
    UIView *lastObject = theScrollView.subviews.lastObject;
    CGFloat contentWidth = lastObject.frame.origin.x + lastObject.frame.size.width + margin;
    theScrollView.contentSize = CGSizeMake(contentWidth, theScrollView.frame.size.height);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeSubCategoriesFound:)]) {
        [self.delegate homeSubCategoriesFound:arrCatItems];
    }
   
}

#pragma mark - Press event for Category
- (void)onCategorySelected:(UIButton *)sender
{
    
   [[theScrollView viewWithTag:100]removeFromSuperview];
    for(UIButton *btn in theScrollView.subviews){
        
       if(btn.tag==sender.tag){
            CGRect rect = btn.frame;
            rect.origin.y = btn.frame.size.height+9;
            rect.size.height = 6;
            rect.size.width = rect.size.width-7;
            rect.origin.x = rect.origin.x;
           
           // [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/2)-SCREEN_WIDTH/2, 0) animated:true];
            
           
            
           // [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/2)-SCREEN_WIDTH/8, 0) animated:true];
            
           NSUInteger count = theScrollView.subviews.count;
           
           
           if (sender.tag==0)
           {
               NSLog(@" scrollview content x point %f",rect.origin.x-10);
               [theScrollView setContentOffset:CGPointMake(rect.origin.x-10, 0) animated:true];
           }
           else if (sender.tag==count-1)
           {
               NSLog(@" scrollview content x point %f",rect.origin.x+(rect.size.width/2)-(SCREEN_WIDTH/2));
               
               
               if(SCREEN_WIDTH==320)
               {
                   [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/2-80, 0) animated:true];
               }
               else if(SCREEN_WIDTH==414)
               {
                   [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/2-90, 0) animated:true];
               }
               else
               {
                   [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/2-120, 0) animated:true];
               }
               //[theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/2)-SCREEN_WIDTH/2, 0) animated:true];
               
           }
//           else if (sender.tag==count-2)
//           {
//               NSLog(@" scrollview content x point %f",rect.origin.x+(rect.size.width/2)-(SCREEN_WIDTH/2));
//               
//               
//               if(SCREEN_WIDTH==320)
//               {
//                   [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/2-80, 0) animated:true];
//               }
//               else if(SCREEN_WIDTH==414)
//               {
//                   [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/2-105, 0) animated:true];
//               }
//               else
//               {
//                   [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/2-120, 0) animated:true];
//               }
//               //[theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/2)-SCREEN_WIDTH/2, 0) animated:true];
//               
//           }
           else
           {
               // [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/2)-SCREEN_WIDTH/8, 0) animated:true];
               [theScrollView setContentOffset:CGPointMake(rect.origin.x+(rect.size.width/45)-SCREEN_WIDTH/15, 0) animated:true];
           }
           
              UIView *view = [[UIView alloc]initWithFrame:rect];
              view.tag = 100;
              view.backgroundColor = [UIColor specSheetGreen];
              [theScrollView addSubview:view];
        }}
    
    if (sender.tag < arrCatItems.count) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(homeSubCategorySelectedWithItem:)]) {
            NSDictionary *item = arrCatItems[sender.tag];
            [self.delegate homeSubCategorySelectedWithItem:item];
         }
    }
}
-(void)CategorySelect:(NSNotification*)notification
{
    NSLog(@"%@",notification.object);
    
   NSInteger pass = [[[notification object] valueForKey:@"number"] intValue];
    [self onCategorySelected:[theScrollView viewWithTag:pass]];
}
#pragma mark - Actions
- (IBAction)onMenuButtonClicked:(id)sender {
    [[AppLauncher sharedInstance] openLeftDrawer];
}

- (IBAction)onBackButtonClicked:(id)sender {
   // [[AppLauncher sharedInstance].topItemVC.navigationController popViewControllerAnimated:YES];
    HomeVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
   
    [self.navigationController pushViewController:vc animated:YES];

    NSLog(@"%@",[AppLauncher sharedInstance].topItemVC.navigationController);
}

- (IBAction)onSearchButtonClicked:(id)sender {
    
    [[AppLauncher sharedInstance]Searchpress];

    
}

#pragma mark - 
- (void)didSelectProduct:(NSDictionary *)item
{
    ProductDetailsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailsVC"];
    vc.product = item;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)ProductList{
    _btnBack.hidden = false;
    _btnBack.frame = CGRectMake(0, 20, 44, 44);
    _btnMenu.hidden = true;
    
}
- (void)CategoryList{
    
    _btnBack.hidden = true;
    _btnMenu.frame = CGRectMake(0, 20, 44, 44);
    _btnMenu.hidden = false;

}
@end
