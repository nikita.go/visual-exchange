//
//  LeftMenuTableCell.m
//  VisualExchange
//
//  Created by Minhaz on 6/20/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "LeftMenuTableCell.h"

@implementation LeftMenuTableCell

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[UIColor specSheetGreen]]; // set color here
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
