//
//  HomeSubCatItemsVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/26/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "HomeSubCatItemsVC.h"
#import "SubCatItemCell.h"
#import "ProductDetailsVC.h"

@interface HomeSubCatItemsVC() <UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UITableView *theTableView;
    NSMutableArray *products;
}
@end

@implementation HomeSubCatItemsVC

- (void)viewDidLoad{
    [super viewDidLoad];
    _alertlbl.hidden=YES;
    
    id responder = self.parentViewController.parentViewController;
    if (responder) {
        self.delegate = responder;
    }
    
    // fetch items
    [self fetchItems];
    
    UINib *nibProduct = [UINib nibWithNibName:NSStringFromClass([ProductCell class]) bundle:nil];
    [theTableView registerNib:nibProduct forCellReuseIdentifier:@"ProductCell"];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [AppLauncher sharedInstance].topItemVC = self;
      [[NSNotificationCenter defaultCenter]postNotificationName:@"ProductList" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
  //  [AppLauncher sharedInstance].topItemVC = nil;
}

#pragma mark - Reload Item list with new Category
- (void)reloadItemList
{
    [self fetchItems];
}

#pragma mark - Fetch Items for Category
- (void)fetchItems
{


    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"user_id"] = kUserDefults_(ResultUser)[@"customer_id"];;
    dict[@"categorie_id"] = _category[@"categorie_id"];
    dict[@"page_number"] = @"1";

    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"productlist.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        if (products) {
            [products removeAllObjects];
        }
       
            products = [NSMutableArray arrayWithArray:[JSON[@"productList"]mutableCopy]];
        
        
        if (!products || products.count == 0) {
            _alertlbl.hidden=NO;
            theTableView.hidden=YES;
            theTableView.scrollEnabled = NO;
        } else{
              theTableView.hidden=NO;
             _alertlbl.hidden=YES;
             theTableView.scrollEnabled = YES;
        }
        
        [self reloadTableData];
    }];

}

#pragma mark - Reload table data
- (void)reloadTableData
{
    theTableView.delegate = self;
    theTableView.dataSource = self;
    [theTableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!products || products.count == 0) {
        return 0; //No data available cell
    }
    return products.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    
     return 300*SCREEN_HEIGHT/568;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  

    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    
    NSString *imagepath = nil;
    if([products[indexPath.row][@"images"]count]>0){
        imagepath = products[indexPath.row][@"images"][0][@"product_image"];
    }else{
        imagepath = products[indexPath.row][@"image"];
    }
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:cell.imgmain urlString:imagepath Complete:^(UIImage *image) {}];
    
    cell.lblName.text = products[indexPath.row][@"name"];
     NSString *subtitle = [NSString stringWithFormat:@"%@ - by %@",products[indexPath.row][@"categoryname"],products[indexPath.row][@"customer_name"]];
    cell.lblSubtitle.text = subtitle;
    cell.lblprice.text = [NSString stringWithFormat:@"$%@",products[indexPath.row][@"price"]];
    cell.clipsToBounds = true;
    return cell;
    
    

}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view  = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!products || products.count == 0) {
        return; //No data available cell
    }
    
    //Product details
    NSDictionary *product = products[indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectProduct:)]) {
        [self.delegate didSelectProduct:product];
    }
}

@end
