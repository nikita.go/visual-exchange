//
//  LeftMenuPersonInfoCell.m
//  VisualExchange
//
//  Created by Minhaz on 6/20/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "LeftMenuPersonInfoCell.h"

@implementation LeftMenuPersonInfoCell

- (void)awakeFromNib {
    // Initialization code
   
    
    _imageViewPhoto.layer.cornerRadius = _imageViewPhoto.frame.size.width/2.0;
    _imageViewPhoto.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f].CGColor;
    _imageViewPhoto.layer.shadowOffset = CGSizeMake(0, 0);
    _imageViewPhoto.layer.shadowOpacity = 0.5f;
    _imageViewPhoto.layer.shadowRadius = 5.f;
    _imageViewPhoto.backgroundColor = [UIColor clearColor];
    _imageViewPhoto.layer.masksToBounds = YES;
    if(kUserDefults_(ResultUser)){
        self.vsaparater.hidden = true;
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end
