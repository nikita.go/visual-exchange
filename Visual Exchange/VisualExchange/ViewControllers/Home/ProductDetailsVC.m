//
//  ProductDetailsVC.m
//  VisualExchange
//
//  Created by Minhaz on 7/3/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "ProductDetailsVC.h"
#import "HomeSubCatItemsVC.h"
#import "IQTextView.h"
#import "HomeSubCatListView.h"
#import "HomeVC.h"



@interface ProductDetailsVC () <UITextViewDelegate>

{
    IBOutlet UIView *videovie;
    __weak IBOutlet UIView *contentView;
    __weak IBOutlet UIScrollView *contentScrollView;
    __weak IBOutlet UIImageView *imageViewUser;
    __weak IBOutlet UILabel *lblUsername;
    
    __weak IBOutlet UIImageView *imageViewProduct;
    __weak IBOutlet UILabel *lblUserSubtitle;
    __weak IBOutlet UIButton *btAskAQuestion;
    
    __weak IBOutlet UILabel *lblMainTitle;
    __weak IBOutlet UILabel *lblProductTitle;
    __weak IBOutlet UILabel *lblProductSubtitle;
    __weak IBOutlet UILabel *lblProductPrice;
    __weak IBOutlet UILabel *lblProductDetails;
    
    IBOutlet UIView *btnview;
    __weak IBOutlet UIButton *btAddToCart;
    __weak IBOutlet UIButton *btCamera;
    __weak IBOutlet UIButton *btVideoCamera;
    __weak IBOutlet UILabel *lblCounter;
    
    __weak IBOutlet UILabel *lblsku;
    __weak IBOutlet UILabel *lblstock;
    __weak IBOutlet UIView *viewAskQuestion;
    __weak IBOutlet UIView *viewPopupBox;
    __weak IBOutlet UILabel *lblAskAQuestion;
    __weak IBOutlet IQTextView *twAskAQuestion;
    __weak IBOutlet UIButton *btSendAskAQuestion;
    __weak IBOutlet UILabel *leadtimelbl;
    __weak IBOutlet UIButton *btCloseAskAQuestion;
    GUIPlayerView *playerView;
    IBOutlet UIImageView *checkimg;
    IBOutlet UILabel *coupencode;
    IBOutlet UIButton *checkbtn;
    
}
@end


@implementation ProductDetailsVC
- (IBAction)pagecontrolaction:(id)sender
{
    [_ScrollImages scrollRectToVisible:CGRectMake(_ScrollImages.frame.size.width*_pageControll.currentPage, _ScrollImages.frame.origin.y, _ScrollImages.frame.size.width, _ScrollImages.frame.size.height) animated:YES];
    

    int page=_pageControll.currentPage;
    CGRect frame=_ScrollImages.frame;
    frame.origin.x=frame.size.width*page;
    frame.origin.y=0;
    [_ScrollImages scrollRectToVisible:frame animated:YES];
}
@synthesize  delegate;


- (void)viewDidLoad {
    [super viewDidLoad];
      // [checkbtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];

    twAskAQuestion.placeholder = @"Enter question..";
    // Do any additional setup after loading the view.
    
    self.view.frame = [UIScreen mainScreen].bounds;
    viewPopupBox.layer.cornerRadius = 3.0;
    lblAskAQuestion.layer.borderColor = [UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0].CGColor;
    lblAskAQuestion.layer.borderWidth = 1;
    
    twAskAQuestion.delegate = self;
    twAskAQuestion.layer.cornerRadius = 3;
    twAskAQuestion.layer.masksToBounds = YES;
    twAskAQuestion.layer.borderColor = [UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0].CGColor;
    twAskAQuestion.layer.borderWidth = 1;
    
    btSendAskAQuestion.layer.cornerRadius = btSendAskAQuestion.frame.size.height/2.0;
    btCloseAskAQuestion.layer.cornerRadius = btCloseAskAQuestion.frame.size.height/2.0;
    
    // round border for user image
    imageViewUser.layer.borderColor = [UIColor specSheetGreen].CGColor;
    imageViewUser.layer.borderWidth = 1.f;
    imageViewUser.layer.cornerRadius = imageViewUser.frame.size.height/2.0;
    
    NSLog(@"ProductDetailsVC > Product: %@",_product);
//    btAddToCart.layer.cornerRadius  = btAddToCart.frame.size.height/2;
//    btAddToCart.layer.masksToBounds = true;
    
    lblMainTitle.text = _product[@"name"];
    
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_960.png"] imageview:imageViewUser  urlString:_product[@"customer_image"] Complete:^(UIImage *image) {}];
    
    NSString *imagepath = nil;
    if([_product[@"images"]count]>0){
        imagepath = _product[@"images"][0][@"product_image"];
    }else{
        
        imagepath = _product[@"image"];
    }
    
    
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_960.png"] imageview:imageViewProduct  urlString:imagepath Complete:^(UIImage *image) {}];
   
    
    NSMutableAttributedString *skustring=[[NSMutableAttributedString alloc]initWithString:_product[@"sku"]];
    NSMutableAttributedString *sku=[[NSMutableAttributedString alloc]initWithString:@"SKU: "];
    
    
    NSMutableAttributedString *stockstr=[[NSMutableAttributedString alloc]initWithString:_product[@"stock_limit"]];
    NSMutableAttributedString *stock=[[NSMutableAttributedString alloc]initWithString:@"Stock: "];
    
    
    
    
    NSMutableAttributedString *timestr=[[NSMutableAttributedString alloc]initWithString:_product[@"lead_time"]];
    NSMutableAttributedString *time=[[NSMutableAttributedString alloc]initWithString:@"Lead Time: "];

   // [sku addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0] range:NSMakeRange(0,sku.length)];
    
    [skustring addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]
                range:NSMakeRange(0, skustring.length)];
    [sku addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor]
                range:NSMakeRange(0, sku.length)];
    
    [sku appendAttributedString:skustring];
    
    
    
    [timestr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]
                      range:NSMakeRange(0, timestr.length)];
    [time addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor]
                range:NSMakeRange(0, time.length)];
    
    [time appendAttributedString:timestr];

    
    
    
    
    [stockstr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]
                      range:NSMakeRange(0, stockstr.length)];
    [stock addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor]
                range:NSMakeRange(0, stock.length)];
    
    [stock appendAttributedString:stockstr];
    lblstock.attributedText=stock;
    lblsku.attributedText=sku;
    leadtimelbl.attributedText=time;
    

    

    lblUsername.text = _product[@"customer_name"];
   // leadtimelbl.text = _product[@"lead_time"];
    //lblsku.text = _product[@"sku"];
   // lblstock.text = _product[@"stock_limit"];
    lblUserSubtitle.hidden = NO;
    NSInteger totalProducts = [_product[@"total_product"] integerValue];
    if (totalProducts >= 1)
    {
        lblUserSubtitle.text = [NSString stringWithFormat:@"has %ld more product(s)",totalProducts];
        
    } else
    {
        
        //lblUserSubtitle.text = [NSString stringWithFormat:@"has %ld products",totalProducts];
        lblUserSubtitle.text = @"has no more products";
    }
    for (UIView *view in [contentScrollView viewWithTag:5252].subviews)
    {
        view.autoresizingMask = UIViewAutoresizingNone;
    }
    
    
    lblProductTitle.text = _product[@"name"];
    lblProductSubtitle.text = _product[@"categoryname"];
    lblProductPrice.text = [NSString stringWithFormat:@"$%@", _product[@"price"]];
    lblProductDetails.text = _product[@"details"];
    CGRect rect = lblProductDetails.frame;
    rect.size.height = [[DLHelper shareinstance]getLabelHeight:lblProductDetails];
    lblProductDetails.frame = rect;
    NSString *str=[NSString stringWithFormat:@"%@", [_product objectForKey:@"couponDetail"]];
    if(str.length==0)
    {
        _CouponView.hidden=YES;
    }
    else
    {
        NSString *code= [NSString stringWithFormat:@"%@",[[_product objectForKey:@"couponDetail"]objectForKey:@"code"]];
        NSString *discount= [NSString stringWithFormat:@"%@",[[_product objectForKey:@"couponDetail"]objectForKey:@"discount"]];
        NSString *type= [NSString stringWithFormat:@"%@",[[_product objectForKey:@"couponDetail"]objectForKey:@"discount_type"]];
        UIFont *VerdanaFont =[UIFont systemFontOfSize:12];
        NSUInteger characterCount = [code length];
        NSRange boldedRange = NSMakeRange(1, characterCount);
        NSString *coupen;
         if([type isEqualToString:@"2"])
         {  NSString *STR=@"% off";
           coupen=[NSString stringWithFormat:@"%@ %@ %@",code,discount,STR];

         }
        else
        {
           coupen=[NSString stringWithFormat:@"%@ $%@ discount",code,discount];

        }
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]initWithString:coupen];
        
        
       
    if([type isEqualToString:@"2"])
        {
             coupencode.text= [NSString stringWithFormat:@"Coupon code: %@",code];
            UIFont * font=[UIFont systemFontOfSize:15];

           
            CGSize stringSize = [code sizeWithFont:font];
            
            CGFloat width = stringSize.width;
            
           // _codediscount.frame=CGRectMake(coupencode.frame.origin.x+width+3, _codediscount.frame.origin.y, _codediscount.frame.size.width, _codediscount.frame.size.height);
           // _codediscount.frame=CGRectMake(coupencode.frame.origin.x, _codediscount.frame.origin.y, _codediscount.frame.size.width, _codediscount.frame.size.height);
            NSString *STR=@"% off";
            _codediscount.text=[NSString stringWithFormat:@"%@ %@",discount,STR];
        }
        else
        {
             coupencode.text= [NSString stringWithFormat:@"Coupon code: %@",code];
            UIFont * font=[UIFont systemFontOfSize:15];
            
            
            CGSize stringSize = [code sizeWithFont:font];
            
            CGFloat width = stringSize.width;
            
           // _codediscount.frame=CGRectMake(coupencode.frame.origin.x+width+3, _codediscount.frame.origin.y, _codediscount.frame.size.width, _codediscount.frame.size.height);
//_codediscount.frame=CGRectMake(coupencode.frame.origin.x, _codediscount.frame.origin.y, _codediscount.frame.size.width, _codediscount.frame.size.height);

            _codediscount.text=[NSString stringWithFormat:@"$%@ discount",discount];

        }
        
        _CouponView.hidden=NO;

//    NSLog(@"btn view height %f",)
//    CGRect frmNew = checkimg.frame;
//    frmNew.origin.y = lblProductDetails.frame.origin.y + lblProductDetails.frame.size.height + 8;
//    checkimg.frame = frmNew;
//    
    CGRect frmNew1 = _CouponView.frame;
    frmNew1.origin.y = btnview.frame.origin.y + lblProductDetails.frame.size.height+30;
    _CouponView.frame = frmNew1;
        
        
        
        
        _productdetailview.frame=CGRectMake(_productdetailview.frame.origin.x, _productdetailview.frame.origin.y, _productdetailview.frame.size.width, _productdetailview.frame.size.height);
        
        
//    CGRect frmNew2 = _mainScrollView.frame;
//    frmNew2.origin.y = _mainScrollView.frame.origin.y + _CouponView.frame.size.height+50 ;
//    _mainScrollView.frame = frmNew2;
        
    }
//
//    CGRect frmNew2 = btnview.frame;
//    frmNew2.origin.y = lblProductDetails.frame.origin.y + lblProductDetails.frame.size.height + 8+btnview.frame.origin.y;
//    btnview.frame = frmNew2;
    
//    CGRect frmNew3 = _CouponView.frame;
//    frmNew3.origin.y = btnview.frame.origin.y + btnview.frame.size.height + 8;
//    _CouponView.frame = frmNew3;
    
    
    //     btn=[[UIButton alloc]initWithFrame:CGRectMake( coupencode.frame.origin.x+ coupencode.frame.size.width+5, coupencode.frame.origin.y, 25, 25)];
    //    [btn setBackgroundColor:[UIColor redColor]];
    //    [btn setTag:10111];
    //    [btn addTarget:self action:@selector(checkbtnpress1:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    //    btn.userInteractionEnabled = YES;
    //    CGRect frmNew2 = btn.frame;
    //   frmNew2.origin.y = lblProductDetails.frame.origin.y + lblProductDetails.frame.size.height + 8;
    //    btn.frame = frmNew2;
    //
    //    [btnview addSubview:btn];
    
    CGRect rectVIEW   = [contentScrollView viewWithTag:5252].frame;
    rectVIEW.size.height = rect.size.height + rectVIEW.size.height ;
    [contentScrollView viewWithTag:5252].frame = rectVIEW;
    
    
    
    
    CGRect rectbtnadd = btAddToCart.frame;
    rectbtnadd.origin.y = rectVIEW.origin.y + rectVIEW.size.height +20 ;
    btAddToCart.frame = rectbtnadd;
    
    
    
    _productdetailview.frame=CGRectMake(_productdetailview.frame.origin.x, _productdetailview.frame.origin.y, _productdetailview.frame.size.width, _productdetailview.frame.size.height);
    

    CGRect rectscroll   = self.ScrollImages.frame;
    rectscroll.size.width = SCREEN_WIDTH-20;
    self.ScrollImages.frame = rectscroll;
    [self.ScrollImages layoutIfNeeded];
    
    for (int i= 0 ; i<[_product[@"images"]count]; i++) {
        UIImageView *imgView = [UIImageView new];
        imgView.frame = CGRectMake(self.ScrollImages.frame.size.width *i, 0, self.ScrollImages.frame.size.width, self.ScrollImages.frame.size.height);
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.clipsToBounds = true;
        [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:imgView urlString:_product[@"images"][i][@"product_image"] Complete:^(UIImage *image) {}];
        [self.ScrollImages addSubview:imgView];
        
    }
    self.pageControll.numberOfPages = [_product[@"images"]count];

    int page=_pageControll.currentPage;
    self.ScrollImages.contentSize = CGSizeMake(self.pageControll.numberOfPages *self.ScrollImages.frame.size.width, 0);
    NSString *limit=[NSString stringWithFormat:@"%@",_product[@"stock_limit"]];
    if( [limit integerValue]<=0)
    {
        [btAddToCart setTitle:@"OUT OF STOCK" forState:UIControlStateNormal];
        btAddToCart.userInteractionEnabled=NO;
        
    }
    else
    {
        [btAddToCart setTitle:@"ADD TO CART" forState:UIControlStateNormal];
        btAddToCart.userInteractionEnabled=YES;

    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [playerView clean];
    playerView = nil;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
//    lblMainTitle.text = _product[@"name"];
//    
//    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_960.png"] imageview:imageViewUser  urlString:_product[@"customer_image"] Complete:^(UIImage *image) {}];
//    
//    NSString *imagepath = nil;
//    if([_product[@"images"]count]>0){
//        imagepath = _product[@"images"][0][@"product_image"];
//    }else{
//        
//        imagepath = _product[@"image"];
//    }
//
//    
//    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_960.png"] imageview:imageViewProduct  urlString:imagepath Complete:^(UIImage *image) {}];
//
//    
//   
//    
//    lblUsername.text = _product[@"customer_name"];
//     leadtimelbl.text = _product[@"lead_time"];
//    lblUserSubtitle.hidden = NO;
//    NSInteger totalProducts = [_product[@"total_product"] integerValue];
//    if (totalProducts > 1) {
//        lblUserSubtitle.text = [NSString stringWithFormat:@"has %ld more products",totalProducts];
//        
//    } else {
////        lblUserSubtitle.text = [NSString stringWithFormat:@"has %ld products",totalProducts];
//        lblUserSubtitle.text = @"No more products";
//    }
//    for (UIView *view in [contentScrollView viewWithTag:5252].subviews){
//        view.autoresizingMask = UIViewAutoresizingNone;
//    }
//    
//    
//    lblProductTitle.text = _product[@"name"];
//    lblProductSubtitle.text = _product[@"categoryname"];
//    lblProductPrice.text = [NSString stringWithFormat:@"$%@", _product[@"price"]];
//    lblProductDetails.text = _product[@"details"];
//    CGRect rect = lblProductDetails.frame;
//    rect.size.height = [[DLHelper shareinstance]getLabelHeight:lblProductDetails];
//    lblProductDetails.frame = rect;
//    
//    CGRect frm1 = btnview.frame;
//    frm1.size.height = lblProductTitle.frame.size.height + lblProductDetails.frame.size.height + 2;
//    btnview.frame = frm1;
//    
//    CGRect frmNew = _CouponView.frame;
//    frmNew.origin.y = btnview.frame.origin.y + btnview.frame.size.height + 8;
//    _CouponView.frame = frmNew;
//
////    CGRect frmNew1 = checkbtn.frame;
////    frmNew1.origin.y = lblProductDetails.frame.origin.y + lblProductDetails.frame.size.height + 8;
////    checkbtn.frame = frmNew1;
//    
//    
////     btn=[[UIButton alloc]initWithFrame:CGRectMake( coupencode.frame.origin.x+ coupencode.frame.size.width+5, coupencode.frame.origin.y, 25, 25)];
////    [btn setBackgroundColor:[UIColor redColor]];
////    [btn setTag:10111];
////    [btn addTarget:self action:@selector(checkbtnpress1:) forControlEvents:UIControlEventTouchUpInside];
////    [btn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
////    btn.userInteractionEnabled = YES;
////    CGRect frmNew2 = btn.frame;
////   frmNew2.origin.y = lblProductDetails.frame.origin.y + lblProductDetails.frame.size.height + 8;
////    btn.frame = frmNew2;
////    
////    [btnview addSubview:btn];
//    
//    CGRect rectVIEW   = [contentScrollView viewWithTag:5252].frame;
//    rectVIEW.size.height = rect.size.height + rectVIEW.size.height ;
//    [contentScrollView viewWithTag:5252].frame = rectVIEW;
//
//    CGRect rectbtnadd = btAddToCart.frame;
//    rectbtnadd.origin.y = rectVIEW.origin.y + rectVIEW.size.height + 10;
//    btAddToCart.frame = rectbtnadd;
//
//    CGRect rectscroll   = self.ScrollImages.frame;
//    rectscroll.size.width = SCREEN_WIDTH-20;
//    self.ScrollImages.frame = rectscroll;
//    [self.ScrollImages layoutIfNeeded];
//    
//    for (int i= 0 ; i<[_product[@"images"]count]; i++) {
//        UIImageView *imgView = [UIImageView new];
//        imgView.frame = CGRectMake(self.ScrollImages.frame.size.width *i, 0, self.ScrollImages.frame.size.width, self.ScrollImages.frame.size.height);
//        imgView.contentMode = UIViewContentModeScaleAspectFill;
//        imgView.clipsToBounds = true;
//        [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:imgView urlString:_product[@"images"][i][@"product_image"] Complete:^(UIImage *image) {}];
//        [self.ScrollImages addSubview:imgView];
//        
//    }
//    self.pageControll.numberOfPages = [_product[@"images"]count];
//    self.ScrollImages.contentSize = CGSizeMake(self.pageControll.numberOfPages *self.ScrollImages.frame.size.width, 0);
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    if(![_product[@"video_id"] isEqualToString:@""] ||  ![_product[@"video"] isEqualToString:@""] ){
        [self onVideoCameraBtClicked:btVideoCamera];
        
    }

    [self AutomaticScrollview];
}

#pragma mark - Actions
- (IBAction)onBackBtClicked:(id)sender {
     [playerView stop];
    [self.navigationController popViewControllerAnimated:YES];
   // HomeVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
   // [self.navigationController pushViewController:vc animated:YES];

}
- (IBAction)onSearchBtClicked:(id)sender {
    [playerView stop];

    [DLHelper AppDelegate].tabbarController.selectedIndex = 2;
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"SearchCatagoryVC"];
    navigation.viewControllers = @[leftDrawer].mutableCopy;
    
}

- (IBAction)onAskAQuestionBtClicked:(id)sender {
     [playerView stop];
    [self openAskQuestionPopup];
}

- (IBAction)onSubtractProductCountClicked:(id)sender {
    NSInteger count = lblCounter.text.integerValue;
    if (count > 1) {
        count = count - 1;
        lblCounter.text = [NSString stringWithFormat:@"%ld",count];
    }
}

- (IBAction)onIncreaseProductCountClicked:(id)sender
{
    NSInteger count = lblCounter.text.integerValue;
    NSString *limit=[NSString stringWithFormat:@"%@",_product[@"stock_limit"]];
    if( [limit integerValue]>0)
    {
         if(count== [limit integerValue])
         {
        NSString *limit=[NSString stringWithFormat:@"You can choose maximum %@ quantity.", _product[@"stock_limit"]];
        
        UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:limit delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [objalert show];
         }
         else{
             count = count + 1;
             lblCounter.text = [NSString stringWithFormat:@"%ld",count];
         }
        
        
    }
       else{
        
           UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This product is out of stock." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
           [objalert show];

    }
   }

- (IBAction)onAddToCartBtClicked:(id)sender {
     [playerView stop];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
      NSMutableArray *arrayProduct = [NSMutableArray array];
    NSString *custid= kUserDefults_(ResultUser)[@"customer_id"];
    for(int i=0; i<1; i++){
        NSMutableDictionary *product = [NSMutableDictionary dictionary];
        product[@"pid"] =_product[@"product_id"];
        product[@"price"] = _product[@"price"];
        product[@"qty"] = lblCounter.text;
        product[@"qty"] = lblCounter.text;

        [arrayProduct addObject:product];
    }
   NSMutableDictionary *tempdict=[arrayProduct objectAtIndex:0];
    NSString *str=[NSString stringWithFormat:@"%@", [_product objectForKey:@"couponDetail"]];
    if([checkstr isEqualToString:@"Checked"])
    {
        [tempdict setObject:[[_product objectForKey:@"couponDetail"]objectForKey:@"Id"] forKey:@"couponId"];
    }
    else
    {
        [tempdict setObject:@"0" forKey:@"couponId"];
    }
   // [arrayProduct addObject:tempdict];
    NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
    
    
    
    NSString * deviceTokenString = [[[[myid description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"The generated device token string is : %@",deviceTokenString);
    if(deviceTokenString.length==0)
    {
        [dict setObject:@"f37481a95690b8b5fc01dc5aa59a24f7153119eda8a5f1d91e35ed2c61d6cc28" forKey:@"device_id"];
    }
    else{
        [dict setObject:deviceTokenString forKey:@"device_id"];
    }

//    NSString *str =[NSString stringWithFormat:@"%@",arrayProduct];
//    NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
//    
//    
//    NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    NSString *jsonString = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
     NSError *error;
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arrayProduct options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    

    dict[@"products"] = jsonString;
    
   
    if(custid.length==0)
    {
            NSMutableArray *array = [kUserDefults_(@"cart+cart")mutableCopy];
            if (array == nil)
            {
                   array=[[NSMutableArray alloc]init];
            }
        
       
        
        
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"addCart.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
        {
            kUserDefults( nil, @"cart+cart");
            
            NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
            dict[@"total_product"] = JSON[@"total_product"];
            kUserDefults(dict.mutableCopy, ResultUser);
            NSUserDefaults *defualt5=[NSUserDefaults standardUserDefaults];
            NSMutableArray *cartary=[[NSMutableArray alloc]init];
            // cartary=[defualt5 objectForKey:@"cartary"];
            NSMutableArray *ary2=JSON[@"cart_id"];
            for (int i=0; i<ary2.count; i++) {
                [cartary addObject:[ary2 objectAtIndex:i]];
            }

           
            [defualt5 setObject:cartary forKey:@"cartary"];
            [defualt5 synchronize];
            

            NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
            [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%d",count]:nil;
            
            
            
            
            //[self showAlertWithTitle:nil message:@"Add Sucessfull."];
            [self.navigationController popViewControllerAnimated:true];

            return;
        }];
    }
    else{
       

    dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
            //if (!kUserDefults_(ResultUser)){
//        NSMutableArray *array = [kUserDefults_(@"cart+cart")mutableCopy];
//        if (array == nil){
//            array=[[NSMutableArray alloc]init];
//        }
//        for(int i=0; i<1; i++){
//            NSMutableDictionary *product = [NSMutableDictionary dictionary];
//            product[@"pid"] =_product[@"product_id"];
//            product[@"price"] = _product[@"price"];
//            product[@"qty"] = lblCounter.text;
//            [array addObject:product];
//        }
//
//        [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = array.count==0?nil:[NSString stringWithFormat:@"%lu",(unsigned long)array.count];
//        kUserDefults(array, @"cart+cart");
//        [self.navigationController popViewControllerAnimated:true];
//        return;
   // }

    
  
      
             //NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arrayProduct options:NSJSONWritingPrettyPrinted error:&error];
   // NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    

    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"addCart.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
         kUserDefults( nil, @"cart+cart");
        
        NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
        dict[@"total_product"] = JSON[@"total_product"];
        kUserDefults(dict.mutableCopy, ResultUser);
        NSString *cartid=JSON[@"cart_id"];
       //  NSUserDefaults *defualt5=[NSUserDefaults standardUserDefaults];
//        NSMutableArray *cartary=[[NSMutableArray alloc]init];
//
//        // cartary=[defualt5 objectForKey:@"cartary"];
//        [cartary addObject:cartid];
//        [defualt5 setObject:cartary forKey:@"cartary"];
//        [defualt5 synchronize];
        
        NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
        [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%d",count]:nil;

        
        
        
        //[self showAlertWithTitle:nil message:@"Add Sucessfull."];
        [self.navigationController popViewControllerAnimated:true];
        return;
    }];
    }

    
}
- (IBAction)onCameraBtClicked:(UIButton *)sender {
    //if ([_delegate respondsToSelector:@selector(playerDidPause)]) {
    //[playerView clean];
        [self playerDidPause];
    
    //}
    UIButton *btn =  (UIButton *)[sender.superview viewWithTag:11];
    UIButton *btn1 =  (UIButton *)[sender.superview viewWithTag:10];
       btn.selected = false;
    sender.selected = true;
      //  [playerView clean];
    
       // [playerView stop];

    [self.ScrollImages.superview bringSubviewToFront:self.ScrollImages];
    [self.pageControll.superview bringSubviewToFront:self.pageControll];
    
   }
- (void)playerDidPause
{
    [_player stop];
     [self.videoPlayerViewController.moviePlayer stop];
   // playerView = [[GUIPlayerView alloc] init];
  //  [playerView stop];
     //[self.videoContainerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
   // [playerView seekToTime:kCMTimeZero];
    
   // [playButton setSelected:NO];
}
- (IBAction)onVideoCameraBtClicked:(UIButton *)sender {
    if([_product[@"video_id"] isEqualToString:@""] && [_product[@"video"] isEqualToString:@""] ){
        [self showErrorWithMessage:dlkProductVideoMiss];
        return;
    }
    
    UIButton *btn =  (UIButton *)[sender.superview viewWithTag:10];
    btn.selected = false;
    sender.selected = true;
    
    
    if(![_product[@"video"] isEqualToString:@""]){
        

        NSURL *url = [NSURL URLWithString:_product[@"video"]];
        
        CGRect rect = self.videoContainerView.frame;
        rect.origin.x = 0;
       rect.origin.y = 0;
        //[playerView clean];
       // playerView = nil;
        
     _player = [[MPMoviePlayerController alloc] initWithContentURL:url];
        [_player.view setFrame:CGRectMake(-4, _videoContainerView.frame.origin.y, _videoContainerView.frame.size.width, _videoContainerView.frame.size.height)];
       
        _player.shouldAutoplay = YES;
        //_player.controlStyle = MPMovieControlStyleDefault;

       // playerView = [[GUIPlayerView alloc] initWithFrame:rect];
        [_player play];
        _player.fullscreen = YES;
       // [self presentMoviePlayerViewControllerAnimated: player];
       
      //  [self.videoContainerView addSubview:player.view];
        //[theMoviPlayer play];
              //[playerView setVideoURL:url];
        [videovie addSubview:_player.view];
        _videoContainerView.hidden=YES;
       // [playerView prepareAndPlayAutomatically:YES];
        [self.videoContainerView.superview bringSubviewToFront:self.videoContainerView];
        return;
        
        
        //  self.videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:@"6v2L2UGZJAM"]; ///];
    }else
    {
        [self.videoContainerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        NSString *videoIdentifier = _product[@"video_id"];
        
        self.videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:videoIdentifier];
        
    }
    
    [self.videoPlayerViewController presentInView:self.videoContainerView];
    [self.videoPlayerViewController.moviePlayer prepareToPlay];
    [self.videoContainerView.superview bringSubviewToFront:self.videoContainerView];
    

    
//    if([_product[@"video_id"] isEqualToString:@""] && [_product[@"video"] isEqualToString:@""] ){
//        [self showErrorWithMessage:dlkProductVideoMiss];
//        return;
//    }
//    
//    UIButton *btn =  (UIButton *)[sender.superview viewWithTag:10];
//    btn.selected = false;
//    sender.selected = true;
//    
//    
//    if(![_product[@"video"] isEqualToString:@""]){
//        NSURL *url = [NSURL URLWithString:_product[@"video"]];
//        
//        CGRect rect = self.videoContainerView.frame;
//        rect.origin.x = 0;
//        rect.origin.y = 0;
//        [playerView clean];
//       // playerView = nil;
//
//        playerView = [[GUIPlayerView alloc] initWithFrame:rect];
//        [self.videoContainerView addSubview:playerView];
//       // playerView.delegate=self;
//        [playerView setVideoURL:url];
//               [playerView prepareAndPlayAutomatically:YES];
//         [self.videoContainerView.superview bringSubviewToFront:self.videoContainerView];
//         return;
//        //[self.videoContainerView.superview bringSubviewToFront:self.videoContainerView];
//    
//      //  self.videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:@"6v2L2UGZJAM"]; ///];
//    }else{
//        [self.videoContainerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//         NSString *videoIdentifier = _product[@"video_id"];
//        
//        self.videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:videoIdentifier];
//
//    }
//    
//    [self.videoPlayerViewController presentInView:self.videoContainerView];
//    [self.videoPlayerViewController.moviePlayer prepareToPlay];
//    [self.videoContainerView.superview bringSubviewToFront:self.videoContainerView];
//    
}



#pragma mark - Open Ask a question popup
- (void)openAskQuestionPopup
{
    [self.videoPlayerViewController.moviePlayer stop];

   [_player stop];
    if (!kUserDefults_(ResultUser)){
        [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
            if(DLindex==0)
                return;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            vc.hidesBottomBarWhenPushed = true;
            [[DLHelper AppDelegate].tabbarController.selectedViewController pushViewController:vc animated:YES];
            return;
        } title:nil message:@"You need to Login/Sign up first" canceltile:@"CANCEL" othertitle:@"OK", nil];
        
        return;
    }

    
    [viewAskQuestion removeFromSuperview];
    
    [self.view addSubview:viewAskQuestion];
    viewAskQuestion.frame = self.view.bounds;
    viewPopupBox.center = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
}

- (IBAction)onSendAskAQuestionClicked:(id)sender {
 
   [_player stop];
    if (twAskAQuestion.text.length==0){
        return;
    }
    [self.view endEditing:YES];
    [self showPopupWithCloseKeyboard];
    NSLog(@"Question Content > %@",twAskAQuestion.text);
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"text"] = twAskAQuestion.text;
    dict[@"user_id"] = kUserDefults_(ResultUser)[@"customer_id"];
    dict[@"pid"]  = _product[@"product_id"];
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"askQuestion.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        twAskAQuestion.text = @"";
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Your question sent successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
       // alert.delegate=self;
        [alert show];
       // [self showAlertWithTitle:@"Message" message:JSON[@"message"]];
        [self.view endEditing:YES];
        [viewAskQuestion removeFromSuperview];
        
    }];

}

- (IBAction)onCloseAskAQuestionClicked:(id)sender {
    [playerView stop];

    [self.view endEditing:YES];
    [viewAskQuestion removeFromSuperview];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self showPopupWithOpenKeyboard];
}

#pragma mark - Show popup with open keyboard
- (void)showPopupWithOpenKeyboard
{
    CGPoint center = viewPopupBox.center;
    center.y = 30.0 + viewPopupBox.frame.size.height/2.0;
    [UIView animateWithDuration:0.35f animations:^{
        viewPopupBox.center = center;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)showPopupWithCloseKeyboard
{
    CGPoint center = viewPopupBox.center;
    center = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
    [UIView animateWithDuration:0.35f animations:^{
        viewPopupBox.center = center;
    } completion:^(BOOL finished) {
        
    }];
}
-(void)AutomaticScrollview{
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in contentScrollView.subviews)
        contentRect = CGRectUnion(contentRect, view.frame);
    CGFloat pageWidth = self.ScrollImages.frame.size.width;
    int page = self.ScrollImages.contentOffset.x / pageWidth;
   self.pageControll.currentPage = page;
    CGRect frame=_ScrollImages.frame;
    frame.origin.x=frame.size.width*page;
    frame.origin.y=0;
    [_ScrollImages scrollRectToVisible:frame animated:YES];
    
    contentScrollView.contentSize = CGSizeMake(contentRect.size.width, contentRect.size.height+100) ;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    
    [self setIndiactorForCurrentPage];
    
}

-(void)setIndiactorForCurrentPage
{
    CGFloat SCROLLWIDTH = self.ScrollImages.frame.size.width;

    uint page = _ScrollImages.contentOffset.x / SCROLLWIDTH;
    [_pageControll setCurrentPage:page];
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if(scrollView == self.ScrollImages){
        
    CGFloat pageWidth = self.ScrollImages.frame.size.width;
    int page = floor((self.ScrollImages.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControll.currentPage = page;
    }
}
- (IBAction)MoreProductPress:(id)sender {
    [playerView stop];

  
    NSInteger totalProducts = [_product[@"total_product"] integerValue];
    if (totalProducts > 0) {
        ProductVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductVC"];
        vc.moreProduct = _product.mutableCopy;
        [self.navigationController pushViewController:vc animated:YES];
    }
    

}

- (IBAction)onTapCouponCodeBtn:(id)sender
{
    NSLog(@"Btn Tapped");
    if(checkbtn.tag==1)
    {
        checkstr=@"Checked";
        checkimg.image=[UIImage imageNamed:@"checked.png"];
       // [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        NSString *str=[NSString stringWithFormat:@"%@", [_product objectForKey:@"couponDetail"]];
        if(str.length>0)
        {
            NSString *str1=[NSString stringWithFormat:@"%@", [[_product objectForKey:@"couponDetail"]objectForKey:@"discount_type"]];
            if(str1.length>0)
            {
                if([str1 isEqualToString:@"1"])
                {
                    NSString *pricestr=[NSString stringWithFormat:@"%@", _product[@"price"]];
                    int a=[pricestr intValue];
                    NSString *discountstr=[NSString stringWithFormat:@"%@", [[_product objectForKey:@"couponDetail"]objectForKey:@"discount"]];
                    int discount=[discountstr intValue];
                    a=a-discount;
                    lblProductPrice.text=[NSString stringWithFormat:@"$%d",a];
                }
                else{
                    NSString *pricestr=[NSString stringWithFormat:@"%@", _product[@"price"]];
                    int a=[pricestr intValue];
                    NSString *discountstr=[NSString stringWithFormat:@"%@", [[_product objectForKey:@"couponDetail"]objectForKey:@"discount"]];
                    int discount=[discountstr intValue];
                   int d=a*discount/100;
                    a=a-d;
                    lblProductPrice.text=[NSString stringWithFormat:@"$%d",a];
                    
                }
            }
        }
        [checkbtn setTag:2];
    }
    else{
        //[btn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
        checkstr=@"UnChecked";

        checkimg.image=[UIImage imageNamed:@"unchecked.png"];
        NSString *pricestr=[NSString stringWithFormat:@"%@", _product[@"price"]];
        lblProductPrice.text=[NSString stringWithFormat:@"$%@",pricestr];
        [checkbtn setTag:1];
        
    }

}
@end
