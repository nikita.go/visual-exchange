//
//  LeftMenuPersonInfoCell.h
//  VisualExchange
//
//  Created by Minhaz on 6/20/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuPersonInfoCell : UITableViewCell

@property (nonatomic) __weak IBOutlet UIImageView *imageViewPhoto;
@property (nonatomic) __weak IBOutlet UILabel *lblName, *lblEmail;
@property (weak, nonatomic) IBOutlet UIView *vsaparater;

@end
