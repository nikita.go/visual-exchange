//
//  HomeSubCatListView.m
//  VisualExchange
//
//  Created by Minhaz on 6/26/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "HomeSubCatListView.h"
#import "HomeSubCategoryCell.h"
#import "HomeSubCategoriesDelegate.h"

@interface HomeSubCatListView() <UITableViewDelegate,UITableViewDataSource,HomeSubCategoriesDelegate>
{
    NSArray *subCategories;
}
@end

@implementation HomeSubCatListView

- (void)viewDidLoad{
    [super viewDidLoad];
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([SearchCatagoryCell class]) bundle:nil];
    [self.theTableView registerNib:nib forCellReuseIdentifier:@"SearchCatagoryCell"];
}

#pragma mark - HomeSubCategoriesDelegate
- (void)homeSubCategoriesFound:(NSArray *)items
{
    subCategories = [NSArray arrayWithArray:items];
    if (!subCategories || subCategories.count == 0) {
        _theTableView.scrollEnabled = NO;
    } else{
        _theTableView.scrollEnabled = YES;
    }
    [self reloadTableData];
}

- (void)homeSubCategorySelectedWithItem:(NSDictionary *)item {
    [self selectCategoryWithItem:item];
}

#pragma mark - Reload Data
- (void)reloadTableData
{
    self.theTableView.delegate = self;
    self.theTableView.dataSource = self;
    [self.theTableView reloadData];
}
-(void)viewWillAppear:(BOOL)animated{
     [[NSNotificationCenter defaultCenter]postNotificationName:@"CategoryList" object:nil];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!subCategories || subCategories.count == 0) {
        return 1;
    }
    return subCategories.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (!subCategories || subCategories.count == 0) {
        return tableView.frame.size.height;
    }
    
        return 200*SCREEN_HEIGHT/568;
   

   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *imagepath = nil;
    if([subCategories[indexPath.row][@"images"]count]>0){
        imagepath = subCategories[indexPath.row][@"images"][0][@"product_image"];
    }else{
        imagepath = subCategories[indexPath.row][@"image"];
    }
    
    SearchCatagoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCatagoryCell" forIndexPath:indexPath];
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:cell.imgmain urlString:imagepath Complete:^(UIImage *image) {
        
    }];
    UIFont *yourFont = cell.lblName.font;
    CGSize stringBoundingBox = [subCategories[indexPath.row][@"name"] sizeWithFont:yourFont];
    cell.viewAlfa.frame = CGRectMake((SCREEN_WIDTH-stringBoundingBox.width + 15)/2,(cell.frame.size.height/2)-10, stringBoundingBox.width + 15, stringBoundingBox.height+15);
   // cell.viewAlfa.center = CGPointMake(cell.center.x, cell.center.y-10);
    cell.lblName.text = subCategories[indexPath.row][@"name"];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!subCategories || subCategories.count == 0) {
        return; //No data available cell
    }
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CateGorySelected" object:@{@"number":[NSString stringWithFormat:@"%ld",(long)indexPath.row]}];
  
    NSDictionary *item = subCategories[indexPath.row];
    [self selectCategoryWithItem:item];
}

#pragma mark - Select Category With Item
- (void)selectCategoryWithItem:(NSDictionary *)item
{
    assert(item);
    
    UIViewController *topVC = [self topViewController];
    if (![topVC isKindOfClass:[HomeSubCatItemsVC class]]) {
        HomeSubCatItemsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeSubCatItemsVC"];
        vc.category = item;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        HomeSubCatItemsVC *vc = (HomeSubCatItemsVC *)topVC;
        vc.category = item;
        [vc reloadItemList];
    }
    
    
}

- (UIViewController *)topViewController
{
    NSArray *VCs = self.navigationController.viewControllers;
    return VCs.lastObject;
}

@end
