//
//  ForgotPasswordVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "ForgotPasswordVC.h"

@interface ForgotPasswordVC () <UITextFieldDelegate>
{
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UIButton *btSubmit;
}
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    btSubmit.layer.cornerRadius = btSubmit.frame.size.height/2;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)onSubmitBtClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if (!tfEmail.text.validateEmailWithString)
    {
        [self showErrorWithMessage:dlkEmail];
    }
    else
    {
       
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"email"] = tfEmail.text;
        
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"forgotPasswod.php" UrlBody:dict Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
        {
            NSLog(@"%@",JSON);
            if([[JSON objectForKey:@"message"]isEqualToString:@"Please check mail for reset your password"])
           {
               objalert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please check mail for reset your password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               objalert.tag=1;
            [objalert show];
           }
           else if([[JSON objectForKey:@"message"]isEqualToString:@"Given email is not valid to reset password"]){
              UIAlertView* objalert1=[[UIAlertView alloc]initWithTitle:@"" message:@"Given email is not valid to reset password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [objalert1 show];

           }
            
                     }];

    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(objalert.tag==1)
    {
    if(buttonIndex==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
 
    }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==tfEmail)
    {
        [tfEmail resignFirstResponder];
    }
    return  YES;
}

- (IBAction)onBackBtClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//#pragma mark - Error message display
//- (void)showErrorWithMessage:(NSString *)errorMessage
//{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alert show];
//}
//
//- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
//{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alert show];
//}

#pragma mark - on Shake Event
- (void)onShakeEventOccurred
{
    tfEmail.text = @"minhaz.panara@gmail.com";
}

@end
