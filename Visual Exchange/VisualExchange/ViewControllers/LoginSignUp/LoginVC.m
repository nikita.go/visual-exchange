//
//  LoginVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "LoginVC.h"
#import "SignUpVC.h"
#import "ForgotPasswordVC.h"

@interface LoginVC () <UITextFieldDelegate>
{
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword;
    __weak IBOutlet UIButton *btSignIn;
}
@end

@implementation LoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [DLHelper AppDelegate].tabbarController.tabBar.hidden =true;

    [btSignIn layoutIfNeeded];
    CGRect frm=btSignIn.frame;
    NSLog(@" Height of btn %f",frm.size.height);
    btSignIn.layer.cornerRadius =frm.size.height/2;
    btSignIn.layer.masksToBounds=YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
   
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)onSignInBtClicked:(id)sender
{
    
    if ([tfEmail.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your email address" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (!tfEmail.text.validateEmailWithString)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([tfPassword.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
       NSMutableDictionary *dict = [NSMutableDictionary dictionary];
       dict[@"email"] = tfEmail.text;
       dict[@"password"] = tfPassword.text;
        dict[@"device"] = @"1";

        NSMutableArray *cartary=[[NSUserDefaults standardUserDefaults]objectForKey:@"cartary"];
        NSString *str =[NSString stringWithFormat:@"%@",cartary];
        NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
        
        
        NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
        NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
        dict[@"cart_ids"] = finalstr;
        NSString *myid=[[NSUserDefaults standardUserDefaults]objectForKey:@"gcm"];
        
        
        
        NSString * deviceTokenString = [[[[myid description]
                                          stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                         stringByReplacingOccurrencesOfString: @">" withString: @""]
                                        stringByReplacingOccurrencesOfString: @" " withString: @""];
        
        NSLog(@"The generated device token string is : %@",deviceTokenString);
        if (deviceTokenString.length>0) 
        {
            [dict setObject:deviceTokenString forKey:@"device_id"];

        }
        else{
             [dict setObject:@"f37481a95690b8b5fc01dc5aa59a24f7153119eda8a5f1d91e35ed2c61d6cc28" forKey:@"device_id"];
                   }
      
       [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"login.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
           for(NSString *str in JSON.allKeys){
               if([JSON[str] isKindOfClass:[NSNull class]])
               {
                   JSON[str] = @"";
               }
           }
           NSString *userid= JSON[@"user_id"];
           if(userid.length==0 )
           {
               UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"" message:@"email and password wrong." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
               [objalert show];
           }
           else{

           JSON[@"customer_id"] = JSON[@"user_id"];
           [JSON removeObjectForKey:@"user_id"];
           kUserDefults(@"", ISLOGIN);
           kUserDefults(JSON.mutableCopy, ResultUser);
           [[DLHelper AppDelegate]isLoginUser];
           }
           [[DLHelper shareinstance]DLImageStore:nil urlString:kUserDefults_(ResultUser)[@"image"]  Complete:^(UIImage *image)
           {
               
           }];
           
           NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
           [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%d",count]:nil;

         
           NSMutableArray *array = [kUserDefults_(@"cart+cart")mutableCopy];
           NSMutableArray *arrayProduct = [NSMutableArray array];
           for(int i=0; i<array.count; i++){
               NSMutableDictionary *product = [NSMutableDictionary dictionary];
               
               product[@"pid"] = array[i][@"pid"];
               product[@"price"] =  array[i][@"price"];
               product[@"qty"] = array[i][@"qty"];

              
               [arrayProduct addObject:product];
           }
           
           if(arrayProduct.count>0){
               
               NSError *error;
               NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arrayProduct options:NSJSONWritingPrettyPrinted error:&error];
               NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
               NSMutableDictionary *body = [NSMutableDictionary dictionary];
               body[@"products"] = jsonString;
               body[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
               [[DLHelper shareinstance]DLSERVICE:self.view Indicater:false url:@"addCart.php" UrlBody:body.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                   
                   NSMutableDictionary *dict = [kUserDefults_(ResultUser)mutableCopy];
                   dict[@"total_product"] = JSON[@"total_product"];
                   kUserDefults(dict.mutableCopy, ResultUser);
                   
                   NSInteger count = [kUserDefults_(ResultUser)[@"total_product"]integerValue];
                   [DLHelper AppDelegate].tabbarController.tabBar.items[1].badgeValue = count?[NSString stringWithFormat:@"%d",count]:nil;
               }];
           }
       }];
    }
        
}

- (IBAction)onSignUpBtClicked:(id)sender {
    SignUpVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onForgotPasswordBtClicked:(id)sender {
    ForgotPasswordVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == tfEmail)
    {
        [tfPassword becomeFirstResponder];
    } else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - on Shake Event
- (void)onShakeEventOccurred
{
    static int number = 0;
    switch (number) {
        case 0:
        {
            tfEmail.text = @"minhaz123@gmail.com";
            tfPassword.text = @"12345678";
        }
            number++;
            break;
        
        case 1:
        {
            tfEmail.text = @"test@gmail.com";
            tfPassword.text = @"llllllll";
        }
            number = 0;
            break;

        default:
            break;
    }
}



@end
