//
//  DLHelper.h
//  UFeedback
//
//  Created by Moweb_10 on 20/04/16.
//  Copyright © 2016 Moweb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
#import "Constant.h"
@interface DLHelper : NSObject<CLLocationManagerDelegate,UIAlertViewDelegate>

@property(strong,nonatomic)NSString *lattitude;
@property(strong,nonatomic)NSString *longitude;

+(DLHelper *)shareinstance;
+(NSString *)UDID;
+(NSString *)DEVICETOKEN;
+(NSData *)DEVICETOKEN_DATA;
+(NSString *)ACCESSTOKEN;
+(NSString *)USERID;
+(NSInteger)LOGIN_TYPE;
+(NSString *)USEREMAIL;
+(NSString *)PROFILE_PIC;
@property(assign,nonatomic)NSInteger selectedIndex;

-(void)pedingAll:(UIView *)view;
-(void)TrimingAllTextfiled:(UIView *)view;
@property(retain,nonatomic)UITextField *textfild;
-(void)TrimmingSingTextField;
-(NSString *)Trimmingstring:(NSString *)obj;
-(void)KeyboardHide:(UIView *)view;
-(void)ClearTextfield:(UIView *)view;
- (CGFloat)getLabelHeight:(UILabel*)label;



-(void)DLSERVICE:(UIView *)view Indicater:(BOOL)indicater url:(NSString *)apiUrl UrlBody:(NSMutableDictionary *)apiBody Complete:(void(^)(NSMutableDictionary *JSON,NSInteger statuscode))completed;
-(void)DLSERVICE_IMAGE:(UIView *)view Indicater:(BOOL)indicater url:(NSString *)apiUrl UrlBody:
(NSMutableDictionary *)apiBody Complete:(void(^)(NSMutableDictionary *JSON,NSInteger statuscode))completed;
-(void)uploadWithURL:(NSString *)str andfilePath:(NSURL *)path andName:(NSString *)controlName andParameters:(NSMutableDictionary *)parameters success:(void(^)(id responce))successResponce failure:(void(^)(NSError *error))failureResponce;


-(void)DLImageStore:(UIImageView *)imageview urlString:(NSString *)url Complete:(void(^)(UIImage *image))completed;
- (UIImage *)blurredImageWithImage:(UIImage *)sourceImage;
-(void)DLImageStoreWithPlaceholderImage:(UIImage *)imageplacehoder Bureimageview:(UIImageView *)bureimageview imageview:(UIImageView *)imageview urlString:(NSString *)url Complete:(void(^)(UIImage *image))completed;
-(void)DLImageStoreWithPlaceholderImage:(UIImage *)imageplacehoder imageview:(UIImageView *)imageview urlString:(NSString *)url Complete:(void(^)(UIImage *image))completed;

typedef void (^DLActionsheet)(NSInteger DLindex);
@property(strong,nonatomic)DLActionsheet actionsheet;
-(void)DLActionsheet:(DLActionsheet)actionsheet title:(NSString *)title canceltile:(NSString *)canceltile othertitle:(NSString *)othertitle, ...NS_REQUIRES_NIL_TERMINATION ;

typedef void (^DLAlertView)(NSInteger DLindex);
@property(strong,nonatomic)DLAlertView alertView;
-(void)DLAlert:(DLAlertView)alertView title:(NSString *)title message:(NSString *)message canceltile:(NSString *)canceltile othertitle:(NSString *)othertitle, ...NS_REQUIRES_NIL_TERMINATION ;


-(void)setDidTapBlock:(void(^)(id button))didTapBlock;
@property(strong,nonatomic)void(^didTapBlock)(id button);
+(AppDelegate *)AppDelegate;
  @end
