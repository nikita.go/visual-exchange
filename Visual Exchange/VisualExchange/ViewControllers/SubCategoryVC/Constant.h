//
//  Constant.h
//  BestEmployee
//
//  Created by Moweb_10 on 22/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//
#import "PrefixHeader.pch"
#import "AppDelegate.h"
#ifndef Constant_h
#define Constant_h

#define kUserDefults(Value,Key)  [[NSUserDefaults standardUserDefaults]setValue:Value forKey:Key]
#define kUserDefults_(Key)       [[NSUserDefaults standardUserDefaults]valueForKey:Key]
#define kHudshow(view)          [MBProgressHUD showHUDAddedTo:view animated:true].labelText=@"Loading..."
#define kHudhide(view)           [MBProgressHUD hideAllHUDsForView:view animated:true]

/*/ DIRECT USER HELPER /*/
#define kACCESSTOKEN        [DLHelper ACCESSTOKEN]
#define kDEVICETOKEN        [DLHelper DEVICETOKEN]
#define kUDID               [DLHelper UDID]
#define kUSERID             [DLHelper USERID]
#define kDEVICETOKEN_DATA   [DLHelper DEVICETOKEN_DATA]
#define kUSER_TYPE         [DLHelper LOGIN_TYPE]
#define kUSEREMAIL   [DLHelper USEREMAIL]
#define kPROFILE_PIC         [DLHelper PROFILE_PIC]



/*/ UISCREEN & IPHONE /*/
#define IS_IPAD     [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad
#define IS_IPHONE_4 [UIScreen mainScreen].bounds.size.height==480
#define IS_IPHONE_5 [UIScreen mainScreen].bounds.size.height==568
#define IS_IPHONE_6 [UIScreen mainScreen].bounds.size.height==667
#define IS_IPHONE_6P [UIScreen mainScreen].bounds.size.height==736
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define Dlk(v)         v * SCREEN_WIDTH/320

// Contant Key

#define INTRO @"IS_FINISH_INTRO"
#define ISLOGIN @"IS_LOGIN"
#define ResultUser  @"Result+User"

#define BASE_URL @"http://www.thevisualexchange.com/visual/api/"
#define BASE_URL_NEW @"http://www.thevisualexchange.com/visualweb/api/get/"
#define LINKEDIN_CLIENT_ID      @"75fyazdgydng5w"
#define LINKEDIN_CLIENT_SECRET  @"HPi3KVTds2QO8CU8"

// ErrorMessages

#define  dlkJobID_Invalide  @"Please provide Job ID"
#define  dlkJobTitle_Invalide  @"Please provide Job Title"
#define  dlkJoblocation_Invalide  @"Please provide Job location"
#define  dlkPhone_Invalide  @"Please enter a 10 digit phone number."
#define  dlkWeb_Invalide  @"Please provide Valid Website.\n Ex:- 'https://VisualExchange.com'"
#define dlkSalaryRang_Invalide @"Please Enter Salary"
#define dlkNoInterNet @"You require an internet connection via WiFi or cellular network for location finding to work."
#define dlkServer_Off @"Some error occurred, please try again."

#define dlkmanager_Invalide @""

#define invalidPhoneNo @"Please enter a 10 digit phone number."
#define invalidEmail @"Please enter valid email address."
#define enterEmail @"Please provide your email address."

#define invalidCountryCode @"Please enter valid country code."
#define enterValidCountryCode @"Please enter country code."

#define enterCity @"Please enter city."
#define enterCompany @"Please enter company."
#define invalidCompany @"Please enter valid company name."
#define enterName @"Please provide your name."

#define enterTitle @"Please enter title."
#define invalidTitle @"Title contains certain special characters and it is not allowed."

#define enterdescription @"Please enter description."
#define invalidDescription @"Description is very short. We would need a detailed description."

#define invalidAnnualSalary @"Annual salary must be less than 7 digits."
#define invalidHourlySalary @"Hourly salary must be less than 4 digits."
#define annualSalaryShouldBeLessThan 6
#define hourlySalaryShouldBeLessThan 3

#define charAllowedForCountryCode @"0123456789+"
#define charAllowedForCompanyFields @" 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz-/:;()@\".,!'[]{}#=_\\|~<>"
#define charAllowedForCityAndTitleFields @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
#define charAllowedForCityFields @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "

#define dlkName @"Please enter valid name"
#define dlkEmail @"Please enter valid email address"
#define dlkAddress @"Please enter valid address"
#define dlkCity @"Please enter valid city"
#define dlkState @"Please enter valid state"
#define dlkCountry @"Please Enter valid country"
#define dlkZipcode @"Please enter valid zipcode"
#define dlkMobile @"Please enter valid mobile No."

#define dlkOldPass @"Please enter at least 6 characters."
#define dlkNewPass @"Please enter valid new password.               at least 6 characters."
#define dlkPassMis @"New and confirm passwords do not match."
#define dlkSuccesspass @"Password successfully changed."
#define dlkloginsignup @"You need to login first"
#define dlkOneVideo @"You can upload only video."
#define dlkImage5 @"You can upload upto 5 images."
#define dlkimageSelect @"Please select product image"
#define dlkproductname @"Please enter product Name"
#define dlkVideoSelect @"Please enter videoLink"
#define dlkProductCategory @"Please select category"
#define dlkProductDescription @"Please enter valid description"
#define dlkProductWaight @"Please enter valid weight"
#define dlkProductPrice @"Please enter valid price"
#define dlkMinimumImage @"You need to add minimum one image"
#define dlkimageProfile @"Please select  profilePic"
#define dlkSuccessProfile @"User profile updated successfully"
#define dlkCardAddress @"Please select address"
#define dlkProductVideoMiss @"No video available for this product"
#define dlkProductSKU @"Please enter SKU of product"
#define dlkStockLimit @"Please enter stock limit"
#define dlkLeadtime @"Please enter lead time"
#define dlkheight @"Please enter height"
#define dlkwidth @"Please enter width"
#define dlklength @"Please enter length"
#define OpenSenceSB  @"OpenSans"
#define OpenSenceB  @"OpenSans-Semibold"
#define RobotoCondensed @"Roboto Condensed"
#define MVBOLI @"MV Boli"
#define RobotoCondensedRegular @"RobotoCondensed-Regular"
#define RobotoLight @"Roboto-Light"
#define RobotoRegular @"Roboto-Regular"
#define RobotoMedium @"Roboto-Medium"
#define actionSheetTitleFontSize 15
#define DLAutomatic(v)  IS_IPAD?v*1.3: v * SCREEN_WIDTH/320
//Font Style as per latest guide

#define devPlaceholderColor [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1] //939393
#define devTextBigTitleColor [UIColor colorWithRed:31/255.0 green:31/255.0 blue:31/255.0 alpha:1] // 1f1f1f
#define devSmallTitleColor [UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1] //6d6d6d
#define tableviewSeparatorColor [UIColor colorWithRed:184/255.0 green:184/255.0 blue:184/255.0 alpha:1] //b8b8b8
#define AppyelloColor [UIColor colorWithRed:254/255.0 green:192/255.0 blue:6/255.0 alpha:1]

#endif /* Constant_h */
