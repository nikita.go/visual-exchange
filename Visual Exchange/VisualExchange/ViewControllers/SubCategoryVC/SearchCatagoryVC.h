//
//  ViewController.h
//  DemoProject
//
//  Created by Nilay Shah on 11/07/16.
//  Copyright © 2016 Nilay Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLHelper.h"
#import "SearchCatagoryCell.h"
#import "ProductCell.h"
#import "ProductDetailsVC.h"
#import "ProductVC.h"
@interface SearchCatagoryVC : BaseVC<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *alertlbl;
@property (strong, nonatomic) IBOutlet UIView *searchview;


@property(strong,nonatomic)NSString *orderstr;
@property (strong, nonatomic) IBOutlet UILabel *perfectlbl;

@property (strong, nonatomic) IBOutlet UILabel *seacondlbl;
@property (strong, nonatomic) IBOutlet UILabel *oderrbl;
@property (strong, nonatomic) IBOutlet UILabel *thirdlbl;
@property (strong, nonatomic) IBOutlet UILabel *frstlbl;
@property (weak, nonatomic) IBOutlet UIView *searchviewupdated;

@end

