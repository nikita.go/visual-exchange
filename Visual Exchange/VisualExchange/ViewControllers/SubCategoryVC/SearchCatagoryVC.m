//
//  ViewController.m
//  DemoProject
//
//  Created by Nilay Shah on 11/07/16.
//  Copyright © 2016 Nilay Shah. All rights reserved.
//

#import "SearchCatagoryVC.h"

@interface SearchCatagoryVC (){
    
    NSMutableArray *arraySearch;
}@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation SearchCatagoryVC

-(void)viewWillAppear:(BOOL)animated
{
    if(_orderstr.length>0)
    {
        _alertlbl.hidden=YES;
        _tableview.hidden=YES;
        _searchview.hidden=YES;
        _txtSearch.hidden=YES;
        
        self.tableview.backgroundColor = [UIColor clearColor];
        
        self.view.backgroundColor = [UIColor colorWithRed:245/255.f green:245/255.f blue:245/255.f alpha:1.0];
     
        _perfectlbl.hidden=NO;
        _seacondlbl.hidden=NO;
        
        _thirdlbl.hidden=NO;
        
        _frstlbl.hidden=NO;
        
        NSString *output=[NSString stringWithFormat:@"Your order #%@ has been",_orderstr];
        NSUInteger a=_orderstr.length;
        a=a+2;
       
       // NSMutableAttributedString *attrstr=[[NSMutableAttributedString alloc]initWithString:output];
       // [attrstr addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(10, a)] ;
        UIFont *font1=[UIFont fontWithName:@"OpenSans-Bold" size:17.0f];
      //  NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
       // //NSDictionary *vdict=[[NSDictionary alloc]init];
        //[vdict setValue:font1 forKey:NSFontAttributeName];
        NSMutableAttributedString *vAttrString=[[NSMutableAttributedString alloc]initWithString:output];
       
        //[attrstr addAttribute:NSFontAttributeName value:font range:NSMakeRange(10, a)];
        [vAttrString addAttribute:NSForegroundColorAttributeName value: [UIColor colorWithRed:153/255.0 green:193/255.0 blue:58/255.0 alpha:1.0] range:(NSMakeRange(10, a))];
        NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:vAttrString];
        [newString addAttribute:NSFontAttributeName value:font1  range:(NSMakeRange(10, a))];
        _frstlbl.attributedText=newString;
        
    }
    else
    {
       
        _alertlbl.hidden=YES;
        _perfectlbl.hidden=YES;
        _seacondlbl.hidden=YES;
        
        _thirdlbl.hidden=YES;
        
        _frstlbl.hidden=YES;
        _oderrbl.hidden=YES;
        
      //  [[DLHelper shareinstance]pedingAll:self.txtSearch.superview];
//        self.txtSearch.layer.borderColor = [UIColor colorWithRed:153/255.f green:193/255.f blue:58/255.f alpha:1.0].CGColor;
//        self.txtSearch.layer.cornerRadius = 4;
//        self.txtSearch.layer.borderWidth = 1;
//        self.txtSearch.layer.masksToBounds = true;
        self.tableview.tableFooterView = [UIView new];
        self.tableview.backgroundColor = [UIColor clearColor];
        
        self.searchviewupdated.layer.borderColor = [UIColor colorWithRed:153/255.f green:193/255.f blue:58/255.f alpha:1.0].CGColor;
        self.searchviewupdated.layer.cornerRadius = 4;
        self.searchviewupdated.layer.borderWidth = 1;
        self.searchviewupdated.layer.masksToBounds = true;

        
        self.view.backgroundColor = [UIColor colorWithRed:245/255.f green:245/255.f blue:245/255.f alpha:1.0];
        
        
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([SearchCatagoryCell class]) bundle:nil];
        [self.tableview registerNib:nib forCellReuseIdentifier:@"SearchCatagoryCell"];
        
        UINib *nibProduct = [UINib nibWithNibName:NSStringFromClass([ProductCell class]) bundle:nil];
        [self.tableview registerNib:nibProduct forCellReuseIdentifier:@"ProductCell"];
        
    }
    
}

#pragma mark - ViewController life cycle -
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [DLHelper AppDelegate].tabbarController.tabBar.hidden =false;
    
       [DLHelper shareinstance].selectedIndex=3;

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextfield Delegate -
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self SearchPress:nil];
    [textField resignFirstResponder];
    return true;
}
#pragma mark -UITAbleview DataSource & Delegate -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
       return arraySearch.count;;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(![arraySearch[indexPath.row][@"type"] isEqualToString:@"Product"]){
        
    SearchCatagoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCatagoryCell" forIndexPath:indexPath];
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:cell.imgmain urlString:arraySearch[indexPath.row][@"image"] Complete:^(UIImage *image) {
        
    }];
        cell.lblName.text = arraySearch[indexPath.row][@"name"];
        
        UIFont *yourFont = cell.lblName.font;
        CGSize stringBoundingBox = [arraySearch[indexPath.row][@"name"] sizeWithFont:yourFont];
        cell.viewAlfa.frame = CGRectMake((SCREEN_WIDTH-stringBoundingBox.width + 15)/2,(cell.frame.size.height/2)-10, stringBoundingBox.width + 15, stringBoundingBox.height+15);

        return cell;
    }
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    
    NSString *imagepath = nil;
    if([arraySearch[indexPath.row][@"images"]count]>0){
        imagepath = arraySearch[indexPath.row][@"images"][0][@"product_image"];
    }else{
        imagepath = arraySearch[indexPath.row][@"image"];
    }
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:cell.imgmain urlString:imagepath Complete:^(UIImage *image) {}];
    
    cell.lblName.text = arraySearch[indexPath.row][@"name"];
    NSString *subtitle = [NSString stringWithFormat:@"%@ - by %@",arraySearch[indexPath.row][@"categoryname"],arraySearch[indexPath.row][@"customer_name"]];
    cell.lblSubtitle.text = subtitle;
    cell.lblprice.text = [NSString stringWithFormat:@"$%@",arraySearch[indexPath.row][@"price"]];
    cell.clipsToBounds = true;
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([arraySearch[indexPath.row][@"type"] isEqualToString:@"Product"]){
        ProductDetailsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailsVC"];
        vc.product = [arraySearch[indexPath.row]mutableCopy];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        ProductVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductVC"];
        vc.category = [arraySearch[indexPath.row]mutableCopy];
         [self.navigationController pushViewController:vc animated:YES];
        
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(![arraySearch[indexPath.row][@"type"] isEqualToString:@"Product"])
         return 200*SCREEN_HEIGHT/568;
         return 300*SCREEN_HEIGHT/568;
}
#pragma mark -IBAction Methods -
- (IBAction)SearchPress:(id)sender {
    
    if(self.txtSearch.text.length>0){
        [[[UIApplication sharedApplication]keyWindow]endEditing:true];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"keyword"] = self.txtSearch.text;
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"productsearch.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        for(NSMutableDictionary *dict in JSON[@"productList"]){
            dict[@"categorie_id"] = dict[@"categori_id"];
            dict[@"categori_id"] = nil;
        }
        arraySearch = [JSON[@"productList"]mutableCopy];
        if(arraySearch.count>0)
        {
            _tableview.hidden=NO;
            _alertlbl.hidden=YES;
        [self.tableview reloadData];
        }
        else{
            _tableview.hidden=YES;
            _alertlbl.hidden=NO;
            _alertlbl.text=@"No Result Found";
        }
        
    }];
    }
}
- (IBAction)BackPress:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)MenuPress:(id)sender {
    [[AppLauncher sharedInstance]openLeftDrawer];
}

@end
