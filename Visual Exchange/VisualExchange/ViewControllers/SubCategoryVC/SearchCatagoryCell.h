//
//  SearchCatagoryCell.h
//  DemoProject
//
//  Created by Nilay Shah on 11/07/16.
//  Copyright © 2016 Nilay Shah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCatagoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgmain;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viewAlfa;


@end
