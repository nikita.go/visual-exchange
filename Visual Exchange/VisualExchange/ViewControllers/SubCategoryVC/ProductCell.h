//
//  ProductCell.h
//  DemoProject
//
//  Created by Nilay Shah on 11/07/16.
//  Copyright © 2016 Nilay Shah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblprice;
@property (weak, nonatomic) IBOutlet UIImageView *imgmain;
-(void)setDidtapBlock:(void(^)(UIButton *button))compition;
@property(strong,nonatomic)void(^didtapBlock)(UIButton *button);
@end
