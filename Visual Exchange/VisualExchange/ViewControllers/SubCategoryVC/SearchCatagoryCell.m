//
//  SearchCatagoryCell.m
//  DemoProject
//
//  Created by Nilay Shah on 11/07/16.
//  Copyright © 2016 Nilay Shah. All rights reserved.
//

#import "SearchCatagoryCell.h"

@implementation SearchCatagoryCell

- (void)awakeFromNib {
    // Initialization code
    self.imgmain.clipsToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
