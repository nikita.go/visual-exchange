//
//  CategorySelectionViewController.h
//  VisualExchange
//
//  Created by mac on 12/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryTableViewCell.h"
@interface CategorySelectionViewController : UIViewController
{
    NSMutableArray *arrStates;
     NSMutableArray *categoryidary;
    CategoryTableViewCell *cell;
    NSMutableArray *selectionary;
}
@property (weak, nonatomic) IBOutlet UITableView *tblview;
@property (weak, nonatomic) IBOutlet UIButton *savebtn;
- (IBAction)saevbtnpress:(id)sender;
- (IBAction)backbtnpress:(id)sender;
@property(strong,nonatomic)NSMutableArray *passcatary;
@end
