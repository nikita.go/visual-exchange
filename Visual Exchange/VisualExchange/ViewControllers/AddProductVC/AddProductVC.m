//
//  AddProductVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 28/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "AddProductVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "CategorySelectionViewController.h"
#import "TotalOrderVC.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface AddProductVC ()
{
    NSMutableArray *arrayVideo;
    NSMutableArray *arrStates;
    
    AVAudioPlayer *audioPlayer;
    MPMoviePlayerViewController *moviePlayer;
        
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *Scrollview;
- (IBAction)datevaluechange:(id)sender;

@end

@implementation AddProductVC

- (void)viewDidLoad {
    [super viewDidLoad];
      _datepikcerview.hidden=YES;
    
    arrayVideo = [NSMutableArray new];
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
   
    [self.scollMain AutomaticScrollview];
}
-(void)viewWillAppear:(BOOL)animated{
    if(_passcateary.count>0)
    {
        NSMutableArray *stringary=[[NSMutableArray alloc]init];
        idary=[[NSMutableArray alloc]init];
        for (int i=0 ; i<_passcateary.count; i++) {
            [stringary addObject:[[_passcateary objectAtIndex:i]objectForKey:@"name"]];
            [idary addObject:[[_passcateary objectAtIndex:i]objectForKey:@"categorie_id"]];
        }
        NSString *str =[NSString stringWithFormat:@"%@",stringary];
        
        NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
        
        
        NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
        NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
        self.txtCategory.text =finalstr;
        
    }
    
    

}
- (void)fetchStates
{
//    [self.viewAlltextfield endEditing:YES];
//    
//    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"categoriesList.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
//        arrStates = [JSON[@"country"]mutableCopy];
//        
//    
//        CategorySelectionViewController * obj = [ self.storyboard instantiateViewControllerWithIdentifier:@"CategorySelectionViewController"];
//        [self.navigationController pushViewController:obj animated:true];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CategorySelectionViewController *dealVC1 = (CategorySelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"CategorySelectionViewController"];
    dealVC1.passcatary=_passcateary;
        [self.navigationController pushViewController:dealVC1 animated:YES];
//        PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
//        pickerViewController.pickerType = CustomPickerType;
//        pickerViewController.dataSourceForCustomPickerType = [arrStates valueForKey:@"name"];
//        [pickerViewController setInitialItemAtIndex:0];
//        pickerViewController.delegate = self;
//        [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
//        [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
//        
        
   // }];
    
}
- (void)didSelectItemAtIndex:(NSUInteger)index{
    self.txtCategory.text = arrStates[index][@"name"];
}
#pragma mark - Get uer object -
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == self.txtCategory){
        [textField resignFirstResponder];
        
    }
    return true;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtProductname)
    {
        [_txtVideoLink becomeFirstResponder];
    } else if (textField == _txtVideoLink)
    {
        [_txtVideoLink resignFirstResponder];
    }
    else if (textField == _txtDescription)
    {
        [_txtWight becomeFirstResponder];
    }
    else if (textField == _txtWight)
    {
        [_txtPrice becomeFirstResponder];
    }
    else if (textField == _txtPrice)
    {
        [_txtSKU becomeFirstResponder];
    }
    else if (textField == _txtSKU)
    {
        [_txtstocklimit becomeFirstResponder];
    }
    else if (textField == _txtstocklimit)
    {
        [_txtstocklimit resignFirstResponder];
    }
    else if (textField == _txtlegth)
    {
        [_txtheight resignFirstResponder];
    }
    else if (textField == _txtheight)
    {
        [_txtwidth resignFirstResponder];
    }
    else if (textField == _txtwidth)
    {
        [_txtwidth resignFirstResponder];
    }
    else if (textField == _txtleadtime)
    {
        [_txtlegth becomeFirstResponder];
    }

//    else
//    {
//        [textField resignFirstResponder];
//    }
    return YES;

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(actionSheet.tag==100){
        
        if (buttonIndex == 0) { // Camera
            [self openVideoPickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        } else  if (buttonIndex == 1) { // Photo Gallery
            [self openVideoPickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        return;

    }
    if (buttonIndex == 0) { // Camera
        YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
        camController.delegate=self;
        [self presentViewController:camController animated:YES completion:^{
            // completion code
        }];
    } else  if (buttonIndex == 1) { // Photo Gallery
        
        
        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
        
        photoPicker.cropBlock = ^(UIImage *image) {
            //do something
            [self SetingsImages:image];
        };
        
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
        [navCon setNavigationBarHidden:YES];
        
        [self presentViewController:navCon animated:YES completion:NULL];

       

    }
}
- (void)didFinishPickingImage:(UIImage *)image{
    [self  SetingsImages:image];
}
- (void)openImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

        UIImagePickerController *ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = sourceType;
        ipc.allowsEditing = NO;
        [self presentViewController:ipc animated:YES completion:nil];
    }];
}
- (void)openVideoPickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = sourceType;
        ipc.allowsEditing = NO;
        ipc.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
        [ipc setVideoMaximumDuration:10];
        ipc.allowsEditing = YES;
        [self presentViewController:ipc animated:YES completion:nil];
    }];
}
- (IBAction)PhotoPress:(id)sender {
    
    NSLog(@"%@",sender);
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Gallery",nil];
    [actionSheet showInView:self.view];

    
    
}
- (IBAction)VideoPress:(id)sender {
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose Video from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;


}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info


{
    

    
    [self dismissViewControllerAnimated:YES completion:^{
        
        
        [self.scollMain viewWithTag:2525].hidden = false;
        
        UIImageView *btnImage;
        UIButton *deleteButton;
        
        NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        if ( [mediaType isEqualToString:@"public.movie" ])
        {
     
            if ([self isVideoAvalable]){
                [self showErrorWithMessage:dlkOneVideo];
                return;
            }
            NSLog(@"Picked a movie at URL %@",  [info objectForKey:UIImagePickerControllerMediaURL]);
            NSURL *url =  [info objectForKey:UIImagePickerControllerMediaURL];
            NSLog(@"> %@", [url absoluteString]);
            
            CGFloat imageHeight = SCREEN_WIDTH/6;
            
            
            
            CGRect rect =  CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 0, imageHeight,imageHeight);
            if(arrayVideo.count<3){
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+20;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
                rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
                
            }else{
                
                NSInteger arraycount = arrayVideo.count-3;
                rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+imageHeight+55;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
            }
            
            btnImage  = [UIImageView new];
            btnImage.frame = rect;
            btnImage.image = [UIImage imageNamed:@"play"];
            [btnImage setTag:arrayVideo.count];
            btnImage.layer.cornerRadius = 4;
            btnImage.layer.masksToBounds = true;
           
            
            deleteButton  =    [[UIButton alloc]initWithFrame:CGRectMake(rect.size.width-20, -20, 40, 40)];
            deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
            deleteButton.clipsToBounds = false;
            deleteButton.tag = btnImage.tag;
            [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
            deleteButton.userInteractionEnabled = true;
            btnImage.frame = CGRectMake(0, 0, imageHeight, imageHeight);
            btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
            UIView *view = [[UIView alloc]initWithFrame:rect];
            view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
            [view addSubview:btnImage];
            view.clipsToBounds = false;
            view.tag = btnImage.tag;
            view.backgroundColor = [UIColor clearColor];
            [view addSubview:deleteButton];
            [self.Scrollview addSubview:view];
            [arrayVideo addObject:url];
            [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
            //[btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];

        }else{
            
            NSInteger countt = 5;
            if ([arrayVideo containsObject:[NSURL class]]){
                countt = 6;
            }
            if (countt <= arrayVideo.count) {
                [self showErrorWithMessage:dlkImage5];
                return;
            }
            UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
            CGFloat imageHeight = (SCREEN_WIDTH/6);
            
       
            CGRect rect;
            if(arrayVideo.count<3){
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+20;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
            rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
                
            }else{
             
                NSInteger arraycount = arrayVideo.count-3;
                 rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+imageHeight+55;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
            }
            
            btnImage  = [UIImageView new];
            btnImage.frame = rect;
            btnImage.image = img; // setBackgroundImage:img forState:UIControlStateNormal];
            [btnImage setTag:arrayVideo.count];
            btnImage.layer.cornerRadius = 4;
            btnImage.layer.masksToBounds = true;
            //[self.Scrollview addSubview:btnImage];

            [arrayVideo addObject:img];
            
           
            
            
            UIButton *   deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
            deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
            deleteButton.clipsToBounds = false;
            deleteButton.tag = btnImage.tag;
            [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
            deleteButton.userInteractionEnabled = true;
            
            btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
            UIView *view = [[UIView alloc]initWithFrame:rect];
            view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
            [view addSubview:btnImage];
             view.tag = btnImage.tag;
            view.clipsToBounds = false;
            view.backgroundColor = [UIColor clearColor];
            [view addSubview:deleteButton];
            [self.Scrollview addSubview:view];
            [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
           // [btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
        }
       

        CGRect rectmain = self.viewAlltextfield.frame;
        rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
        self.viewAlltextfield.frame = rectmain;
        
    }];
}
-(void)SetingsImages:(UIImage *)img{
    
    UIImageView *btnImage;
    NSInteger countt = 5;
    if ([arrayVideo containsObject:[NSURL class]]){
        countt = 6;
    }
    if (countt <= arrayVideo.count) {
        [self showErrorWithMessage:dlkImage5];
        return;
    }
    //UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    CGFloat imageHeight = (SCREEN_WIDTH/6);
    
    
    CGRect rect;
    if(arrayVideo.count<3){
        CGRect rectScroll = self.Scrollview.frame;
        rectScroll.origin.x = 0;
        rectScroll.size.height = imageHeight+20;
        rectScroll.size.width = SCREEN_WIDTH;
        self.Scrollview.frame = rectScroll;
        rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
        
    }else{
        
        NSInteger arraycount = arrayVideo.count-3;
        rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
        CGRect rectScroll = self.Scrollview.frame;
        rectScroll.origin.x = 0;
        rectScroll.size.height = imageHeight+imageHeight+55;
        rectScroll.size.width = SCREEN_WIDTH;
        self.Scrollview.frame = rectScroll;
    }
    
    btnImage  = [UIImageView new];
    btnImage.frame = rect;
    btnImage.image = img; // setBackgroundImage:img forState:UIControlStateNormal];
    [btnImage setTag:arrayVideo.count];
    btnImage.layer.cornerRadius = 4;
    btnImage.layer.masksToBounds = true;
    //[self.Scrollview addSubview:btnImage];
    
    [arrayVideo addObject:img];
    
    
    
    
    UIButton *   deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
    deleteButton.clipsToBounds = false;
    deleteButton.tag = btnImage.tag;
    [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    deleteButton.userInteractionEnabled = true;
    
    btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
    UIView *view = [[UIView alloc]initWithFrame:rect];
    view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
    [view addSubview:btnImage];
    view.tag = btnImage.tag;
    view.clipsToBounds = false;
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:deleteButton];
    [self.Scrollview addSubview:view];
    [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
    // [btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];



CGRect rectmain = self.viewAlltextfield.frame;
rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
self.viewAlltextfield.frame = rectmain;
    

}
- (IBAction)UpdatePress:(id)sender {
    
    [self.view endEditing: true];
    if ([self isVideoAvalable] && arrayVideo.count<2) {
         [self showErrorWithMessage:dlkimageSelect];
    }
   else if (arrayVideo.count<1) {
        [self showErrorWithMessage:dlkimageSelect];
    }
    else if (self.txtProductname.text.isEmptyString) {
        [self showErrorWithMessage:dlkproductname];
    }
//    else if (!self.txtVideoLink.text.isValidateUrl) {
//        [self showErrorWithMessage:dlkVideoSelect];
//    }
    
    else if (self.txtCategory.text.isEmptyString) {
        [self showErrorWithMessage:dlkProductCategory];
    }
    else if (self.txtDescription.text.isEmptyString) {
        [self showErrorWithMessage:dlkProductDescription];
    }
    else if ([self.txtWight.text floatValue]==0) {
        [self showErrorWithMessage:dlkProductWaight];
    }
    else if ([self.txtPrice.text floatValue]==0) {
        [self showErrorWithMessage:dlkProductPrice];
    }
    else if ([self.txtSKU.text isEmptyString]) {
        [self showErrorWithMessage:dlkProductSKU];
    }
    else if ([self.txtstocklimit.text isEmptyString]) {
        [self showErrorWithMessage:dlkStockLimit];
    }
    else if ([self.txtleadtime.text isEmptyString]) {
        [self showErrorWithMessage:dlkLeadtime];
    }
    else if ([self.txtheight.text isEmptyString]) {
        [self showErrorWithMessage:dlkheight];
    }
    else if ([self.txtwidth.text isEmptyString]) {
        [self showErrorWithMessage:dlkwidth];
    }
    else if ([self.txtlegth.text isEmptyString]) {
        [self showErrorWithMessage:dlklength];
    }else{
        
        NSString * stateid = nil;
        for(int i=0; i<arrStates.count; i++){
            if([self.txtCategory.text isEqualToString:arrStates[i][@"name"]]){
                stateid = arrStates[i][@"categorie_id"];
                break;break;
            }
        }

        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"customer_id"] =  [NSString stringWithFormat:@"%@",kUserDefults_(ResultUser)[@"customer_id"]];
         dict[@"price"] = self.txtPrice.text;
        
        
        
        NSString *str =[NSString stringWithFormat:@"%@",idary];
        
        NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
        
        
        NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
        NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];

         dict[@"category_id"] = finalstr;
         dict[@"details"] = self.txtDescription.text;
         dict[@"product_name"] = self.txtProductname.text;
         dict[@"weight"] = self.txtWight.text;
         dict[@"stock_limit"] = self.txtstocklimit.text;
        dict[@"lead_time"] = self.txtleadtime.text;
        dict[@"width"] = self.txtwidth.text;
        dict[@"height"] = self.txtheight.text;
        dict[@"length"] = self.txtlegth.text;
        

        dict[@"sku"] = self.txtSKU.text;

         dict[@"image"] = [self uploadMultiplePics];
        dict[@"video_link"] = self.txtVideoLink.text;
        [[DLHelper shareinstance]DLSERVICE_IMAGE:self.view Indicater:true url:@"addproduct.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TotalOrderVC *dealVC1 = (TotalOrderVC *)[storyboard instantiateViewControllerWithIdentifier:@"TotalOrderVC"];
            [self.navigationController pushViewController:dealVC1 animated:YES];
            //[self.navigationController popViewControllerAnimated:true];
            
        }];

        
        
    }
}
- (IBAction)BackPress:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}
-(IBAction)DeletePress:(UIButton *)sender{
    
    
    
    
                    [sender.superview removeFromSuperview];
                    [arrayVideo removeObjectAtIndex:sender.superview.tag];
                    if(arrayVideo.count==0){
                        [self.scollMain viewWithTag:2525].hidden = true;
                    }
            CGRect rect;
            for (UIView *v in self.Scrollview.subviews) {
                
             
                if(v.tag>sender.superview.tag){
                    v.tag = v.tag-1;
                }
                NSLog(@"%ld",(long)v.tag);
                
                
                CGFloat imageHeight = (SCREEN_WIDTH/6);
                
                
                
                if(v.tag<3){
                    CGRect rectScroll = self.Scrollview.frame;
                    rectScroll.origin.x = 0;
                    rectScroll.size.height = imageHeight+20;
                    rectScroll.size.width = SCREEN_WIDTH;
                    self.Scrollview.frame = rectScroll;
                    rect = CGRectMake(v.tag*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
                    
                    
                }else{
                    
                    NSInteger arraycount = v.tag-3;
                    rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
                    CGRect rectScroll = self.Scrollview.frame;
                    rectScroll.origin.x = 0;
                    rectScroll.size.height = imageHeight+imageHeight+55;
                    rectScroll.size.width = SCREEN_WIDTH;
                    self.Scrollview.frame = rectScroll;
                }
                

                 v.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
                
                  
            }
            
            CGRect rectmain = self.viewAlltextfield.frame;
            rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
            self.viewAlltextfield.frame = rectmain;

        
}
-(NSMutableData *)uploadMultiplePics
{
    NSString *string ;
    NSData *imageData;
     NSMutableData *body;
    body = [NSMutableData data];
    for(int j=0;j < arrayVideo.count;j++) // scrollViewImageArray is images count
    {
        
        if ([arrayVideo[j] isKindOfClass:[UIImage class]]){
        double my_time = [[NSDate date] timeIntervalSince1970];
       
        NSString *imageName = [NSString stringWithFormat:@"%d%d",j,(int)(my_time)];
        NSString *imagetag=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image[]\"; filename=\""];
        string = [NSString stringWithFormat:@"%@%@%@", imagetag, imageName, @".jpg\"\r\n\""];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithString:string] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        UIImage *image=[[UIImage alloc]init];
        image=[arrayVideo objectAtIndex:j];
                imageData = UIImageJPEGRepresentation(image, .30);
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }else{
            
            NSData *data = [NSData dataWithContentsOfURL:arrayVideo[j]];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"video\"; filename=\"a.mov\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: video/mov\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:data];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

            
        }
    }
    

    return body;

}
//- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//
//    
//    
//    [self.scollMain viewWithTag:2525].hidden = false;
//    
//    UIImageView *btnImage;
// 
//   // NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
//    for (NSDictionary *dict in info) {
//        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
//            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
//            UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
//                
//                
//                CGFloat imageHeight = (SCREEN_WIDTH/6);
//                CGRect rect;
//                if(arrayVideo.count<3){
//                    CGRect rectScroll = self.Scrollview.frame;
//                    rectScroll.origin.x = 0;
//                    rectScroll.size.height = imageHeight+20;
//                    rectScroll.size.width = SCREEN_WIDTH;
//                    self.Scrollview.frame = rectScroll;
//                    rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
//                    
//                }else{
//                    
//                    NSInteger arraycount = arrayVideo.count-3;
//                    rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
//                    CGRect rectScroll = self.Scrollview.frame;
//                    rectScroll.origin.x = 0;
//                    rectScroll.size.height = imageHeight+imageHeight+55;
//                    rectScroll.size.width = SCREEN_WIDTH;
//                    self.Scrollview.frame = rectScroll;
//                }
//                
//                btnImage  = [UIImageView new];
//                btnImage.frame = rect;
//                btnImage.image = image;
//                [btnImage setTag:arrayVideo.count];
//                btnImage.layer.cornerRadius = 4;
//                btnImage.layer.masksToBounds = true;
//                //[self.Scrollview addSubview:btnImage];
//                
//             
//                UIButton *   deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
//                                              deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
//                deleteButton.clipsToBounds = false;
//                deleteButton.tag = btnImage.tag;
//                [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
//                deleteButton.userInteractionEnabled = true;
//                
//                btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
//                UIView *view = [[UIView alloc]initWithFrame:rect];
//                view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
//                [view addSubview:btnImage];
//               // view.backgroundColor =[UIColor blackColor];
//                view.tag = btnImage.tag;
//                view.clipsToBounds = false;
//                view.backgroundColor = [UIColor clearColor];
//                [view addSubview:deleteButton];
//                view.userInteractionEnabled = true;
//                [self.Scrollview addSubview:view];
//                [arrayVideo addObject:image];
//            
//                 [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
//                //[btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
//            }
//          //  [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
//            
//            
//            CGRect rectmain = self.viewAlltextfield.frame;
//            rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
//            self.viewAlltextfield.frame = rectmain;
//           
//                
//    }}}
//
//- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
-(BOOL)isVideoAvalable{
    int conn = 0;
    for (id view  in arrayVideo){
        if (![view isKindOfClass:[UIImage class]]){
            conn++;
        }
    }
    if (conn > 0){
        
        return true;
    }
    return NO;
}

- (IBAction)catbtnpress:(id)sender
{
    [self fetchStates];

}
- (IBAction)datevaluechange:(id)sender
{
    
}
- (IBAction)leadimebtnpress:(id)sender
{
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;

    _datepikcerview.hidden=NO;
}
- (IBAction)cancelbtnpress:(id)sender
{
      [DLHelper AppDelegate].tabbarController.tabBar.hidden =false;
     _datepikcerview.hidden=YES;
}

- (IBAction)donebtnpress:(id)sender
{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"]; //12 hr time format
    NSString *dateString = [outputFormatter stringFromDate:self.datepicker.date];
    _txtleadtime.text=dateString;
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = false;
     _datepikcerview.hidden=YES;

}
@end
