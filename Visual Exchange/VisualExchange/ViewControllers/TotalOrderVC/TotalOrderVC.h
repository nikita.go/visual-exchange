//
//  TotalOrderVC.h
//  VisualExchange
//
//  Created by Nilay Shah on 29/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalOrderVC : BaseVC <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *todate;
@property (weak, nonatomic) IBOutlet UILabel *fromdate;


@end
