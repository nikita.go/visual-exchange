//
//  ProductTotalCell.m
//  VisualExchange
//
//  Created by Nilay Shah on 05/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "ProductTotalCell.h"

@implementation ProductTotalCell

- (void)awakeFromNib {
    // Initialization code
    self.scrollview.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)DeletePress:(id)sender {
  if(self.didtapBlock){
      self.didtapBlock(sender);
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat pageWidth = self.scrollview.frame.size.width;
    int page = floor((self.scrollview.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControll.currentPage = page;
}
@end
