//
//  TotalOrderVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 29/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "TotalOrderVC.h"

@interface TotalOrderVC ()
{
    NSMutableArray *arrayOrder;
    UIButton *btntemp;
    UILabel *templbl;
    NSInteger ordertag;
    NSMutableDictionary *dictData;
}

@end

@implementation TotalOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableview.backgroundColor = [UIColor clearColor];
    _tableview.opaque = NO;
    _tableview.backgroundView = nil;
    ordertag = 1;
    self.view.frame = [UIScreen mainScreen].bounds;
    [self.view layoutIfNeeded];
    UINib *nibProduct = [UINib nibWithNibName:NSStringFromClass([OrderCell class]) bundle:nil];
    [self.tableview registerNib:nibProduct forCellReuseIdentifier:@"OrderCell"];
    
    
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if(!kUserDefults_(ResultUser))
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"You need to Login/Sign up frist" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK" ,nil];
        
        [alert show];
        
    }
    else{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"totalorders" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        
      
        
        dictData = [JSON mutableCopy];
        [self ordermange];
        
        
    }];
    }
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mart - UITableview Delegate -
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section==0){
        return 1;
    }
    return arrayOrder.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    if(indexPath.section == 0){
        
        CelenderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CelenderCell" forIndexPath:indexPath];
        cell.lblRevenue.text = [NSString stringWithFormat:@"%lu",[dictData[@"ProductList"]count]];
        cell.lblPaid.text = [NSString stringWithFormat:@"%lu",[dictData[@"orderstList"]count]];
        cell.lblBalence.text = [NSString stringWithFormat:@"%lu",[dictData[@"UnshippedOrderList"]count]];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        return cell;
        
    }
    
    
    if (ordertag==1) {
        ProductTotalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductTotalCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        return cell;
        
    }
    
    WalletCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WalletCell" forIndexPath:indexPath];
    cell.backgroundColor =[UIColor clearColor];
    cell.lblDate.text = [NSString stringWithFormat:@"%@",arrayOrder[indexPath.row][@"created_date"]];
    cell.lblordetID.text = [NSString stringWithFormat:@"Order ID: #%@", arrayOrder[indexPath.row][@"order_id"]];
    cell.lblproduct.text = [NSString stringWithFormat:@" %lu",[arrayOrder[indexPath.row][@"productarray"]count]];
    cell.lblStatus.text = [self ststusWithCode:arrayOrder[indexPath.row][@"status"]];
    cell.lblUSD.text = [NSString stringWithFormat:@" %@",arrayOrder[indexPath.row][@"totalAmountRemaining"]];
    
    cell.lblname.hidden = true;
    
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:cell.imgprofile urlString:arrayOrder[indexPath.row][@"productarray"][0][@"image"]  Complete:^(UIImage *image) {
        
    }];
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
//    OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCell" forIndexPath:indexPath];
//    cell.lblDate.text = [NSString stringWithFormat:@":%@",arrayOrder[indexPath.row][@"created_date"]];
//    cell.lblordetID.text = [NSString stringWithFormat:@"Order ID: #%@", arrayOrder[indexPath.row][@"order_id"]];
//    cell.lblproduct.text = [NSString stringWithFormat:@": %lu",[arrayOrder[indexPath.row][@"productarray"]count]];
//    cell.lblStatus.text =  [self ststusWithCode:arrayOrder[indexPath.row][@"status"]];
//    cell.lblUSD.text = [NSString stringWithFormat:@": %@",arrayOrder[indexPath.row][@"amount"]];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cellall forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if ([cellall isKindOfClass:[ProductTotalCell class]]){
        
        
        NSInteger index = indexPath.row - 1;
        
        ProductTotalCell *cell = (ProductTotalCell *)cellall;
        NSString *imagepath = nil;
        if([arrayOrder[indexPath.row][@"images"]count]>0){
            imagepath = arrayOrder[indexPath.row][@"images"][0][@"product_image"];
        }else{
            imagepath = arrayOrder[indexPath.row][@"image"];
        }
      
        for (int i= 0 ; i<[arrayOrder[indexPath.row][@"images"]count]; i++) {
            UIImageView *imgView = [UIImageView new];
            imgView.clipsToBounds = true;
            imgView.frame = CGRectMake(cell.scrollview.frame.size.width*i,0,cell.scrollview.frame.size.width, cell.scrollview.frame.size.height);
            imgView.contentMode = UIViewContentModeScaleAspectFill;
            [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:imgView urlString:arrayOrder[indexPath.row][@"images"][i][@"product_image"] Complete:^(UIImage *image) {}];
            [cell.scrollview addSubview:imgView];
            NSLog(@"%@",imgView);
            
        }
        cell.pageControll.numberOfPages = [arrayOrder[indexPath.row][@"images"]count];
        cell.scrollview.contentSize = CGSizeMake(cell.pageControll.numberOfPages *cell.scrollview.frame.size.width, 0);
        //
        //        [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:cell.imgproduct urlString:imagepath Complete:^(UIImage *image) {}];
        
        cell.lblname.text = arrayOrder[indexPath.row][@"name"];
        cell.lblDescription.text = arrayOrder[indexPath.row][@"customer_name"];
        cell.lblPrice.text =  [NSString stringWithFormat:@"$%@",arrayOrder[indexPath.row][@"price"]];
        
        
        [cell setDidtapBlock:^(UIButton *button) {
            
            if(button.tag == 4){
                
                EditProductVC * obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProductVC"];
                NSUserDefaults *default4=[NSUserDefaults standardUserDefaults];
                [default4 setObject:arrayOrder[indexPath.row] forKey:@"Totalorder"];
                [default4 synchronize];
                obj.result = arrayOrder[indexPath.row];
                [self.navigationController pushViewController:obj animated:true];
                return ;
            }
            else{
            
          
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete product?" preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alertController animated:YES completion:nil];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    dict[@"product_id"] = arrayOrder[indexPath.row][@"product_id"];
                    dict[@"customer_id"] = arrayOrder[indexPath.row][@"customer_id"];
                    
                    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"deleteProduct.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                        
                        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
                        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"totalorders" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                            dictData = [JSON mutableCopy];
                            [self ordermange];
                            
                            
                        }];
                        
                        
                        
                    }];
                    
                }]];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     [self dismissViewControllerAnimated:YES completion:nil];
                }]];

            }
            
            
        }];

    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        return 155*SCREEN_WIDTH/320;
    }
    if (ordertag==1) {
        
        return 290*SCREEN_WIDTH/320;
    }
    return 110*SCREEN_WIDTH/320;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(section==0){
        return nil;
    }
    
    if(ordertag==1){
        UIView *view = [UIView new];
        view.frame = CGRectMake(0, 0, SCREEN_WIDTH,  35*SCREEN_WIDTH/320);
        view.backgroundColor = [UIColor colorWithRed:69/255.f green:110/255.5 blue:10/255.f alpha:1.0];
        UIButton *btnproduct = [[UIButton alloc]initWithFrame:CGRectMake(10, 0, DLAutomatic(100), 35*SCREEN_WIDTH/320)];
        [btnproduct setTitle:@"My Product" forState:UIControlStateNormal];
        [btnproduct setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnproduct.titleLabel.font = [UIFont fontWithName:OpenSenceSB size:DLAutomatic(15)];
        btnproduct.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [view addSubview:btnproduct];
        
      
        
        float width = SCREEN_WIDTH;
        float btnWidth = DLAutomatic(100);
        ;
        UIButton *btnNew = [[UIButton alloc]initWithFrame:CGRectMake(width-btnWidth-10,0, DLAutomatic(100), 35*SCREEN_WIDTH/320)];
        [btnNew setTitle:@"Add New" forState:UIControlStateNormal];
        [btnNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnNew addTarget:self action:@selector(AddnewPress:) forControlEvents:UIControlEventTouchUpInside];
        btnNew.titleLabel.font = [UIFont fontWithName:OpenSenceSB size:DLAutomatic(15)];
        btnNew.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [view addSubview:btnNew];
        
        return view;
        
    }
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0){
        return 0.1;
    }
    if(ordertag==1)
        return 35*SCREEN_WIDTH/320;
    return 0.1;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        return;
    }
    if (ordertag==1){
        return;
    }
    OrderDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
    obj.result = [arrayOrder[indexPath.row]mutableCopy];
    obj.from_sellonVC = 1;
    [self.navigationController pushViewController:obj animated:true];
    
}

- (IBAction)MenuPress:(id)sender {
    [[AppLauncher sharedInstance]openLeftDrawer];
}
- (IBAction)DatePress:(UIButton *)sender {
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
    btntemp = sender;
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"yyyy-MM-dd";
       
    
    NSString *strDate = [dateFormater stringFromDate:date];
   [btntemp setTitle:strDate forState:UIControlStateNormal];
   // templbl.text=btntemp.titleLabel.text;
   // btntemp.titleLabel.text=@"";
    
}

- (IBAction)DonePress:(id)sender {
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0 ];
    CelenderCell *cell =  [self.tableview cellForRowAtIndexPath:indexpath];
    if (![cell.btnFROM.titleLabel.text isEqualToString:@"FROM"] && ![cell.btnTO.titleLabel.text isEqualToString:@"TO"]){
    
    //if([_fromdate.text isEqualToString:@"FROM" ] && ![_todate.text isEqualToString:@"TO"])
      // {
        NSDateFormatter *dateFormater = [NSDateFormatter new];
        dateFormater.dateFormat = @"yyyy-MM-dd";
        

        
        NSString *fromdate=cell.btnFROM.titleLabel.text;
        NSString *todate=cell.btnTO.titleLabel.text;
        NSDate *fromd=[dateFormater dateFromString:fromdate ];
        NSDate *tod=[dateFormater dateFromString:todate ];
        
        if([fromd compare:tod] ==  NSOrderedDescending)
        {
            UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"" message:@"To date must be greater than the From date" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [objalert show];
        }
        else{
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"from_date"] = cell.btnFROM.titleLabel.text;
        dict[@"to_date"] = cell.btnTO.titleLabel.text;
         dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"totalorders" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            
            dictData = JSON.mutableCopy;
            [self ordermange];
        }];
        
        }
    }
}

    - (IBAction)OrderPress:(UIButton *)sender {
        ordertag = sender.tag;

        [self ordermange];
        
    }
    -(IBAction)AddnewPress:(id)sender{
        UIViewController * obj = [self.storyboard instantiateViewControllerWithIdentifier:@"AddProductVC"];
        [self.navigationController pushViewController:obj animated:true];
    }
    
    -(void)ordermange{
        
       
        if (ordertag == 1){
            
            arrayOrder = [dictData[@"ProductList"]mutableCopy];
        }
        else if (ordertag == 2){
            arrayOrder = [dictData[@"orderstList"]mutableCopy];
            
        }else{
            arrayOrder = [dictData[@"UnshippedOrderList"]mutableCopy];
            
        }
        
        
        
        
        
        [self.tableview reloadData];
    }

    
    @end
