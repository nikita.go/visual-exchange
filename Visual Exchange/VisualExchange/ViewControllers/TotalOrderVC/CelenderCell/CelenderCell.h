//
//  CelenderCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 29/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLWalleteDelegate <NSObject>

@optional
- (IBAction)OrderPress:(UIButton *)sender;
- (IBAction)DatePress:(UIButton *)sender;
- (IBAction)DonePress:(id)sender;
@end


@interface CelenderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromlbl;
@property (weak, nonatomic) IBOutlet UILabel *tolbl;

@property (weak, nonatomic) IBOutlet UILabel *lblRevenue;
@property (weak, nonatomic) IBOutlet UILabel *lblPaid;
@property (weak, nonatomic) IBOutlet UILabel *lblBalence;
@property (weak, nonatomic) IBOutlet UILabel *lblStaticRevenue;
@property (weak, nonatomic) IBOutlet UILabel *lblStaticPaid;
@property (weak, nonatomic) IBOutlet UILabel *lblstaticbalance;
@property (weak, nonatomic) IBOutlet UIView *viewTop;

@property (weak, nonatomic) IBOutlet UIButton *btnFROM;
@property (weak, nonatomic) IBOutlet UIButton *btnTO;

@property(weak,nonatomic)id <DLWalleteDelegate>delegate;

@end
