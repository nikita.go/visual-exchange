//
//  WalletCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 10/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblproduct;
@property (weak, nonatomic) IBOutlet UILabel *lblUSD;
@property (weak, nonatomic) IBOutlet UILabel *lblordetID;
@property (weak, nonatomic) IBOutlet UIImageView *imgprofile;
@property (weak, nonatomic) IBOutlet UILabel *lblname;
@end
