//
//  WalletVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//
//
//  WalletVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "WalletVC.h"

@interface WalletVC ()
{
    NSMutableArray *arrayOrder;
    UIButton *btntemp;
    NSInteger ordertag;
    NSMutableDictionary *dictData;
    CelenderCell *cell;

}
@end

@implementation WalletVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableview.backgroundColor = [UIColor clearColor];
    ordertag = 1;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"customer_id"] =  kUserDefults_(ResultUser)[@"customer_id"];
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"wallet" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        dictData = [JSON mutableCopy];
        arrayOrder = [JSON[@"revenueList"]mutableCopy];
        [self.tableview reloadData];
        
        
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mart - UITableview Delegate -


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    if(arrayOrder.count==0)
    {
        return 1;
    }
    else
    {
    return arrayOrder.count+1;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(arrayOrder.count==0)
    {
        
        //CelenderCell *cell;
        if(indexPath.row == 0)
        {

        cell = [tableView dequeueReusableCellWithIdentifier:@"CelenderCell" forIndexPath:indexPath];
        cell.lblRevenue.text = [NSString stringWithFormat:@"%ld",[dictData[@"Revenue"]integerValue]];
        cell.lblPaid.text = [NSString stringWithFormat:@"%ld",[dictData[@"Paid"]integerValue]];
        cell.lblBalence.text = [NSString stringWithFormat:@"%ld",[dictData[@"Balance"]integerValue]];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        }
        return cell;


    }
    else
    {
    if(indexPath.row == 0){
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"CelenderCell" forIndexPath:indexPath];
        cell.lblRevenue.text = [NSString stringWithFormat:@"%ld",[dictData[@"Revenue"]integerValue]];
        cell.lblPaid.text = [NSString stringWithFormat:@"%ld",[dictData[@"Paid"]integerValue]];
        cell.lblBalence.text = [NSString stringWithFormat:@"%ld",[dictData[@"Balance"]integerValue]];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        return cell;
        
    }
        else
        {
    
    NSInteger index = indexPath.row-1;
     WalletCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"WalletCell" forIndexPath:indexPath];
    NSInteger pay_status = [arrayOrder[index][@"pay_status"]integerValue];
   
    cell1.backgroundColor =[UIColor clearColor];
    cell1.lblDate.text = [NSString stringWithFormat:@"%@",arrayOrder[index][@"order_date"]];
    cell1.lblordetID.text = [NSString stringWithFormat:@"Order ID: #%@ - %@", arrayOrder[index][@"order_id"],pay_status == 0 ? @"Balance" :@"Paid"];
        NSMutableArray *ary=[[arrayOrder objectAtIndex:index]objectForKey:@"productarray"];
    cell1.lblproduct.text = [NSString stringWithFormat:@"%lu", (unsigned long)[ary count]];
    cell1.lblStatus.text = [self ststusWithCode:arrayOrder[index][@"status"]];
        
        
        
        
//    NSString *total=[NSString stringWithFormat:@"%@.00",arrayOrder[indexPath.row][@"totalAmount"]];
//    NSString *totalamountdiscount=[NSString stringWithFormat:@"%@.00",arrayOrder[indexPath.row][@"totalAmountDiscount"]];
//    int a=[total intValue]-[totalamountdiscount intValue];
//    NSString *a1=[NSString stringWithFormat:@"%d",a];

   
    cell1.lblUSD.text = [NSString stringWithFormat:@" %@",arrayOrder[index][@"totalAmountAfterComission"]];

    cell1.lblname.text = arrayOrder[index][@"customer_name"];
    
    [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:cell1.imgprofile urlString:arrayOrder[index][@"customer_image"]  Complete:^(UIImage *image) {
        
    }];
    
    cell1.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell1;
    
    }
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row!=0)
    {
        NSInteger index=indexPath.row-1;
        OrderDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
        obj.result = [arrayOrder[index]mutableCopy];
       // obj.from_sellonVC=  1;
        [self.navigationController pushViewController:obj animated:true];
    }
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row ==0){
        
        return  155 *SCREEN_WIDTH/320;
    }
    return 110 *SCREEN_WIDTH/320;
    
}
- (IBAction)MenuPress:(id)sender {
    [[AppLauncher sharedInstance]openLeftDrawer];
}

- (IBAction)OrderPress:(UIButton *)sender {
    NSLog(@"%@",sender);
    ordertag = sender.tag;
    [self ordermanage];
    
}
-(void)ordermanage{
    
    if (ordertag == 1){
        
        arrayOrder = dictData[@"revenueList"];
    }
    else if (ordertag == 3){
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(pay_status == 0)"];
        arrayOrder = [[dictData[@"revenueList"] filteredArrayUsingPredicate:predicate]mutableCopy];;
        
    }
    else if (ordertag == 2){
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(pay_status == 1)"];
        arrayOrder = [[dictData[@"revenueList"] filteredArrayUsingPredicate:predicate]mutableCopy];;
        
    }
//    cell.lblRevenue.text = [NSString stringWithFormat:@"%ld",[dictData[@"Revenue"]integerValue]];
//    cell.lblPaid.text = [NSString stringWithFormat:@"%ld",[dictData[@"Paid"]integerValue]];
//    cell.lblBalence.text = [NSString stringWithFormat:@"%ld",[dictData[@"Balance"]integerValue]];
//    cell.delegate = self;
//cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    [self.tableview reloadData];
    
    
}
- (IBAction)DatePress:(UIButton *)sender {
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
    btntemp = sender;
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"yyyy-MM-dd";
    NSString *strDate = [dateFormater stringFromDate:date];
    
    [btntemp setTitle:strDate forState:UIControlStateNormal];
    
}

- (IBAction)DonePress:(id)sender {
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0 ];
    CelenderCell *cell =  [self.tableview cellForRowAtIndexPath:indexpath];
    
    
    if (![cell.btnFROM.titleLabel.text isEqualToString:@"FROM"] && ![cell.btnTO.titleLabel.text isEqualToString:@"TO"])
    {
        
        if (![cell.btnFROM.titleLabel.text isEqualToString:@"FROM"] && ![cell.btnTO.titleLabel.text isEqualToString:@"TO"]){
            
            //if([_fromdate.text isEqualToString:@"FROM" ] && ![_todate.text isEqualToString:@"TO"])
            // {
            NSDateFormatter *dateFormater = [NSDateFormatter new];
            dateFormater.dateFormat = @"yyyy-MM-dd";
            
            
            
            NSString *fromdate=cell.btnFROM.titleLabel.text;
            NSString *todate=cell.btnTO.titleLabel.text;
            NSDate *fromd=[dateFormater dateFromString:fromdate ];
            NSDate *tod=[dateFormater dateFromString:todate ];
            
            if([fromd compare:tod] ==  NSOrderedDescending)
            {
                UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"" message:@"To date must be greater than the From date" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [objalert show];
            }
            else{

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"from_date"] = cell.btnFROM.titleLabel.text;
        dict[@"to_date"] = cell.btnTO.titleLabel.text;
        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"wallet" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            
            dictData = [JSON mutableCopy];
            [self ordermanage];
            //[self.tableview reloadData];
            
            
        }];
        
            }
        }
    }
}

@end

