//
//  CategorySelectionViewController.m
//  VisualExchange
//
//  Created by mac on 12/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "CategorySelectionViewController.h"
#import "WebServiceViewController.h"
#import "CategoryTableViewCell.h"
#import "AddProductVC.h"


@interface CategorySelectionViewController ()

@end

@implementation CategorySelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrStates=[[NSMutableArray alloc]init];
    selectionary=[[NSMutableArray alloc]init];
    categoryidary=[[NSMutableArray alloc]init];
    _savebtn.layer.cornerRadius =_savebtn.frame.size.height/2;
    _savebtn.layer.masksToBounds=YES;


    [self apicall];
    // Do any additional setup after loading the view.
}
-(void)apicall
{
      [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"categoriesList.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
    {
         arrStates = [JSON[@"country"]mutableCopy];
        
                if(arrStates.count>0)
                {
        
                    for (int i=0; i<arrStates.count; i++) {
        
                      
                          NSMutableDictionary*  tempdict=[arrStates objectAtIndex:i];
                         NSMutableDictionary *   mutable=[tempdict mutableCopy];
                            [mutable setObject:@"1" forKey:@"flag"];
        
                           // [selectionary replaceObjectAtIndex:i withObject:mutable];
        
                        [selectionary addObject:mutable];
                        
                       
                    }
                }
        if(_passcatary.count>0)
        {
            
            for (int i=0; i<_passcatary.count; i++)
            {
              //  NSString *idstr1=[NSString stringWithFormat:@"%@",[_passcatary objectAtIndex:j]objectForKey:@"categorie_id"] ;
                NSString *idstr1=[NSString stringWithFormat:@"%@",[[_passcatary objectAtIndex:i]objectForKey:@"categorie_id"]];
                for(int j=0; j<selectionary.count; j++)
                {
                    NSString *idstr=[[selectionary objectAtIndex:j]objectForKey:@"categorie_id"] ;
                    if([idstr isEqualToString:idstr1])
                    {
                        NSMutableDictionary* tempdict=[selectionary objectAtIndex:j];
                        NSMutableDictionary*   mutable=[tempdict mutableCopy];
                        [mutable setObject:@"0" forKey:@"flag"];
                        
                        [selectionary replaceObjectAtIndex:j withObject:mutable];
                        
                        
                        
                    }
                }
            }
        }
        
  

        for (int i=0;  i<arrStates.count; i++) {
            [categoryidary addObject:[[arrStates objectAtIndex:i]objectForKey:@"categorie_id"]];
        }
        [_tblview reloadData];
    }];
//    NSMutableDictionary *dictdata=[[NSMutableDictionary alloc]init];
//    
//    [dictdata setObject:@"ed6ad1bc7ffd895b2c47caa290c9809f" forKey:@"api_key"];
//    [dictdata setObject:[testclass crypt:_makeid] forKey:@"make"];
//    
//    NSString *follower_URL= [baseUrl3 stringByAppendingString:@"getmodelcat"];
//    [WebServiceViewController wsVC].strURL=follower_URL;
//    [WebServiceViewController wsVC].strCallHttpMethod=@"POST";
//    NSMutableDictionary*logindict =[[NSMutableDictionary alloc]init];
//    logindict =[[WebServiceViewController wsVC] sendRequestWithParameter:dictdata];
//    myary=[logindict objectForKey:@"data"];
//    if(myary.count==0)
//    {
//        _modelheaderlbl.hidden=NO;
//        _objtableview.hidden=YES;
//        _okbtn.hidden=YES;
//        
//        _modelheaderlbl.text=@"No Model's found";
//    }
//    else{
//        
//        _objtableview.hidden=NO;
//        _modelheaderlbl.hidden= YES;
//        
//        
//        NSMutableDictionary *mutable=[[NSMutableDictionary alloc]init];
//        NSMutableDictionary *tempdict=[[NSMutableDictionary alloc]init];
//        
//        
//
//        NSLog(@"%@",newary);
//        
//        
//        for (int i=0; i<newary.count; i++) {
//            [modelary addObject:[newary objectAtIndex:i]];
//            
//            
//        }
//        NSLog(@"%@",modelary);
//        
//        
//        
//    }
//    
//}

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectionary.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if([[[selectionary objectAtIndex:indexPath.row]objectForKey:@"flag"]isEqualToString:@"0"])
    {
        [cell.checkimg setImage:[UIImage imageNamed:@"checked.png"]];
        cell.chekbtn.tag=1;
        cell.checkimg.tag=1;
        
        
        
        
        
        
        
    }
    else{
        
        
        [cell.checkimg setImage:[UIImage imageNamed:@"unchecked.png"]];
        cell.chekbtn.tag=2;
        cell.checkimg.tag=2;
        
        
    }
    
    [cell.chekbtn addTarget:self action:@selector(checkbtnpress:) forControlEvents:UIControlEventTouchUpInside];
    cell.catlbl.text=[[arrStates objectAtIndex:indexPath.row]objectForKey:@"name"];
    
    return cell;
}
-(void)checkbtnpress:(UIButton *)sender
{
 
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tblview];
    NSIndexPath *tappedIP = [self.tblview indexPathForRowAtPoint:buttonPosition];
    
    if(sender.tag==2)
    {
        CategoryTableViewCell *cell = [self.tblview cellForRowAtIndexPath:tappedIP];
        [cell.checkimg setImage:[UIImage imageNamed:@"checked.png"]];
        
        for (int i=0; i<selectionary.count; i++) {
            if([[[selectionary objectAtIndex:tappedIP.row]objectForKey:@"flag"]isEqualToString:@"1"])
            {
                
                NSMutableDictionary *tempdict=[selectionary objectAtIndex:tappedIP.row];
                [tempdict setObject:@"0" forKey:@"flag"];
                [selectionary replaceObjectAtIndex:tappedIP.row withObject:tempdict];
            }
        }
        //[selectionary addObject:[categoryidary objectAtIndex:tappedIP.row]];
        [sender setTag:1];
    }
    else
    {
        CategoryTableViewCell *cell = [self.tblview cellForRowAtIndexPath:tappedIP];
        for (int i=0; i<selectionary.count; i++) {
            if([[[selectionary objectAtIndex:tappedIP.row]objectForKey:@"flag"]isEqualToString:@"0"])
            {
                
                NSMutableDictionary *tempdict=[selectionary objectAtIndex:tappedIP.row];
                [tempdict setObject:@"1" forKey:@"flag"];
                [selectionary replaceObjectAtIndex:tappedIP.row withObject:tempdict];
            }
            
            
            
        }

        //[selectionary removeObjectAtIndex:[categoryidary objectAtIndex:tappedIP.row]];
        [cell.checkimg setImage:[UIImage imageNamed:@"unchecked.png"]];
        [sender setTag:2];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saevbtnpress:(id)sender
{
    NSMutableArray *passcatidary=[[NSMutableArray alloc]init];
    for (int i=0; i<selectionary.count; i++) {
        if([[[selectionary objectAtIndex:i]objectForKey:@"flag"]isEqualToString:@"0"])
        {
           NSMutableDictionary *tempdict=[selectionary objectAtIndex:i];
            [passcatidary addObject:tempdict];
        }
    }
//    AddProductVC * obj = [ self.storyboard instantiateViewControllerWithIdentifier:@"AddProductVC"];
//    obj.passcateary=passcatidary;
//    [self.navigationController pushViewController:obj animated:true];
      for (UIViewController* viewController in self.navigationController.viewControllers) {
    
    
            if ([viewController isKindOfClass:[AddProductVC class]] ) {
    
                AddProductVC *groupViewController = (AddProductVC *)viewController;
                groupViewController.passcateary=passcatidary;

                [self.navigationController popToViewController:groupViewController animated:YES];
  //  [self.navigationController popViewControllerAnimated:NO];
    }
     }
}

- (IBAction)backbtnpress:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
