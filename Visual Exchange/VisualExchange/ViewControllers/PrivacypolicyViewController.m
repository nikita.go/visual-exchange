//
//  PrivacypolicyViewController.m
//  VisualExchange
//
//  Created by mac on 3/20/17.
//  Copyright © 2017 Company. All rights reserved.
//

#import "PrivacypolicyViewController.h"

@interface PrivacypolicyViewController ()

@end

@implementation PrivacypolicyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:_objwebview animated:YES].label.text=@"Loading...";
    [self performSelector:@selector(getprivacypolicy) withObject:self afterDelay:1.0 ];
    
   // [self getprivacypolicy];
    // Do any additional setup after loading the view.
}

#pragma  mark custom method
-(void)getprivacypolicy
{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES].labelText=@"Loading...";
    if([_privacystr isEqualToString:@"privacy"])
    {
        NSString *urlAddress = @"http://thevisualexchange.com/visual/api/privacy.php";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [MBProgressHUD hideAllHUDsForView:_objwebview animated:YES];
        [_objwebview loadRequest:requestObj];
    }
    if([_privacystr isEqualToString:@"return"])
    {
        NSString *urlAddress = @"http://thevisualexchange.com/visual/api/return-policy.php";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [MBProgressHUD hideAllHUDsForView:_objwebview animated:YES];
        [_objwebview loadRequest:requestObj];
    }
    if([_privacystr isEqualToString:@"shipment"])
    {
        NSString *urlAddress = @"http://thevisualexchange.com/visual/api/shipping-policy.php";
        NSURL *url = [NSURL URLWithString:urlAddress];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
       [MBProgressHUD hideAllHUDsForView:_objwebview animated:YES];
        [_objwebview loadRequest:requestObj];
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark Button action events
- (IBAction)tapOnMenubtn:(id)sender
{
     [[AppLauncher sharedInstance]openLeftDrawer];
}
- (IBAction)tapOnSearchbtn:(id)sender
{
    [[AppLauncher sharedInstance]Searchpress];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
