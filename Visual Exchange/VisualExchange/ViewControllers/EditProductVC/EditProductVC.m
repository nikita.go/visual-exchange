//
//  EditProductVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 18/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "EditProductVC.h"
#import "EditCategoryViewController.h"
#import "TotalOrderVC.h"

@interface EditProductVC ()
{
    NSMutableArray *arrayVideo;
    NSMutableArray *arrStates;
    
}

@end

@implementation EditProductVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _datepickerview.hidden=YES;

    if(_result.count>0)
    {
        
    }
    else
    {
        NSUserDefaults *default4=[NSUserDefaults standardUserDefaults];
      _result=  [default4 objectForKey:@"Totalorder"];
        
    }
        
    arrayVideo = [NSMutableArray new];
    self.txtVideoLink.text =[NSString stringWithFormat:@"%@", self.result[@"video_link"]];
      self.txtDescription.text = [NSString stringWithFormat:@"%@",self.result[@"details"]];
    self.txtPrice.text = [NSString stringWithFormat:@"%@",self.result[@"price"]];
    self.txtProductname.text = [NSString stringWithFormat:@"%@",self.result[@"name"]];
    self.txtWight.text = [NSString stringWithFormat:@"%@",self.result[@"weight"]];
     self.txtSKU.text = [NSString stringWithFormat:@"%@",self.result[@"sku"]];
     self.txtLead_time.text = [NSString stringWithFormat:@"%@",self.result[@"lead_time"]];
     self.txtStocklimit.text = [NSString stringWithFormat:@"%@",self.result[@"stock_limit"]];
      self.txtlength.text = [NSString stringWithFormat:@"%@",self.result[@"length"]];
      self.txtheight.text = [NSString stringWithFormat:@"%@",self.result[@"height"]];
      self.txtwidth.text = [NSString stringWithFormat:@"%@",self.result[@"width"]];
    for(int i = 0; i<[self.result[@"images"]count]; i++){
        
        [self.scollMain viewWithTag:2525].hidden = false;
        
        UIImageView *btnImage;
        UIButton *deleteButton;
        
        
        CGFloat imageHeight = (SCREEN_WIDTH/6);
        
        
        CGRect rect;
        if(i<3){
            CGRect rectScroll = self.Scrollview.frame;
            rectScroll.origin.x = 0;
            rectScroll.size.height = imageHeight+20;
            rectScroll.size.width = SCREEN_WIDTH;
            self.Scrollview.frame = rectScroll;
            rect = CGRectMake(i*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
            
        }else{
            
            NSInteger arraycount = i-3;
            rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
            CGRect rectScroll = self.Scrollview.frame;
            rectScroll.origin.x = 0;
            rectScroll.size.height = imageHeight+imageHeight+55;
            rectScroll.size.width = SCREEN_WIDTH;
            self.Scrollview.frame = rectScroll;
        }
        
        btnImage  = [UIImageView new];
        btnImage.frame = rect;
        [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:btnImage urlString:self.result[@"images"][i][@"product_image"] Complete:^(UIImage *image) {
            
        }];
        // btnImage.image = img; // setBackgroundImage:img forState:UIControlStateNormal];
        [btnImage setTag:arrayVideo.count];
        btnImage.layer.cornerRadius = 4;
        btnImage.layer.masksToBounds = true;
        //[self.Scrollview addSubview:btnImage];
        
        NSDictionary *dict  = @{@"isNew":@"0",@"image":btnImage,@"imageUrl":self.result[@"images"][i][@"product_image"],@"product_image_id":self.result[@"images"][i][@"product_image_id"]};
        [arrayVideo addObject:dict];
        
        
        
        
        deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
        deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
        deleteButton.clipsToBounds = false;
        deleteButton.tag = btnImage.tag;
        [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        deleteButton.userInteractionEnabled = true;
        
        btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
        UIView *view = [[UIView alloc]initWithFrame:rect];
        view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
        [view addSubview:btnImage];
        view.tag = btnImage.tag;
        view.clipsToBounds = false;
        view.backgroundColor = [UIColor clearColor];
        [view addSubview:deleteButton];
        [self.Scrollview addSubview:view];
        [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
        // [btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if( ![self.result[@"video"] isEqualToString:@""] && self.result[@"video"] != nil){
        [self addvideo:self.result[@"video"]];
    }
    
    
    
    CGRect rectmain = self.viewAlltextfield.frame;
    rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
    self.viewAlltextfield.frame = rectmain;
    
    self.txtCategory.text =[NSString stringWithFormat:@"%@", self.result[@"categoryname"]];
    self.txtDescription.text = [NSString stringWithFormat:@"%@",self.result[@"details"]];
    self.txtPrice.text = [NSString stringWithFormat:@"%@",self.result[@"price"]];
    self.txtProductname.text = [NSString stringWithFormat:@"%@",self.result[@"name"]];
    self.txtWight.text = [NSString stringWithFormat:@"%@",self.result[@"weight"]];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtProductname)
    {
        [_txtVideoLink becomeFirstResponder];
    } else if (textField == _txtVideoLink)
    {
        [_txtVideoLink resignFirstResponder];
    }
    else if (textField == _txtDescription)
    {
        [_txtWight becomeFirstResponder];
    }
    else if (textField == _txtWight)
    {
        [_txtPrice becomeFirstResponder];
    }
    else if (textField == _txtPrice)
    {
        [_txtSKU becomeFirstResponder];
    }
    else if (textField == _txtSKU)
    {
        [_txtStocklimit becomeFirstResponder];
    }
    else if (textField == _txtStocklimit)
    {
        [_txtStocklimit resignFirstResponder];
    }
    else if (textField == _txtlength)
    {
        [_txtheight resignFirstResponder];
    }
    else if (textField == _txtheight)
    {
        [_txtwidth resignFirstResponder];
    }
    else if (textField == _txtwidth)
    {
        [_txtwidth resignFirstResponder];
    }
    else if (textField == _txtLead_time)
    {
        [_txtlength becomeFirstResponder];
    }
    
    //    else
    //    {
    //        [textField resignFirstResponder];
    //    }
    return YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
   
    [self fetchStates];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
  //  _scollMain.contentSize=CGSizeMake(0, 400);
    
    [self.scollMain AutomaticScrollview];
}

- (void)fetchStates
{
//    [self.viewAlltextfield endEditing:YES];
//    
   [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"categoriesList.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
       
       
       
        arrStates = [JSON[@"country"]mutableCopy];
       NSMutableArray *myary=[[NSMutableArray alloc]init];

       
       if(_passcatidary.count>0)
       {
           for (int i=0; i<_passcatidary.count; i++)
           {
               NSString *idstr1=[NSString stringWithFormat:@"%@",[_passcatidary objectAtIndex:i]];
               for(int j=0; j<arrStates.count; j++)
               {
                   NSString *idstr=[[arrStates objectAtIndex:j]objectForKey:@"categorie_id"] ;
                   if([idstr isEqualToString:idstr1])
                   {
                       NSMutableDictionary* tempdict=[arrStates objectAtIndex:j];
                       NSMutableDictionary*   mutable=[tempdict mutableCopy];
                       
                       
                       [myary addObject:mutable];
                       
                       
                       
                   }
               }
           }
           //   }
           NSMutableArray *catnameary=[[NSMutableArray alloc]init];
           for (int i=0; i<myary.count; i++)
           {
               [catnameary addObject:[[myary objectAtIndex:i]objectForKey:@"name"]];
           }
           NSString *str =[NSString stringWithFormat:@"%@",catnameary];
           
           NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
           NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
           NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
           
           
           NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
           NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
           self.txtCategory.text =finalstr;
           
           

       }
       else{
             NSMutableArray *productary=self.result[@"category_id"];
       if(productary.count>0)
       {
           
           for (int i=0; i<productary.count; i++)
           {
               NSString *idstr1=[NSString stringWithFormat:@"%@",[productary objectAtIndex:i]];
               for(int j=0; j<arrStates.count; j++)
               {
                   NSString *idstr=[[arrStates objectAtIndex:j]objectForKey:@"categorie_id"] ;
                   if([idstr isEqualToString:idstr1])
                   {
                       NSMutableDictionary* tempdict=[arrStates objectAtIndex:j];
                       NSMutableDictionary*   mutable=[tempdict mutableCopy];
                       
                       
                       [myary addObject:mutable];
                       
                       
                       
                   }
               }
           }
       }
       NSMutableArray *catnameary=[[NSMutableArray alloc]init];
       for (int i=0; i<myary.count; i++)
       {
           [catnameary addObject:[[myary objectAtIndex:i]objectForKey:@"name"]];
       }
       NSString *str =[NSString stringWithFormat:@"%@",catnameary];
       
       NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
       NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
       NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
       
       
       NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
       NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
       self.txtCategory.text =finalstr;

       }
//        PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
//        pickerViewController.pickerType = CustomPickerType;
//        pickerViewController.dataSourceForCustomPickerType = [arrStates valueForKey:@"name"];
//        [pickerViewController setInitialItemAtIndex:0];
//        pickerViewController.delegate = self;
//        [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
//        [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
//        
//        
   }];
    
}
- (void)didSelectItemAtIndex:(NSUInteger)index{
    self.txtCategory.text = arrStates[index][@"name"];
}
#pragma mark - Get uer object -
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
////    if(textField == self.txtCategory){
////        [textField resignFirstResponder];
////        [self fetchStates];
////        
////    }
////    return true;
//    
//}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(actionSheet.tag==100){
        
        if (buttonIndex == 0) { // Camera
            [self openVideoPickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        } else  if (buttonIndex == 1) { // Photo Gallery
            [self openVideoPickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        return;
        
    }
    if (buttonIndex == 0) { // Camera
        
        YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
        camController.delegate=self;
        [self presentViewController:camController animated:YES completion:^{
            // completion code
        }];

    } else  if (buttonIndex == 1) { // Photo Gallery
        
        
        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
        
        photoPicker.cropBlock = ^(UIImage *image) {
            //do something
            [self SetingsImages:image];
        };
        
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
        [navCon setNavigationBarHidden:YES];
        
        [self presentViewController:navCon animated:YES completion:NULL];
        
    }
}
- (void)didFinishPickingImage:(UIImage *)image{
    [self  SetingsImages:image];
}

- (void)openImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = sourceType;
        ipc.allowsEditing = NO;
        [self presentViewController:ipc animated:YES completion:nil];
    }];
}
- (void)openVideoPickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = sourceType;
        ipc.allowsEditing = NO;
        ipc.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
        [ipc setVideoMaximumDuration:10];
        ipc.allowsEditing = YES;
        [self presentViewController:ipc animated:YES completion:nil];
    }];
}
- (IBAction)PhotoPress:(id)sender {
    
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Gallery",nil];
    [actionSheet showInView:self.view];
    
    
    
}
- (IBAction)VideoPress:(id)sender {
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose Video from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;
    
    
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info


{
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        
        [self.scollMain viewWithTag:2525].hidden = false;
        
        UIImageView *btnImage;
        UIButton *deleteButton;
        
        NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        if ( [mediaType isEqualToString:@"public.movie" ])
        {
            
            if ([self isVideoAvalable]){
                [self showErrorWithMessage:dlkOneVideo];
                return;
            }
            NSLog(@"Picked a movie at URL %@",  [info objectForKey:UIImagePickerControllerMediaURL]);
            NSURL *url =  [info objectForKey:UIImagePickerControllerMediaURL];
            NSLog(@"> %@", [url absoluteString]);
            
            CGFloat imageHeight = SCREEN_WIDTH/6;
            
            
            
            CGRect rect =  CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 0, imageHeight,imageHeight);
            if(arrayVideo.count<3){
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+20;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
                rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
                
            }else{
                
                NSInteger arraycount = arrayVideo.count-3;
                rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+imageHeight+55;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
            }
            
            btnImage  = [UIImageView new];
            btnImage.frame = rect;
            btnImage.image = [UIImage imageNamed:@"play"];
            [btnImage setTag:arrayVideo.count];
            btnImage.layer.cornerRadius = 4;
            btnImage.layer.masksToBounds = true;
            
            
            deleteButton  =    [[UIButton alloc]initWithFrame:CGRectMake(rect.size.width-20, -20, 40, 40)];
             deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
            deleteButton.clipsToBounds = false;
            deleteButton.tag = btnImage.tag;
            [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
            deleteButton.userInteractionEnabled = true;
            btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
            UIView *view = [[UIView alloc]initWithFrame:rect];
            view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);

            [view addSubview:btnImage];
            view.clipsToBounds = false;
            view.tag = btnImage.tag;
            view.backgroundColor = [UIColor clearColor];
            [view addSubview:deleteButton];
            [self.Scrollview addSubview:view];
            NSDictionary *dict  = @{@"isNew":@"1",@"image":url};
            [arrayVideo addObject:dict];
            [deleteButton addTarget:self action:@selector(DeleteVideo:) forControlEvents:UIControlEventTouchUpInside];
            //[btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
            
        }else{
            
            NSInteger countt = 5;
            if ([self isVideoAvalable]){
                countt = 6;
            }
            if (countt <= arrayVideo.count) {
                [self showErrorWithMessage:dlkImage5];
                return;
            }
            UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
            CGFloat imageHeight = (SCREEN_WIDTH/6);
            
            
            CGRect rect;
            if(arrayVideo.count<3){
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+20;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
                rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
                
            }else{
                
                NSInteger arraycount = arrayVideo.count-3;
                rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
                CGRect rectScroll = self.Scrollview.frame;
                rectScroll.origin.x = 0;
                rectScroll.size.height = imageHeight+imageHeight+55;
                rectScroll.size.width = SCREEN_WIDTH;
                self.Scrollview.frame = rectScroll;
            }
            
            btnImage  = [UIImageView new];
            btnImage.frame = rect;
            btnImage.image = img; // setBackgroundImage:img forState:UIControlStateNormal];
            [btnImage setTag:arrayVideo.count];
            btnImage.layer.cornerRadius = 4;
            btnImage.layer.masksToBounds = true;
            //[self.Scrollview addSubview:btnImage];
            NSDictionary *dict  = @{@"isNew":@"1",@"image":btnImage};
            [arrayVideo addObject:dict];
            
            
            
            
            UIButton *   deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
            deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
            deleteButton.clipsToBounds = false;
            deleteButton.tag = btnImage.tag;
            [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
            deleteButton.userInteractionEnabled = true;
            
            btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
            UIView *view = [[UIView alloc]initWithFrame:rect];
            view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
            [view addSubview:btnImage];
            view.tag = btnImage.tag;
            view.clipsToBounds = false;
            view.backgroundColor = [UIColor clearColor];
            [view addSubview:deleteButton];
            [self.Scrollview addSubview:view];
            [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
            // [btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        CGRect rectmain = self.viewAlltextfield.frame;
        rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
        self.viewAlltextfield.frame = rectmain;
        
    }];
}
-(void)SetingsImages:(UIImage *)img{
    
    UIImageView *btnImage;
    NSInteger countt = 5;
    if ([self isVideoAvalable]){
        countt = 6;
    }
    if (countt <= arrayVideo.count) {
        [self showErrorWithMessage:dlkImage5];
        return;
    }
  
    CGFloat imageHeight = (SCREEN_WIDTH/6);
    
    
    CGRect rect;
    if(arrayVideo.count<3){
        CGRect rectScroll = self.Scrollview.frame;
        rectScroll.origin.x = 0;
        rectScroll.size.height = imageHeight+20;
        rectScroll.size.width = SCREEN_WIDTH;
        self.Scrollview.frame = rectScroll;
        rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
        
    }else{
        
        NSInteger arraycount = arrayVideo.count-3;
        rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
        CGRect rectScroll = self.Scrollview.frame;
        rectScroll.origin.x = 0;
        rectScroll.size.height = imageHeight+imageHeight+55;
        rectScroll.size.width = SCREEN_WIDTH;
        self.Scrollview.frame = rectScroll;
    }
    
    btnImage  = [UIImageView new];
    btnImage.frame = rect;
    btnImage.image = img; // setBackgroundImage:img forState:UIControlStateNormal];
    [btnImage setTag:arrayVideo.count];
    btnImage.layer.cornerRadius = 4;
    btnImage.layer.masksToBounds = true;
    //[self.Scrollview addSubview:btnImage];
    NSDictionary *dict  = @{@"isNew":@"1",@"image":btnImage};
    [arrayVideo addObject:dict];
    
    
    
    
    UIButton *   deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
    deleteButton.clipsToBounds = false;
    deleteButton.tag = btnImage.tag;
    [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    deleteButton.userInteractionEnabled = true;
    
    btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
    UIView *view = [[UIView alloc]initWithFrame:rect];
    view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
    [view addSubview:btnImage];
    view.tag = btnImage.tag;
    view.clipsToBounds = false;
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:deleteButton];
    [self.Scrollview addSubview:view];
    [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
    // [btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];



CGRect rectmain = self.viewAlltextfield.frame;
rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
self.viewAlltextfield.frame = rectmain;

}

-(void)addvideo:(NSURL *)url{
    
    UIImageView *btnImage;
    UIButton *deleteButton;
    
    
    CGFloat imageHeight = SCREEN_WIDTH/6;
    
    
    
    CGRect rect =  CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 0, imageHeight,imageHeight);
    if(arrayVideo.count<3){
        CGRect rectScroll = self.Scrollview.frame;
        rectScroll.origin.x = 0;
        rectScroll.size.height = imageHeight+20;
        rectScroll.size.width = SCREEN_WIDTH;
        self.Scrollview.frame = rectScroll;
        rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
        
    }else{
        
        NSInteger arraycount = arrayVideo.count-3;
        rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
        CGRect rectScroll = self.Scrollview.frame;
        rectScroll.origin.x = 0;
        rectScroll.size.height = imageHeight+imageHeight+55;
        rectScroll.size.width = SCREEN_WIDTH;
        self.Scrollview.frame = rectScroll;
    }
    
    btnImage  = [UIImageView new];
    btnImage.frame = rect;
    btnImage.image = [UIImage imageNamed:@"play"];
    [btnImage setTag:arrayVideo.count];
    btnImage.layer.cornerRadius = 4;
    btnImage.layer.masksToBounds = true;
    
    deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
    deleteButton.clipsToBounds = false;
    deleteButton.tag = btnImage.tag;
    [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    deleteButton.userInteractionEnabled = true;
    btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
    UIView *view = [[UIView alloc]initWithFrame:rect];
    view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
    view.clipsToBounds = false;
    view.tag = btnImage.tag;
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:btnImage];
    [view addSubview:deleteButton];
    [self.Scrollview addSubview:view];
    NSDictionary *dict  = @{@"isNew":@"0",@"imageUrl":url};
    [arrayVideo addObject:dict];
    [deleteButton addTarget:self action:@selector(DeleteVideo:) forControlEvents:UIControlEventTouchUpInside];
    //[btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect rectmain = self.viewAlltextfield.frame;
    rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
    self.viewAlltextfield.frame = rectmain;
    
    
}
- (IBAction)UpdatePress:(id)sender {
    
    [self.view endEditing: true];
    
    if (self.txtProductname.text.isEmptyString) {
        [self showErrorWithMessage:dlkproductname];
    }
//    else if (!self.txtVideoLink.text.isValidateUrl) {
//        [self showErrorWithMessage:dlkVideoSelect];
//    }
    
    else if (self.txtCategory.text.isEmptyString) {
        [self showErrorWithMessage:dlkProductCategory];
    }
    else if (self.txtDescription.text.isEmptyString) {
        [self showErrorWithMessage:dlkProductDescription];
    }
    else if ([self.txtWight.text floatValue]==0) {
        [self showErrorWithMessage:dlkProductWaight];
    }
    else if ([self.txtPrice.text floatValue]==0) {
        [self showErrorWithMessage:dlkProductPrice];
    }else{
        
        NSString * stateid = self.result[@"category_id"];
        for(int i=0; i<arrStates.count; i++){
            if([self.txtCategory.text isEqualToString:arrStates[i][@"name"]]){
                stateid = arrStates[i][@"categorie_id"];
                break;break;
            }
        }
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"customer_id"] = [NSString stringWithFormat:@"%@", kUserDefults_(ResultUser)[@"customer_id"]];
        dict[@"product_id"] = [NSString stringWithFormat:@"%@",self.result[@"product_id"]];
        dict[@"price"] = self.txtPrice.text;
        if(_passcatidary.count>0)
        {
            NSString *str =[NSString stringWithFormat:@"%@",_passcatidary];
            
            NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
            
            
            NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
            NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
             dict[@"category_id"] = finalstr;
        }
        else
        {
            NSMutableArray *stateid=self.result[@"category_id"];
             NSString *str =[NSString stringWithFormat:@"%@",stateid];
            NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
            
            
            NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
            NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
            dict[@"category_id"] = finalstr;

            
            
        }
        dict[@"details"] = self.txtDescription.text;
        dict[@"product_name"] = self.txtProductname.text;
        dict[@"weight"] = self.txtWight.text;
        dict[@"stock_limit"] = self.txtStocklimit.text;

        dict[@"sku"] = self.txtSKU.text;
        dict[@"width"] = self.txtwidth.text;
        dict[@"height"] = self.txtheight.text;
        dict[@"length"] = self.txtlength.text;



        dict[@"lead_time"] = self.txtLead_time.text;

        dict[@"image"] = [self uploadMultiplePics];
         dict[@"video_link"] = self.txtVideoLink.text;
        [[DLHelper shareinstance]DLSERVICE_IMAGE:self.view Indicater:true url:@"editproduct.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            
            //[self.navigationController popViewControllerAnimated:true];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TotalOrderVC *dealVC1 = (TotalOrderVC *)[storyboard instantiateViewControllerWithIdentifier:@"TotalOrderVC"];
            [self.navigationController pushViewController:dealVC1 animated:YES];
            
        }];
        
        
        
    }
}
- (IBAction)BackPress:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TotalOrderVC *dealVC1 = (TotalOrderVC *)[storyboard instantiateViewControllerWithIdentifier:@"TotalOrderVC"];
    [self.navigationController pushViewController:dealVC1 animated:YES];
   // [self.navigationController popViewControllerAnimated:true];
}

-(IBAction)DeleteVideo:(UIButton *)sender{
    
    [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
        
        if(DLindex==1){
            
            
            if(arrayVideo[sender.superview.tag][@"imageUrl"]){
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                dict[@"product_id"] = self.result[@"product_id"];
                dict[@"image_id"] = @"";
                   dict[@"customer_id"] = [NSString stringWithFormat:@"%@",kUserDefults_(ResultUser)[@"customer_id"]];
                dict[@"type"] = @"video";
                
                
                [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"image_delete" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                    
                    [self deleteImage:sender];
                    
                }];
                
                
            }else{
                [self deleteImage:sender];
            }
            
            
        }
    } title:@"Delete" message:@"are you sure?" canceltile:@"Cancel" othertitle:@"Delete", nil];
    
    
    
    
    
}
-(IBAction)DeletePress:(UIButton *)sender{
    
    if (arrayVideo.count==1){
        [self showErrorWithMessage:dlkMinimumImage];
        return;
    }
    else if(arrayVideo.count==2 && [self isVideoAvalable]){
        
        [self showErrorWithMessage:dlkMinimumImage];
        return;
    }
    else{
        
        [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
            
            if(DLindex==1){
                
                
                if(arrayVideo[sender.superview.tag][@"imageUrl"]){
                    
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    dict[@"product_id"] = self.result[@"product_id"];
                    dict[@"image_id"] = arrayVideo[sender.superview.tag][@"product_image_id"];
                    dict[@"type"] = @"image";
                    dict[@"customer_id"] = [NSString stringWithFormat:@"%@",kUserDefults_(ResultUser)[@"customer_id"]];

                    
                    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"image_delete" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
                        
                        [self deleteImage:sender];
                        
                    }];
                    
                    
                }else{
                    [self deleteImage:sender];
                }
                
                
            }
        } title:@"Delete" message:@"are you sure  you want to delete?" canceltile:@"Cancel" othertitle:@"Delete", nil];
        
        
        
    }
    
}
-(void)deleteImage:(UIButton *)sender{
    
    
    [sender.superview removeFromSuperview];
    [arrayVideo removeObjectAtIndex:sender.superview.tag];
    if(arrayVideo.count==0){
        [self.scollMain viewWithTag:2525].hidden = true;
    }
    CGRect rect;
    for (UIView *v in self.Scrollview.subviews) {
        
        
        if(v.tag>sender.superview.tag){
            [v setTag:v.tag-1];
        }
        NSLog(@"%ld",(long)v.tag);
        
        
        CGFloat imageHeight = (SCREEN_WIDTH/6);
        
        
        
        if(v.tag<3){
            CGRect rectScroll = self.Scrollview.frame;
            rectScroll.origin.x = 0;
            rectScroll.size.height = imageHeight+20;
            rectScroll.size.width = SCREEN_WIDTH;
            self.Scrollview.frame = rectScroll;
            rect = CGRectMake(v.tag*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
            
            
        }else{
            
            NSInteger arraycount = v.tag-3;
            rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
            CGRect rectScroll = self.Scrollview.frame;
            rectScroll.origin.x = 0;
            rectScroll.size.height = imageHeight+imageHeight+55;
            rectScroll.size.width = SCREEN_WIDTH;
            self.Scrollview.frame = rectScroll;
        }
        
        
        v.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
        
        
    }
    
    CGRect rectmain = self.viewAlltextfield.frame;
    rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
    self.viewAlltextfield.frame = rectmain;
    
    
    
    
    
    
    
    
}
-(NSMutableData *)uploadMultiplePics
{
    NSString *string ;
    NSData *imageData;
    NSString*urlString=[NSString stringWithFormat:@"http://******"];
    // urlString=[urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSMutableData *body;
    body = [NSMutableData data];
    for(int j=0;j < arrayVideo.count;j++) // scrollViewImageArray is images count
    {
        
        if ([arrayVideo[j][@"image"] isKindOfClass:[UIImageView class]] && [arrayVideo[j][@"isNew"]isEqualToString:@"1"]){
            double my_time = [[NSDate date] timeIntervalSince1970];
            
            NSString *imageName = [NSString stringWithFormat:@"%d%d",j,(int)(my_time)];
            NSString *imagetag=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image[]\"; filename=\""];
            string = [NSString stringWithFormat:@"%@%@%@", imagetag, imageName, @".jpg\"\r\n\""];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:string] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            UIImage *image=[[UIImage alloc]init];
            UIImageView *imgview = [arrayVideo objectAtIndex:j][@"image"];
            image=imgview.image;
            imageData = UIImageJPEGRepresentation(image, .30);
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }else if ([arrayVideo[j][@"image"] isKindOfClass:[NSURL class]]&& [arrayVideo[j][@"isNew"]isEqualToString:@"1"]){
            
            NSData *data = [NSData dataWithContentsOfURL:arrayVideo[j][@"image"]];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"video\"; filename=\"a.mov\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: video/mov\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:data];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            
        }
    }
    [request setHTTPBody:body];
    
    return body;
    
}
//- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//    
//    
//    
//    [self.scollMain viewWithTag:2525].hidden = false;
//    
//    UIImageView *btnImage;
//    
//    // NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
//    for (NSDictionary *dict in info) {
//        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
//            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
//                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
//                
//                
//                CGFloat imageHeight = (SCREEN_WIDTH/6);
//                CGRect rect;
//                if(arrayVideo.count<3){
//                    CGRect rectScroll = self.Scrollview.frame;
//                    rectScroll.origin.x = 0;
//                    rectScroll.size.height = imageHeight+20;
//                    rectScroll.size.width = SCREEN_WIDTH;
//                    self.Scrollview.frame = rectScroll;
//                    rect = CGRectMake(arrayVideo.count*(imageHeight+imageHeight/2)+imageHeight, 20, imageHeight,imageHeight);
//                    
//                }else{
//                    
//                    NSInteger arraycount = arrayVideo.count-3;
//                    rect = CGRectMake(arraycount*(imageHeight+imageHeight/2)+imageHeight,imageHeight+55, imageHeight,imageHeight);
//                    CGRect rectScroll = self.Scrollview.frame;
//                    rectScroll.origin.x = 0;
//                    rectScroll.size.height = imageHeight+imageHeight+55;
//                    rectScroll.size.width = SCREEN_WIDTH;
//                    self.Scrollview.frame = rectScroll;
//                }
//                
//                btnImage  = [UIImageView new];
//                btnImage.frame = rect;
//                btnImage.image = image;
//                [btnImage setTag:arrayVideo.count];
//                btnImage.layer.cornerRadius = 4;
//                btnImage.layer.masksToBounds = true;
//                //[self.Scrollview addSubview:btnImage];
//                
//                
//                UIButton *   deleteButton  =    [UIButton buttonWithType:UIButtonTypeCustom];
//                deleteButton.frame = CGRectMake(rect.size.width-15, 0, 30, 30);
//                deleteButton.clipsToBounds = false;
//                deleteButton.tag = btnImage.tag;
//                [deleteButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
//                deleteButton.userInteractionEnabled = true;
//                
//                btnImage.frame = CGRectMake(0, 15, imageHeight, imageHeight);
//                UIView *view = [[UIView alloc]initWithFrame:rect];
//                view.frame = CGRectMake(rect.origin.x-15, rect.origin.y-15, rect.size.width+15, rect.size.height+15);
//                [view addSubview:btnImage];
//                // view.backgroundColor =[UIColor blackColor];
//                view.tag = btnImage.tag;
//                view.clipsToBounds = false;
//                view.backgroundColor = [UIColor clearColor];
//                [view addSubview:deleteButton];
//                view.userInteractionEnabled = true;
//                [self.Scrollview addSubview:view];
//                NSDictionary *dict  = @{@"isNew":@"1",@"image":btnImage};
//                [arrayVideo addObject:dict];
//                
//                [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
//                //[btnImage addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
//            }
//            //  [deleteButton addTarget:self action:@selector(DeletePress:) forControlEvents:UIControlEventTouchUpInside];
//            
//            
//            CGRect rectmain = self.viewAlltextfield.frame;
//            rectmain.origin.y = self.Scrollview.frame.origin.y + self.Scrollview.frame.size.height + 15;
//            self.viewAlltextfield.frame = rectmain;
//            
//            
//        }}}
//
//- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//


-(BOOL)isVideoAvalable{
    int conn = 0;
    for (id view  in arrayVideo){
        if (![view[@"image"] isKindOfClass:[UIImageView class]]){
            conn++;
        }
    }
    if (conn > 0){
        
        return true;
    }
    return NO;
}
- (IBAction)ditcatebtnpress:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditCategoryViewController *dealVC1 = (EditCategoryViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EditCategoryViewController"];
    if(_passcatidary.count>0)
    {
        dealVC1.productpasary=_passcatidary;
    }else{
    dealVC1.productpasary=self.result[@"category_id"];
        
    }
    [self.navigationController pushViewController:dealVC1 animated:YES];
}

- (IBAction)cancelbtnpress:(id)sender {
     _datepickerview.hidden=YES;
    
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = false;

}

- (IBAction)donebtnpress:(id)sender
{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"]; //12 hr time format
    NSString *dateString = [outputFormatter stringFromDate:self.datepikcer.date];
    _txtLead_time.text=dateString;
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = false;
    _datepickerview.hidden=YES;
}
- (IBAction)leadtimebtnpress:(id)sender
{
    [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
    
    _datepickerview.hidden=NO;

}
@end
