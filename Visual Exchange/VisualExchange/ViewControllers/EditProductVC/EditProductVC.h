//
//  EditProductVC.h
//  VisualExchange
//
//  Created by Nilay Shah on 18/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
@interface EditProductVC : BaseVC<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scollMain;
@property (weak, nonatomic) IBOutlet UITextField *txtProductname;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtWight;
- (IBAction)leadtimebtnpress:(id)sender;
@property (strong, nonatomic) IBOutlet UIDatePicker *datepikcer;
- (IBAction)cancelbtnpress:(id)sender;
- (IBAction)donebtnpress:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtPrice;
- (IBAction)ditcatebtnpress:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *datepickerview;
@property (weak, nonatomic) IBOutlet UITextField *txtwidth;
@property (weak, nonatomic) IBOutlet UIView *viewAlltextfield;
@property (weak, nonatomic) IBOutlet UIScrollView *Scrollview;
@property (weak, nonatomic) IBOutlet UITextField *txtheight;
@property (weak, nonatomic) IBOutlet UITextField *txtSKU;
@property (weak, nonatomic) IBOutlet UITextField *txtStocklimit;
@property (weak, nonatomic) IBOutlet UITextField *txtLead_time;
@property(weak,nonatomic)NSMutableDictionary *result;
@property (weak, nonatomic) IBOutlet UITextField *txtVideoLink;
@property (weak, nonatomic) IBOutlet UITextField *txtlength;
@property(strong,nonatomic)NSMutableArray *passcatidary;
@end
