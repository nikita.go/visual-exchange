//
//  BaseVC.h
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

- (void)showErrorWithMessage:(NSString *)errorMessage;
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
- (void)onShakeEventOccurred;
-(CGFloat)DDD:(float)floating;
-(NSString *)ststusWithCode:(id)status;
@end
