//
//  AddAdressVC.h
//  VisualExchange
//
//  Created by Nilay Shah on 24/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAdressVC : BaseVC
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;

@property (weak, nonatomic) IBOutlet UITextField *txtZipcode;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property(assign,nonatomic) BOOL isNew;
@property(strong,nonatomic)NSMutableDictionary *result;
@end
