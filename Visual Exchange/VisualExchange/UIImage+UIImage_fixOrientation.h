//
//  UIImage+UIImage_fixOrientation.h
//  VisualExchange
//
//  Created by mac on 1/13/17.
//  Copyright © 2017 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_fixOrientation)
- (UIImage *)fixOrientation;
@end
