//
//  SelleremailViewController.m
//  VisualExchange
//
//  Created by mac on 12/23/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "SelleremailViewController.h"

@interface SelleremailViewController ()

@end

@implementation SelleremailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  _sendemailbtn.layer.cornerRadius =_sendemailbtn.frame.size.height/2;
    _sendemailbtn.layer.masksToBounds=YES;
    NSString *mailcheckstatus=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"status"] ];
    if([mailcheckstatus isEqualToString:@"1"])
    {
        _sendemailbtn.hidden=YES;
        _emaillbl.text=@"Your Account is under review";
    }
    else{
        _sendemailbtn.hidden=NO;
        _emaillbl.text=@"You are not authorised to sell please click below to activate your seller account";

    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sendemailbtnpress:(id)sender
{
    _sendemailbtn.hidden=YES;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"change_customer_type" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
     {
//         NSUserDefaults *default5=[NSUserDefaults standardUserDefaults];
//         [default5 setObject:[JSON objectForKey:@"status"] forKey:@"status"];
//         [default5 synchronize];
         _emaillbl.text=@"Your Account is under review";
    }];
}

- (IBAction)menubtnpress:(id)sender
{
      [[AppLauncher sharedInstance]openLeftDrawer];
}
@end
