//
//  Confirm_orderViewController.m
//  VisualExchange
//
//  Created by mac on 1/6/17.
//  Copyright © 2017 Company. All rights reserved.
//

#import "Confirm_orderViewController.h"
#import "HomeVC.h"
#import "SearchCatagoryVC.h"

@interface Confirm_orderViewController ()

@end

@implementation Confirm_orderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [DLHelper AppDelegate].tabbarController.tabBar.hidden =false;
  //  [DLHelper AppDelegate].tabbarController.selectedIndex=2;


    _orderid.text=[NSString stringWithFormat:@"# %@",_orderstr];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)searchbtnpress:(id)sender
{
    SearchCatagoryVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchCatagoryVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backpress:(id)sender {
    HomeVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    
    [self.navigationController pushViewController:vc animated:YES];

}
@end
