//
//  AppLauncher.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "AppLauncher.h"
#import "AppDelegate.h"
#import "TabbarController.h"
#import "PrivacypolicyViewController.h"

@interface AppLauncher ()
@end
@implementation AppLauncher

+ (AppLauncher *)sharedInstance {
    static AppLauncher *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[AppLauncher alloc] init];
    });
    return instance;
}

- (MMDrawerController *)drawerController
{
    return [DLHelper AppDelegate].drawerController;
}

#pragma mark - Tabbar selection
- (void)setTabbarSelectedIndex:(NSInteger)tabIndex
{
    if ([DLHelper AppDelegate].tabbarController) {
        [[DLHelper AppDelegate].tabbarController setSelectedIndex:tabIndex];
    }
}

#pragma mark - Drawer Actions
- (void)openLeftDrawer
{
    if ([self drawerController]) {
        [[self drawerController] openDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
            
        }];
    }
}

- (void)closeDrawer
{
    if ([self drawerController]) {
        [[self drawerController] closeDrawerAnimated:YES completion:^(BOOL finished) {
            
        }];
    }
}

#pragma mark - Open Change Password 
- (void)openChangePassword
{
    [[DLHelper AppDelegate].tabbarController setSelectedIndex:3];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangePasswordVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    navigation.viewControllers = @[vc].mutableCopy;
}
- (void)Searchpress
{
    [[DLHelper AppDelegate].tabbarController setSelectedIndex:2];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"SearchCatagoryVC"];
    navigation.viewControllers = @[leftDrawer].mutableCopy;
}
-(void)orderPress{
    
    [[DLHelper AppDelegate].tabbarController setSelectedIndex:2];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryVC"];
    navigation.viewControllers = @[leftDrawer].mutableCopy;

    
    
}
-(void)privacy
{
    [[DLHelper AppDelegate].tabbarController setSelectedIndex:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"PrivacypolicyViewController"];
    
    navigation.viewControllers = @[leftDrawer].mutableCopy;
    

}
-(void)ShellonVR
{
    
    
    [[DLHelper AppDelegate].tabbarController setSelectedIndex:2];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"TotalOrderVC"];
    navigation.viewControllers = @[leftDrawer].mutableCopy;
    
    
    

}

@end
