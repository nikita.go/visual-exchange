//
//  AppDelegate.h
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import "Firebase.h"
#import "XCDYouTubeVideoPlayerViewController.h"

#import "WebServiceViewController.h"
#import "GUIPlayerView.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,FIRMessagingDelegate,UIAlertViewDelegate,UNUserNotificationCenterDelegate>
  
@property (strong, nonatomic) UIWindow *window;
-(void)isLoginUser;
@property(strong,nonatomic) MMDrawerController *  drawerController;
@property (strong, nonatomic)  UITabBarController *   tabbarController;
 @property (nonatomic, strong) XCDYouTubeVideoPlayerViewController *videoPlayerViewController;
@property (weak, nonatomic) id<GUIPlayerViewDelegate> delegate;
//customer_id
@end












