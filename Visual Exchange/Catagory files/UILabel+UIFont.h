//
//  UILabel+UIFont.h
//  BestEmployee
//
//  Created by Moweb_10 on 25/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (UIFont)
@property(assign,nonatomic)IBInspectable BOOL FontAutomatic;
@property(assign,nonatomic)IBInspectable BOOL FontHeader;
-(CGFloat)DLAutomatic:(CGFloat)FontAutomatic;
@end
