//
//  UITextView+font.m
//  BestEmployee
//
//  Created by Moweb_10 on 28/05/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import "UITextView+font.h"

@implementation UITextView (font)
@dynamic FontAutomatic;
-(void)setFontAutomatic:(BOOL)FontAutomatic{
    
    if(FontAutomatic){
        
        CGFloat height = (self.frame.size.height*SCREEN_HEIGHT)/568;
        self.font = [self.font fontWithSize:(height*self.font.pointSize)/self.frame.size.height];
    }
}

@end
