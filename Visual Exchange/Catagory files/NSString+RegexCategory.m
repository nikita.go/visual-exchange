//
//  NSString+RegexCategory.h
//  IOS-Categories
//
//  Created by KevinHM on 15/6/24.
//  Copyright (c) 2015年 KevinHM. All rights reserved.
//  https://github.com/KevinHM
//

#import "NSString+RegexCategory.h"

@implementation NSString (RegexCategory)

-(BOOL)validateEmailWithString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)validatePassword
{
    if (self.length < 4) {
        return NO;
    }
    return YES;
}

-(BOOL)validatePhoneNumber
{
    if (self.length != 10) {
        return NO;
    }
    return YES;
}

-(BOOL) isEmptyString
{
    NSString *str = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (str.length == 0) {
        return YES;
    }
    return NO;
}
- (BOOL) isValidateUrl {
    NSString *urlRegEx = @"(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:self];
}



@end
