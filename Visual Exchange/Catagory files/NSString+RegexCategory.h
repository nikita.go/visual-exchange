

#import <Foundation/Foundation.h>



@interface NSString (RegexCategory)

-(BOOL)validateEmailWithString;
-(BOOL)validatePassword;
-(BOOL)validatePhoneNumber;
-(BOOL) isEmptyString;
- (BOOL) isValidateUrl;
@end
