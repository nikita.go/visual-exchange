//
//  UIScrollView+DLscroll.h
//  VisualExchange
//
//  Created by Nilay Shah on 28/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (DLscroll)
-(void)AutomaticScrollview;
@end
