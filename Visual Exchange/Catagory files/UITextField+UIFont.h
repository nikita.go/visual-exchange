//
//  UITextField+UIFont.h
//  BestEmployee
//
//  Created by Moweb_10 on 25/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (UIFont)
@property(assign,nonatomic)IBInspectable BOOL FontAutomatic;

@end
