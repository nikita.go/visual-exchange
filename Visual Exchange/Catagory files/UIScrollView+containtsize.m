//
//  UIScrollView+containtsize.m
//  BestEmployee
//
//  Created by Moweb_10 on 25/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import "UIScrollView+containtsize.h"

@implementation UIScrollView (containtsize)
-(void)automatictScroll{
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.contentSize = CGSizeMake(0, contentRect.size.height+2);
}
@end
