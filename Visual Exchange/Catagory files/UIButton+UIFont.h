//
//  UIButton+UIFont.h
//  BestEmployee
//
//  Created by Moweb_10 on 28/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (UIFont)
@property(assign,nonatomic)IBInspectable BOOL FontAutomatic;
@end
