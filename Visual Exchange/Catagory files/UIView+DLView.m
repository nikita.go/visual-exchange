//
//  UIView+DLView.m
//  UFeedback
//
//  Created by Moweb_10 on 19/04/16.
//  Copyright © 2016 Moweb. All rights reserved.
//

#import "UIView+DLView.h"

@implementation UIView (DLView)
@dynamic DLBordercolor;
@dynamic DLBorderWidth;
@dynamic DLCornerRedius;
@dynamic DLRound;
@dynamic FixRound;
@dynamic IphoneRound;
-(void)setDLBordercolor:(UIColor *)DLBordercolor{
    self.layer.borderColor = DLBordercolor.CGColor;
    self.layer.masksToBounds = true;
}
-(void)setDLBorderWidth:(CGFloat)DLBorderWidth{
    
    self.layer.borderWidth = DLBorderWidth;
    self.layer.masksToBounds = true;
}
-(void)setDLCornerRedius:(CGFloat)DLCornerRedius{
    
    self.layer.cornerRadius = DLCornerRedius;
    self.layer.masksToBounds = true;
    
}
-(void)setDLRound:(BOOL)DLRound{
    
    if(DLRound){
        //        if(IS_IPHONE_4){
        //            self.layer.cornerRadius = self.frame.size.height/2;
        //            self.layer.masksToBounds = true;
        //            return;
        //        }
        CGFloat Height =  (SCREEN_HEIGHT *self.frame.size.height)/568;
        self.layer.cornerRadius = Height/2;
        self.layer.masksToBounds = true;
    }
}
-(void)setFixRound:(BOOL)FixRound{
    if(FixRound){
        
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = true;
    }
}
-(void)setIphoneRound:(BOOL)IphoneRound{
    
    if(IphoneRound){
        if(IS_IPHONE_4){
            self.layer.cornerRadius = self.frame.size.height/2;
            self.layer.masksToBounds = true;
            return;
        }
        CGFloat Height =  (SCREEN_HEIGHT *self.frame.size.height)/568;
        self.layer.cornerRadius = Height/2;
        self.layer.masksToBounds = true;
    }
    
}
-(void)setShadow:(BOOL)Shadow{
    if(Shadow){
        // UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        self.layer.shadowOpacity = 0.6f;
        //        if([self isKindOfClass:[UILabel class]]){
        //
        //            UILabel * lbl = (UILabel *)self;
        //
        //
        //        }
        // self.layer.shadowPath = shadowPath.CGPath;
    }
}
- (void)BottomBoder
{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColor lightGrayColor] CGColor];
    upperBorder.frame = CGRectMake(0, self.frame.size.height-1, CGRectGetWidth(self.frame), 1.0f);
    [self.layer addSublayer:upperBorder];
}


-(void)TopRedius:(CGFloat )cornerredius {
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight) cornerRadii:CGSizeMake(cornerredius, cornerredius)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.layer.mask = maskLayer;
}
-(void)BottomRedius:(CGFloat )cornerredius {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerBottomRight|UIRectCornerBottomLeft) cornerRadii:CGSizeMake(cornerredius, cornerredius)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

-(void)setIphoneroundmake:(BOOL)Iphoneroundmake{
    
    if(Iphoneroundmake){
        // if(IS_IPAD ){
        [self performSelector:@selector(IphoneRoundMaking) withObject:nil afterDelay:.01];
      //   }
       //  else{
             
             CGFloat Height =  (SCREEN_WIDTH * self.frame.size.height)/320;
             self.layer.cornerRadius = Height/2;
             self.layer.masksToBounds = true;
             
       //  }
    }
}
-(void)IphoneRoundMaking
{
 
   
    CGRect rect = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.height, self.frame.size.height);
    self.frame = rect;
    self.layer.cornerRadius = self.frame.size.width/2;
    self.layer.masksToBounds = true;
    
}
@end
