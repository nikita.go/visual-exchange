//
//  UITextField+UIFont.m
//  BestEmployee
//
//  Created by Moweb_10 on 25/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import "UITextField+UIFont.h"

@implementation UITextField (UIFont)
@dynamic FontAutomatic;
-(void)setFontAutomatic:(BOOL)FontAutomatic{
    
    if(FontAutomatic){
        
   
        
        CGFloat height = (self.frame.size.height*SCREEN_HEIGHT)/568;
       self.font = [self.font fontWithSize:(height*self.font.pointSize)/self.frame.size.height];
    }
}

@end
