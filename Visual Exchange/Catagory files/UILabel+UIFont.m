//
//  UILabel+UIFont.m
//  BestEmployee
//
//  Created by Moweb_10 on 25/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import "UILabel+UIFont.h"

@implementation UILabel (UIFont)
@dynamic FontAutomatic;
-(void)setFontAutomatic:(BOOL)FontAutomatic{
 
    if(FontAutomatic){
        
       CGFloat height = (self.frame.size.height*SCREEN_HEIGHT)/568;
   self.font = [self.font fontWithSize:(height*self.font.pointSize)/self.frame.size.height];        
    }
}
-(CGFloat)DLAutomatic:(CGFloat)FontAutomatic{
    
  
        
      return  FontAutomatic * SCREEN_WIDTH/320;
    
    
}
-(void)setFontHeader:(BOOL)FontHeader{
    
    if(FontHeader){
        if(IS_IPAD){
            
            self.font = [self.font fontWithSize:25];
            return;
        }
        
        CGFloat height = (self.frame.size.height*SCREEN_HEIGHT)/568;
        self.font = [self.font fontWithSize:(height*self.font.pointSize)/self.frame.size.height];
        
    }
}

@end
