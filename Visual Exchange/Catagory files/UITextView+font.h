//
//  UITextView+font.h
//  BestEmployee
//
//  Created by Moweb_10 on 28/05/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (font)
@property(assign,nonatomic)IBInspectable BOOL FontAutomatic;
@end
